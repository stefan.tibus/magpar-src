#!/bin/sh

# magpar output files

rm core
rm stdouterr.txt
rm prunscript.* stdout.* stderr.*
rm $1.lo*
rm $1.????.datmsh
rm $1.????.fe???
rm $1.????.d
rm $1.????.gz
rm $1.????.?.png
rm Writefld*
rm JOB_FINISHED
rm $1.*.off
rm $1.agr
rm stdouterr*
exit

# DEPRECATED

# vecu output files

rm $1.000
rm $1.all
rm $1.err
rm $1.eta
rm $1.exc
rm $1.inf
#rm $1*.inp
rm $1.jh
rm $1.jt
rm $1.lst
rm $1.sat
rm $1.set
rm out

# Diffpack output files

rm SIMULATION*
rm $1.logT
rm ana$1.agr
rm ana$1.agr.com
rm pattern1
rm .menutree
