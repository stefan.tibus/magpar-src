/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* $Id: util.h 2962 2010-02-04 19:50:44Z scholz $ */

#ifndef UTIL_H
#define UTIL_H

#include "griddata.h"

int ascat(int **aindata,int n_items,int s_item,int *newproc,int insize,int send);
int AreaCal(Mat AIpol, PetscReal *p_area);
int AxesRotation(PetscReal *slice_p,PetscReal *slice_n,PetscReal *axes1,int inv,int n_vert,PetscReal *vertxyz,PetscReal *vertxyz2);
int barycent(PetscReal *p,PetscReal *a,PetscReal *b,PetscReal *c,PetscReal *d,PetscReal *bary);
int bbox2(GridData *gdata,PetscReal *vertxyz2,int n_prop,int *propid,PetscReal *bbox,int *pix);
int calcbbox(int n_ele,int *elevert,int *eleprop,PetscReal *vertxyz2,int n_pids,int *slice_id,PetscReal *bbox);
int Cart2SphereDiff(Vec C,Vec X,Vec S);
int Cart2Sphere(Vec C,Vec S);
int CalAfe2fe(GridData *gdata,PetscReal *shift,int *slice_id,Mat *pAIpol);
int CalAsq2fe(GridData *gdata,PetscReal *vertxyz2,PetscReal *bbox,int *pix,int slice_id,Mat *AIpol);
int CalAfe2sq(GridData *gdata,PetscReal *vertxyz2,PetscReal *bbox,int *pix,int slice_id,Mat *AIpol);
int distint(int from,int to,int a);
int DistortVec(PetscRandom rctx,Vec v,PetscReal distpar);
int DistPointLine(PetscReal *point,PetscReal *linepoint,PetscReal *linevec,PetscReal *dist);
PetscScalar getscalf(PetscReal tfdata[],PetscReal time);
int matcreateseqadj(int nvert,int nele,int *elevert,Mat *mat);
int matviewstruct(MPI_Comm comm,Mat mat);
int Mesh2Dual(int n_ele, int n_vert, int *elevert, int **ia, int **ja);
int Mesh2Dual2(int n_ele, int n_vert, int *elevert, int **ia, int **ja);
int Mesh2Nodal(int n_ele, int n_vert, int *elevert, int **ia, int **ja);
int Mesh2VertEle(int n_ele, int n_vert, int *elevert, int **ia, int **ja);
int PointFromPlane(PetscReal *x, PetscReal *v1, PetscReal *v2, PetscReal *v3, PetscReal *d);
int PrintMatInfo(Mat A,MatInfoType flag);
int PrintMatInfoAll(Mat A);
int ProgressBar(int cur,int max,int ival);
int readht(FILE *fd,PetscReal **indata,const int col);
int readxy(FILE *fd,const int colx,const int coly,int *n_rows,PetscReal **indata);
PetscReal RenormVec(Vec v,PetscReal normset,PetscReal normtol,int dim);
int PidSharedArea(GridData *gdata,int pid1,int pid2,PetscReal *sharedarea_area,PetscReal *sharedarea_point,PetscReal *sharedarea_norm);
int SolidAngle(PetscScalar *x,PetscScalar *v1,PetscScalar *v2,PetscScalar *v3,PetscScalar *omega);
int Sphere2Cart(Vec S,Vec C);
int SynchronizedFastFPrintf(FILE *fd,int nchars,int nmax,char *wbufs,PetscTruth zip);
int tetnb(int *tet1, int *tet2, int *vshared);
PetscReal tetvol(PetscReal *x1,PetscReal *x2,PetscReal *x3,PetscReal *x4);
PetscReal triarea(PetscReal *v0,PetscReal *v1,PetscReal *v2,PetscReal *n);
int TetSharedVertices(int *v1,int *v2,int *s);
int VecSetVec(Vec vec,PetscScalar *vec3,int dim);

#endif
