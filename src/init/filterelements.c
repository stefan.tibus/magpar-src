/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: filterelements.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "init.h"

#define C_DEL -5   /**< indicator for elements to be deleted*/

int FilterElements(int *pn_ele,int **pelevert,int **peleprop,PetscReal *propdat)
{
  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  if (rank) {
    MagparFunctionLogReturn(0);
  }

  int n_ele;    n_ele=*pn_ele;
  int *elevert; elevert=*pelevert;
  int *eleprop; eleprop=*peleprop;

  int n_del=0;

  /* get number of elements to be deleted, delete element if Js<0 */
  for (int i=0;i<n_ele;i++) {
    if (propdat[NP*eleprop[i]+4]<0.0) {
      eleprop[i]=-1;
      n_del++;
    }
  }

  if (n_del>0) {
    PetscPrintf(PETSC_COMM_WORLD,
    "Removing elements: %i out of %i\n",n_del,n_ele);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"No elements to be removed\n");
    MagparFunctionLogReturn(0);
  }


  PetscPrintf(PETSC_COMM_WORLD,"Allocating new element arrays\n");

  /* allocate new array for elements */
  int *televert;
  ierr = PetscMalloc(NV*(n_ele-n_del)*sizeof(int),&televert);CHKERRQ(ierr);
  int *teleprop;
  ierr = PetscMalloc((n_ele-n_del)*sizeof(int),&teleprop);CHKERRQ(ierr);

  PetscPrintf(PETSC_COMM_WORLD,"Copying element data\n");

  /* copy elements and properties */
  int k=0;
  for (int i=0;i<n_ele;i++) {
    if (eleprop[i]>=0) {
      for (int j=0;j<NV;j++) {
        televert[NV*k+j]=elevert[NV*i+j];
      }
      teleprop[k]=eleprop[i];
      k++;
    }
  }
  assert(k==n_ele-n_del);

  n_ele -= n_del;

  /* update counters */
  *pn_ele=n_ele;

  ierr = PetscFree(*pelevert);CHKERRQ(ierr);
  *pelevert=televert;
  ierr = PetscFree(*peleprop);CHKERRQ(ierr);
  *peleprop=teleprop;

  MagparFunctionLogReturn(0);
}
