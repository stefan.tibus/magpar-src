/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: hexternal.c 3001 2010-03-15 15:22:36Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "field.h"
#include "util/util.h"

#ifdef ADDONS
#include "addons/addons.h"
#endif

#ifdef SPINTORQ
#include "spintorq/spintorq.h"
#endif

static int doinit=1;
static Vec VHextsum=PETSC_NULL;
static PetscReal hextsum=0;

int Hexternal_Init(GridData *gdata)
{
  MagparFunctionLogBegin;

  PetscPrintf(PETSC_COMM_WORLD,"Hexternal on\n");

  ierr = VecDuplicate(gdata->M,&VHextsum);CHKERRQ(ierr);
  ierr = VecDuplicate(gdata->M,&gdata->VHext);CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}


int Hexternal(GridData *gdata,Vec VHtotsum)
{
  MagparFunctionInfoBegin;

  /* initialize if necessary */
  if (doinit) {
    ierr = Hexternal_Init(gdata);CHKERRQ(ierr);
    doinit=0;
  }

  hextsum=0;
  ierr = VecZeroEntries(VHextsum);CHKERRQ(ierr);

  PetscReal hext;
  hext=0;ierr = Hext_ho(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
  hext=0;ierr = Hext_kq(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
  hext=0;ierr = Hext_cu(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
  hext=0;ierr = Hext_py(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
#ifdef ADDONS
  hext=0;ierr = Hext_ac(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
  hext=0;ierr = Hext_co(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
  hext=0;ierr = Hext_do(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
  hext=0;ierr = Hext_ht1(gdata,VHextsum,&hext);CHKERRQ(ierr);hextsum += hext;
  hext=0;ierr = Hext_ht2(gdata,VHextsum,&hext);CHKERRQ(ierr);hextsum += hext;
  hext=0;ierr = Hext_mp(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
  hext=0;ierr = Hext_pd(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
  hext=0;ierr = Hext_rg(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
  hext=0;ierr = Hext_tf(gdata,VHextsum,&hext);CHKERRQ(ierr); hextsum += hext;
#endif
#ifdef SPINTORQ
  hext=0;ierr = Hcurrent(gdata,VHextsum,&hext);CHKERRQ(ierr);hextsum += hext;
  hext=0;ierr = Hcurrent_stack(gdata,VHextsum,&hext);CHKERRQ(ierr);hextsum += hext;
#endif

  /* copy total field into VHext */
  ierr = VecCopy(VHextsum,gdata->VHext);CHKERRQ(ierr);

  ierr = VecAXPY(VHtotsum,1,VHextsum);CHKERRQ(ierr);

  MagparFunctionProfReturn(0);
}


int Hexternal_Energy(GridData *gdata,Vec VMom,PetscReal *energy)
{
  PetscReal e;

  MagparFunctionInfoBegin;

  ierr = VecDot(VMom,VHextsum,&e);CHKERRQ(ierr);
  e *= -1.0;

  *energy=e;

  MagparFunctionProfReturn(0);
}


PetscReal Hexternal_hext()
{
  MagparFunctionInfoBegin;
  MagparFunctionInfoReturn(hextsum);
}

