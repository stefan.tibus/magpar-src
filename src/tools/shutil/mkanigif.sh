#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Syntax: $0 <simName>"
  echo "  This script converts all *.????.?.png into gif files using"
  echo "  the convert tool from ImageMagick and makes three"
  echo "  animated gifs (*_mov.?.gif) using whirlgif."
  exit
fi

for i in $1.????.?.png; do
  convert $i `basename $i .png`.gif
done

whirlgif -loop -o $1_mov.0.gif *.0.gif
whirlgif -loop -o $1_mov.1.gif *.1.gif
whirlgif -loop -o $1_mov.2.gif *.2.gif


