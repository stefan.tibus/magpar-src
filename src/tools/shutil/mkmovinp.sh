#!/bin/sh

femesh=$1
movinp=mov.inp

nodedata=13
eledata=0

# create header for multistep file

echo $(( $# - 1 )) > $movinp
echo "data" >> $movinp
echo "step1" >> $movinp

# append first inp file with mesh data

cat $femesh >> $movinp

shift

# append first data file without the "step1"

echo "$nodedata $eledata"  >> $movinp    # number node data and element data
gunzip -c $1 >> $movinp
step=$(( $step + 1 ))
shift

# append other data files

# set counter
step=2

while [ $# -gt 0 ]; do
  echo "step$step"  >> $movinp
  echo "$nodedata $eledata"  >> $movinp    # number node data and element data
  gunzip -c $1 >> $movinp
  step=$(( $step + 1 ))
  shift
done
