/*
    This file is part of magpar.

    Copyright (C) 2006-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* $Id: printmatinfo.c 2962 2010-02-04 19:50:44Z scholz $ */

#include "util.h"

int PrintMatInfo(Mat A,MatInfoType flag)
{
  int ierr;
  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  PetscTruth oinfo;
  PetscOptionsHasName(PETSC_NULL,"-info",&oinfo);
if (oinfo) {

  MatInfo info;
  ierr = MatGetInfo(A,MAT_GLOBAL_SUM,&info);CHKERRQ(ierr);

  PetscErrorCode (*printffunc)(MPI_Comm comm,const char format[],...);

  if (flag==MAT_LOCAL) {
    printffunc=PetscSynchronizedPrintf;
  }
  else {
    printffunc=PetscPrintf;
  }

#if PETSC_VERSION >= 300
  PetscInt grows,gcols;
  ierr = MatGetSize(A,&grows,&gcols);CHKERRQ(ierr);
  printffunc(PETSC_COMM_WORLD,"  <%i> global rows and columns: %g x %g\n",
    rank,grows,gcols);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);

  PetscInt lrows,lcols;
  ierr = MatGetLocalSize(A,&lrows,&lcols);CHKERRQ(ierr);
  printffunc(PETSC_COMM_WORLD,"  <%i> local rows and columns: %g x %g (%g %%)\n",
    rank,lrows,lcols,100.0*lrows/grows);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);

  PetscInt bs;
  ierr = MatGetBlockSize(A,&bs);CHKERRQ(ierr);
  printffunc(PETSC_COMM_WORLD,"  <%i> block size %g\n",
    rank,bs);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);
#else
  printffunc(PETSC_COMM_WORLD,"  <%i> global rows and columns: %g x %g\n",
    rank,info.rows_global,info.columns_global);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);
  printffunc(PETSC_COMM_WORLD,"  <%i> local rows and columns: %g x %g (%g %%)\n",
    rank,info.rows_local,info.columns_local,100.0*info.rows_local/info.rows_global);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);
  printffunc(PETSC_COMM_WORLD,"  <%i> block size %g\n",
    rank,info.block_size);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);
#endif
  printffunc(PETSC_COMM_WORLD,"  <%i> nonzeroes allocated: %g (%g %%)  used: %g (%g %%)  unneeded: %g (%g %%)\n",
    rank,info.nz_allocated,100.0*info.nz_allocated/info.nz_used,info.nz_used,100.0*info.nz_used/info.nz_used,info.nz_unneeded,100.0*info.nz_unneeded/info.nz_used);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);
  printffunc(PETSC_COMM_WORLD,"  <%i> memory allocated: %g\n",
    rank,info.memory);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);
  printffunc(PETSC_COMM_WORLD,"  <%i> number of matrix assemblies called: %g\n",
    rank,info.assemblies);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);
  printffunc(PETSC_COMM_WORLD,"  <%i> number of mallocs during MatSetValues: %g\n",
    rank,info.mallocs);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);
  printffunc(PETSC_COMM_WORLD,"  <%i> fill_ratio_given: %g  fill_ratio_needed: %g  for LU/ILU\n",
    rank,info.fill_ratio_given,info.fill_ratio_needed);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);
  printffunc(PETSC_COMM_WORLD,"  <%i> number of mallocs during factorization: %g\n",
    rank,info.factor_mallocs);if (flag==MAT_LOCAL) PetscSynchronizedFlush(PETSC_COMM_WORLD);

}

  return(0);
}

int PrintMatInfoAll(Mat A)
{
  int ierr;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  PetscTruth info;
  PetscOptionsHasName(PETSC_NULL,"-info",&info);
if (info) {

  if (size>1) {
    PetscPrintf(PETSC_COMM_WORLD,"local matrix info:\n");
    ierr = PrintMatInfo(A,MAT_LOCAL);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"global max. matrix info:\n");
    ierr = PrintMatInfo(A,MAT_GLOBAL_MAX);CHKERRQ(ierr);
  }
  PetscPrintf(PETSC_COMM_WORLD,"global sum matrix info:\n");
  ierr = PrintMatInfo(A,MAT_GLOBAL_SUM);CHKERRQ(ierr);

}

  return(0);
}
