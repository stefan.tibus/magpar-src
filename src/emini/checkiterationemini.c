/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: checkiterationemini.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "emini.h"
#include "io/magpario.h"
#include "field/field.h"

static int       doinit=1;

static int       condinp_equil;
static PetscReal condinp_j;
static PetscReal condinp_equil_j;

int CheckIterationEmini_Init()
{
  MagparFunctionLogBegin;

  PetscTruth flg;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-condinp_equil",(PetscInt*)&condinp_equil,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    condinp_equil=1;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -condinp_equil not found, using default value: %i\n",
      condinp_equil
    );
  }
  ierr = PetscOptionsGetReal(PETSC_NULL,"-condinp_j",&condinp_j,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    condinp_j=0.1;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -condinp_j not found, using default value: %g\n",
      condinp_j
    );
  }
  ierr = PetscOptionsGetReal(PETSC_NULL,"-condinp_equil_j",&condinp_equil_j,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    condinp_equil_j=0.0;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -condinp_equil_j not found, using default value: %g\n",
      condinp_equil_j
    );
  }

  MagparFunctionLogReturn(0);
}


int CheckIterationEmini(GridData *gdata)
{
  static PetscReal condinp_last_j=PETSC_MIN;
  static PetscReal condinp_last_equil_j=PETSC_MIN;

  MagparFunctionInfoBegin;

  /* initialize if necessary */
  if (doinit) {
    ierr = CheckIterationEmini_Init();CHKERRQ(ierr);
    doinit=0;
  }

  /* check if we should save complete set of output data */
  PetscReal valMpHext;
  ierr = MpHext(gdata->M,&valMpHext);CHKERRQ(ierr);

  /* check if a set of output data should be written */
  if (PetscAbsReal(valMpHext-condinp_last_j) > condinp_j) {
    gdata->inp=PetscAbsInt(gdata->inp);
    condinp_last_j=valMpHext;
  }
  else if (condinp_equil==1 && gdata->equil>0) {
    /* check for condinp_equil_j (defaults to 0.0) */
    if (PetscAbsReal(valMpHext-condinp_last_equil_j) > condinp_equil_j) {
      gdata->inp=PetscAbsInt(gdata->inp);
      condinp_last_j=valMpHext;
      condinp_last_equil_j=valMpHext;
    }
  }
  else {
    /* write checkpoint file even if we are not in equilibrium */
    int  oldinp;
    oldinp=gdata->inp;
    gdata->inp=9999;

    PetscPrintf(PETSC_COMM_WORLD,
      "Writing checkpoint file (9999) at: %g ns\n",
      gdata->time*gdata->tscale*1e9
    );
    ierr = WriteSet(gdata);CHKERRQ(ierr);

    gdata->inp=oldinp;
  }

  /* write output data */
  ierr = WriteLog(gdata);CHKERRQ(ierr);
  ierr = WriteSet(gdata);CHKERRQ(ierr);

  /* if system is in equilibrium */
  if (gdata->equil>0) {

    /* update external field */
    ierr = Hext_ho_hstep(gdata);CHKERRQ(ierr);

    /* increase (pseudo-)time by 1 ns */
    gdata->time += 1e-9/gdata->tscale;
  }

  /* exit if field did not get updated and we are
     therefore still in equilibrium
  */
  if (gdata->equil>0) {
    gdata->mode=99;
  }

  MagparFunctionProfReturn(0);
}

