/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* $Id: emini.h 2962 2010-02-04 19:50:44Z scholz $ */

#ifndef EMINI_H
#define EMINI_H

#include "griddata.h"

#ifdef TAO

#include "tao.h"

int CheckIterationEmini(GridData *gdata);
int EminiSolve(GridData *gdata);
int myTSCreateEmini(GridData *gdata);

#endif

#endif
