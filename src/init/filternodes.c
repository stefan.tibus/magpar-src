/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: filternodes.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "init.h"

#define C_REQ -5   /**< indicator for required node */

int FilterNodes(int *pn_vert, int *pn_ele, PetscReal **pvertxyz, int *elevert, Vec *pM, Vec *pVH1, Vec *pVH2)
{
  Vec          M,VH1,VH2;
  PetscReal    *toM,*toVH1,*toVH2;
  PetscReal    *tnM,*tnVH1,*tnVH2;

  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  if (rank) {
    int n_del;
    ierr = MPI_Bcast(&n_del,1,MPI_INT,0,PETSC_COMM_WORLD);CHKERRQ(ierr);

    if (n_del!=0) {
      ierr = VecCreate(PETSC_COMM_WORLD,&M);CHKERRQ(ierr);
      ierr = VecSetSizes(M,0,PETSC_DECIDE);CHKERRQ(ierr);
      ierr = VecSetFromOptions(M);CHKERRQ(ierr);
      ierr = VecSetBlockSize(M,ND);CHKERRQ(ierr);
      ierr = VecDestroy(*pM);CHKERRQ(ierr);
      *pM=M;

      if (*pVH1!=PETSC_NULL) {
        ierr = VecDuplicate(M,&VH1);CHKERRQ(ierr);
        ierr = VecDestroy(*pVH1);CHKERRQ(ierr);
        *pVH1=VH1;
      }
      if (*pVH2!=PETSC_NULL) {
        ierr = VecDuplicate(M,&VH2);CHKERRQ(ierr);
        ierr = VecDestroy(*pVH2);CHKERRQ(ierr);
        *pVH2=VH2;
      }
    }

    MagparFunctionLogReturn(0);
  }

  int n_vert;         n_vert=*pn_vert;
  int n_ele;          n_ele=*pn_ele;
  PetscReal *vertxyz; vertxyz=*pvertxyz;

  int *nodesreq;
  ierr = PetscMalloc(n_vert*sizeof(int),&nodesreq);CHKERRQ(ierr);
  for (int i=0;i<n_vert;i++) nodesreq[i]=C_UNK;

  /* find all required nodes */
  for (int i=0;i<n_ele;i++) {
    for (int j=0;j<NV;j++) {
      nodesreq[elevert[i*NV+j]]=C_REQ;
    }
  }

  int n_req=0;

  /* create mapping */
  for (int i=0;i<n_vert;i++) {
    if (nodesreq[i]==C_REQ) {
      /* assign new id and update counter */
      nodesreq[i]=n_req++;
    }
  }

  /* get number of nodes to be deleted and inform other processors */
  int n_del;
  n_del=n_vert-n_req;
  ierr = MPI_Bcast(&n_del,1,MPI_INT,0,PETSC_COMM_WORLD);CHKERRQ(ierr);

  if (n_del!=0) {
    PetscPrintf(PETSC_COMM_WORLD,
    "Unassigned vertices: %i out of %i\n",n_del,n_vert);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"No unassigned vertices\n");
    ierr = PetscFree(nodesreq);CHKERRQ(ierr);
    MagparFunctionLogReturn(0);
  }

  /* renumber nodes */
  for (int i=0;i<n_ele;i++) {
    for (int j=0;j<NV;j++) {
      elevert[i*NV+j]=nodesreq[elevert[i*NV+j]];
      assert(nodesreq[elevert[i*NV+j]]<n_req);
    }
    assert(
      elevert[i*NV+0] !=
      elevert[i*NV+1] &&
      elevert[i*NV+0] !=
      elevert[i*NV+2] &&
      elevert[i*NV+0] !=
      elevert[i*NV+3] &&
      elevert[i*NV+1] !=
      elevert[i*NV+2] &&
      elevert[i*NV+1] !=
      elevert[i*NV+3] &&
      elevert[i*NV+2] !=
      elevert[i*NV+3]
    );
  }

  PetscPrintf(PETSC_COMM_WORLD,"Updated elements\n");

  PetscReal *tvertxyz;
  ierr = PetscMalloc(ND*n_req*sizeof(PetscReal),&tvertxyz);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&M);CHKERRQ(ierr);
  ierr = VecSetSizes(M,ND*n_req,PETSC_DECIDE);CHKERRQ(ierr);
  ierr = VecSetFromOptions(M);CHKERRQ(ierr);
  ierr = VecSetBlockSize(M,ND);CHKERRQ(ierr);

  ierr = VecGetArray(*pM,&toM);CHKERRQ(ierr);
  ierr = VecGetArray(M,&tnM);CHKERRQ(ierr);

  if (*pVH1!=PETSC_NULL) {
    ierr = VecDuplicate(M,&VH1);CHKERRQ(ierr);
    ierr = VecGetArray(*pVH1,&toVH1);CHKERRQ(ierr);
    ierr = VecGetArray(VH1,&tnVH1);CHKERRQ(ierr);
  }
  if (*pVH2!=PETSC_NULL) {
    ierr = VecDuplicate(M,&VH2);CHKERRQ(ierr);
    ierr = VecGetArray(*pVH2,&toVH2);CHKERRQ(ierr);
    ierr = VecGetArray(VH2,&tnVH2);CHKERRQ(ierr);
  }

  int k=0;
  /* update (and compress) vertxyz */
  for (int i=0;i<n_vert;i++) {
    if (nodesreq[i]!=C_UNK) {
      for (int j=0;j<ND;j++) {
        tvertxyz[ND*k+j]=vertxyz[ND*i+j];
        tnM[ND*k+j]=toM[ND*i+j];
        if (*pVH1!=PETSC_NULL) tnVH1[ND*k+j]=toVH1[ND*i+j];
        if (*pVH2!=PETSC_NULL) tnVH2[ND*k+j]=toVH2[ND*i+j];
      }
      k++;
    }
  }
  assert(k==n_req);

  ierr = VecRestoreArray(*pM,&toM);CHKERRQ(ierr);
  ierr = VecRestoreArray(M,&tnM);CHKERRQ(ierr);
  ierr = VecDestroy(*pM);CHKERRQ(ierr);
  *pM=M;

  if (*pVH1!=PETSC_NULL) {
    ierr = VecRestoreArray(*pVH1,&toVH1);CHKERRQ(ierr);
    ierr = VecRestoreArray(VH1,&tnVH1);CHKERRQ(ierr);
    ierr = VecDestroy(*pVH1);CHKERRQ(ierr);
    *pVH1=VH1;
  }
  if (*pVH2!=PETSC_NULL) {
    ierr = VecRestoreArray(*pVH2,&toVH2);CHKERRQ(ierr);
    ierr = VecRestoreArray(VH2,&tnVH2);CHKERRQ(ierr);
    ierr = VecDestroy(*pVH2);CHKERRQ(ierr);
    *pVH2=VH2;
  }

  PetscPrintf(PETSC_COMM_WORLD,
    "Updated Cartesian coordinates of %i vertices\n",
    n_req
  );

  /* update counters */
  *pn_vert=n_req;
  *pn_ele=n_ele;

  ierr = PetscFree(*pvertxyz);CHKERRQ(ierr);
  *pvertxyz=tvertxyz;

  ierr = PetscFree(nodesreq);CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}
