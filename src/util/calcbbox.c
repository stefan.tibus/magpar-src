/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: calcbbox.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "util.h"

int calcbbox(int n_ele,int *elevert,int *eleprop,PetscReal *vertxyz2,int n_pids,int *slice_id,PetscReal *bbox)
{
  int i,j,k,l;
  PetscReal bbox2[2*ND]={PETSC_MAX,PETSC_MAX,PETSC_MAX,PETSC_MIN,PETSC_MIN,PETSC_MIN};

  MagparFunctionLogBegin;


  assert(n_pids>=1);

  PetscPrintf(PETSC_COMM_WORLD,
    "Searching bbox for %i volumes, pid[0]=%i\n",
    n_pids,slice_id[0]+1
  );

  /* find bbox of cut slice in x-y plane */
  for (i=0; i<n_ele; i++) {
    for (l=0;l<n_pids;l++) {
      if (slice_id[l]<0 || eleprop[i] == slice_id[l]) {
        for (j=0; j<NV; j++) {
          for (k=0;k<ND;k++) {
            bbox2[k]   =PetscMin(bbox2[k],   vertxyz2[ND*elevert[NV*i+j]+k]);
            bbox2[ND+k]=PetscMax(bbox2[ND+k],vertxyz2[ND*elevert[NV*i+j]+k]);
          }
        }
      }
    }
  }

  PetscPrintf(PETSC_COMM_WORLD,
    "bbox of volume with pid %i (0=any):\n"
    "%14s %14s   %s\n",
    slice_id[0]+1,"min","max","width"
  );

  for (k=0;k<ND;k++) {
    ierr = PetscGlobalMin(&bbox2[k],   &bbox[k],   PETSC_COMM_WORLD);CHKERRQ(ierr);
    ierr = PetscGlobalMax(&bbox2[ND+k],&bbox[ND+k],PETSC_COMM_WORLD);CHKERRQ(ierr);

    PetscPrintf(PETSC_COMM_WORLD,
      "%14e %14e   %g\n",
      bbox[k],bbox[ND+k],bbox[ND+k]-bbox[k]
    );
    assert(bbox[ND+k]-bbox[k]>0);
  }

  MagparFunctionLogReturn(0);
}

