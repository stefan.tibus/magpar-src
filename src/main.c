/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: main.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#ifdef PYTHON
#include "Python.h"
#endif

#include "init/init.h"
#include "util/util.h"
#include "io/magpario.h"
#include "field/field.h"

#ifdef SUNDIALS_VERSION
#include "llg/llg.h"
#endif

#ifdef TAO
#include "emini/emini.h"
#endif

#ifdef ADDONS
#include "addons/addons.h"
#include "llgts/llgts.h"
#include "llg2/llg2.h"
#include "brown/brown.h"
#include "util/util.h"
#endif

#ifdef EBM
#include "ebm/ebm.h"
#endif

/* prototype for function Solve below */
int Solve(GridData *gdata);

int main(int argc,char **args)
{
  int ierr;
  GridData gdata;

  ierr = PetscInitialize(
    &argc,&args,
    "allopt.txt",
    "magpar - parallel micromagnetics package\n"
  );CHKERRQ(ierr);
#ifdef TAO
  TaoInitialize(&argc,&args,(char *)0,"magpar - parallel micromagnetics package\n");
#endif

#ifdef PYTHON
  Py_Initialize();

  /* import python script/module */
#define NEWPYTHONIMPORT
#ifdef NEWPYTHONIMPORT
  gdata.pModule = PyImport_ImportModule(MAGPARSTDPYTHONMOD); /* new reference */
#else
  PyObject *pName;
  pName = PyString_FromString(MAGPARSTDPYTHONMOD); /* new reference */
  gdata.pModule = PyImport_Import(pName);          /* new reference */
#endif
  if(!gdata.pModule) {
    PetscPrintf(PETSC_COMM_WORLD,"Could not import Python module %s.py. The module file needs to be in PYTHONPATH!\n",MAGPARSTDPYTHONMOD);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"Imported Python module %s.py.\n",MAGPARSTDPYTHONMOD);
  }
#endif

  PetscLogDouble t_t1,t_t2;
  PetscPrintf(PETSC_COMM_WORLD,">> %s::%s\n",__FILE__,__FUNCT__);
  PetscGetTime(&t_t1);

  PetscPrintf(PETSC_COMM_WORLD,
    "\nmagpar is distributed under the terms of the GNU General Public License\n"
    "Copyright (C) 2002-2010 Werner Scholz\n"
  );

#include "magpar_version.h"
#include "magpar_revision.h"
  PetscPrintf(PETSC_COMM_WORLD,
    "--------------------------------------------------------------------------\n\n"
    "magpar version %s build %s\n",
    MAGPAR_VERSION,MAGPAR_REVISION
  );

  /* Turns on debugging for the memory management routines.
     CHKMEMQ calls below:
     Checks the memory for corruption, calls error handler if any is detected.
  */
  ierr = PetscMallocDebug(PETSC_TRUE);CHKERRQ(ierr);

  ierr = SerInit(&gdata);CHKERRQ(ierr);CHKMEMQ;

  ierr = ParInit(&gdata);CHKERRQ(ierr);CHKMEMQ;

  /* Turns off debugging for the memory management routines. */
  ierr = PetscMallocDebug(PETSC_FALSE);CHKERRQ(ierr);

  ierr = Solve(&gdata);CHKERRQ(ierr);

  PetscGetTime(&t_t2);
  PetscPrintf(PETSC_COMM_WORLD,
    "<< %s::%s took %g s = %g h\n",
    __FILE__,__FUNCT__,t_t2-t_t1,(t_t2-t_t1)/3600
  );

#ifdef PYTHON
#ifndef NEWPYTHONIMPORT
  Py_DECREF(pName);
#endif
  if (gdata.pModule) {
    Py_DECREF(gdata.pModule);
  }

  Py_Finalize();
#endif

  PetscFinalize();
}


int keepsolving(GridData *gdata)
{
  MagparFunctionInfoBegin;

  /* initialize upon first call */
  static PetscReal jfinal=PETSC_MIN;
  static PetscReal mfinal[ND]={PETSC_MIN,PETSC_MIN,PETSC_MIN};
  static PetscReal tstop=PETSC_MIN;

#ifdef PETSC_USE_SINGLE
  /* single precision needs a cast
     there seems to be an issue with the definition of PETSC_MIN and single precision
  */
  if (jfinal==(PetscReal)PETSC_MIN) {
#else
  if (jfinal==PETSC_MIN) {
#endif
    PetscTruth flg;
    ierr = PetscOptionsGetReal(PETSC_NULL,"-jfinal",&jfinal,&flg);CHKERRQ(ierr);
    if (flg!=PETSC_TRUE) {
      jfinal=-0.95;
      PetscPrintf(PETSC_COMM_WORLD,
        "Option -jfinal not found, using default value: %g\n",
        jfinal
      );
    }
    PetscPrintf(PETSC_COMM_WORLD,"jfinal: %g (1)\n",jfinal);

    int nmax = ND;
    ierr = PetscOptionsGetRealArray(PETSC_NULL,"-mfinal",mfinal,(PetscInt*)&nmax,&flg);CHKERRQ(ierr);

    if (flg!=PETSC_TRUE) {
      PetscPrintf(PETSC_COMM_WORLD,
        "Option -mfinal not found, using default value: %g,%g,%g\n",
        mfinal[0],mfinal[1],mfinal[2]
      );
      nmax=ND;
    }
    if (nmax!=ND){
      SETERRQ(PETSC_ERR_ARG_INCOMP,
        "Wrong number of values for option -mfinal!\n");
    }
    PetscPrintf(PETSC_COMM_WORLD,"mfinal: %g,%g,%g (1)\n",mfinal[0],mfinal[1],mfinal[2]);

    ierr = PetscOptionsGetReal(PETSC_NULL,"-ts_max_time",&tstop,&flg);CHKERRQ(ierr);
    if (flg!=PETSC_TRUE) {
      if (gdata->mode==0 || gdata->mode==2 || gdata->mode==3) {
        tstop=5;
      }
      else {
        tstop=PETSC_MAX;
      }
      PetscPrintf(PETSC_COMM_WORLD,
        "Option -ts_max_time not found, using default value: %g\n",
        tstop
      );
    }
    tstop /= 1e9*gdata->tscale;
    PetscPrintf(PETSC_COMM_WORLD,"reduced end time: %g (1)\n",tstop);
  }


  /* write inp file regularly (for easy restart):
     check if 2000 s (=33 min) have passed since last checkpoint file
     and set flag
  */
  int checkpoint=0;

  static PetscLogDouble inp_last_CPUt=PETSC_MIN;
  ierr = PetscGetTime(&t_t2);CHKERRQ(ierr);
  if (t_t2-inp_last_CPUt > 2000.0) {
    checkpoint=1;
  }
  /* broadcast the flag from the first processor to all others;
     this is necessary because each processors gets a slightly different
     result and they they might part ways here and hang
  */
  ierr = MPI_Bcast(&checkpoint,1,MPI_INT,0,PETSC_COMM_WORLD);CHKERRQ(ierr);

  /* write checkpoint file if necessary */
  if (checkpoint) {
    int  oldinp;

    oldinp=gdata->inp;
    gdata->inp=9999;

    /* is t_t2 an int? */
    PetscPrintf(PETSC_COMM_WORLD,
      "Writing checkpoint file (9999) at: %g ns\n",
      gdata->time*gdata->tscale*1e9
    );
    ierr = WriteSet(gdata);CHKERRQ(ierr);

    gdata->inp=oldinp;
    inp_last_CPUt=t_t2;

    checkpoint=0;
  }

  int exitcode=1;   /* by default return true */

  PetscReal valMpHext;
  ierr = MpHext(gdata->M,&valMpHext);CHKERRQ(ierr);
  if (valMpHext < jfinal) {
    PetscPrintf(PETSC_COMM_WORLD,
      "J//Hext %g <= jfinal %g\n",
      valMpHext,jfinal
    );
    exitcode=0;
  }

  PetscReal Mtot[ND];
  ierr = Vec3VolAvg(gdata->M,Mtot);CHKERRQ(ierr);

  for (int i=0;i<ND;i++) {
    if (Mtot[i] < mfinal[i]) {
      PetscPrintf(PETSC_COMM_WORLD,
        "M_%i %g <= mfinal\n",
        i,Mtot[i]
      );
      exitcode=0;
    }
  }

  if (gdata->time > tstop) {
    PetscPrintf(PETSC_COMM_WORLD,
      "gdata->time %g ns >= tstop %g ns\n",
      gdata->time*gdata->tscale*1e9,tstop*gdata->tscale*1e9
    );
    exitcode=0;
  }

  /* broadcast exit signal to all processors */
  ierr = MPI_Bcast(&gdata->mode,1,MPI_INT,0,PETSC_COMM_WORLD);CHKERRQ(ierr);

  MagparFunctionInfoReturn(exitcode);
}


int Solve(GridData *gdata)
{
  MagparFunctionLogBegin;

  PetscPrintf(PETSC_COMM_WORLD,
    "--------------------------------------------------------------------------\n"
    "Entering solution loop\n\n"
  );

  while (keepsolving(gdata) && gdata->mode!=99) {
    switch (gdata->mode) {
      case 0:
#ifdef SUNDIALS_VERSION
        ierr = myTSStepPVode(gdata);CHKERRQ(ierr);

        /* necessary to recalculate Htot, Energy ? */
        /* does not seem so (compared .log files with and without these calls)
           error is sometimes in the last digit
           skip it - saves time!
           further simulation does not depend on it anyway,
           only output in .log file is a little inaccurate

        ierr = Htot(gdata);CHKERRQ(ierr);
        */

        ierr = Htot_Energy(gdata);CHKERRQ(ierr);
        ierr = CheckIterationLLG(gdata);CHKERRQ(ierr);
#endif
      break;
      case 1:
#ifdef TAO
        ierr = EminiSolve(gdata);CHKERRQ(ierr);
        ierr = CheckIterationEmini(gdata);CHKERRQ(ierr);
#if 0
        /* hack for reader sensitivity function */
        gdata->mode=1;
#endif
#endif
      break;
      case 2:
      case 3:
#ifdef SUNDIALS_VERSION
        ierr = myTSStepPVode(gdata);CHKERRQ(ierr);
        ierr = Htot_Energy(gdata);CHKERRQ(ierr);
        ierr = CheckIterationLLG(gdata);CHKERRQ(ierr);
#endif
      break;
#ifdef ADDONS
      case 4:
        ierr = myTSStep2(gdata);CHKERRQ(ierr);
        ierr = Htot_Energy(gdata);CHKERRQ(ierr);
        ierr = CheckIterationLLG2(gdata);CHKERRQ(ierr);
      break;
#endif
#ifdef EBM
      case 50:
        ierr = myTSStepPVodeEBM(gdata);CHKERRQ(ierr);
        ierr = CheckIterationEBM(gdata);CHKERRQ(ierr);
        gdata->equil=0;
      break;
#endif
#ifdef ADDONS
      case 51:
        ierr = myTSStep(gdata);CHKERRQ(ierr);
        ierr = Htot_Energy(gdata);CHKERRQ(ierr);
        ierr = CheckIterationLLGTS(gdata);CHKERRQ(ierr);
      break;
      case 52:
        ierr = BrownSolve(gdata);CHKERRQ(ierr);
      break;
      case 53:
#ifdef TAO
        ierr = EminiSolve(gdata);CHKERRQ(ierr);
        ierr = CheckIterationEmini(gdata);CHKERRQ(ierr);
#endif
      break;
#endif
      default:
        SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"Unknown mode selected! Exiting!\n");
      break;
    }
  }

  PetscGetTime(&t_t2);
  PetscPrintf(PETSC_COMM_WORLD,
    "--------------------------------------------------------------------------\n"
    "Finished solution loop in %g s = %g h\n\n",
    t_t2-t_t1,(t_t2-t_t1)/3600
  );

  /* make sure that we save the final state
     this way we have a duplicate of the last log line
     but otherwise we would miss the inp number in the log file
  */
  gdata->inp=PetscAbsInt(gdata->inp);
  ierr = WriteLog(gdata);CHKERRQ(ierr);
  ierr = WriteSet(gdata);CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}

