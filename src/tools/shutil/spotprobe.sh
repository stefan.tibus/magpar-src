#!/bin/bash

if [ $# -ne 2 ]; then
  echo "Syntax: $0 <simName>"
  echo "  <simName>: simulation/project name without any suffix"
  echo "  <id>:      id of the node on the sampling line"
  echo "             (get it from <simName>.datmsh)"
  exit
fi

ls $1.????.d | awk '{print " # " $1}' > $1.d.list
grep -h "^   $2" $1.????.d > $1.d.$2

grep -v "    0    0" $1.log | \
grep -v "\#" | \
awk '{print $3 "  " $2}' > $1.d.t

echo "#  time (ns) inp       spot id           data... # data file name" > $1.d."$2"t
paste $1.d.t $1.d.$2 $1.d.list >> $1.d."$2"t

rm $1.d.list
rm $1.d.t
rm $1.d.$2
