/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: distpointline.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "util.h"

/* Weisstein, Eric W. "Point-Line Distance--3-Dimensional." From MathWorld--A Wolfram Web Resource.
 * http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
 */
int DistPointLine(PetscReal *point,PetscReal *linepoint,PetscReal *linevec,PetscReal *dist)
{
  PetscReal x0[ND],x1[ND],x2[ND];

  my_dcopy(ND,point,1,x0,1);
  my_dcopy(ND,linepoint,1,x1,1);

  my_dcopy(ND,linepoint,1,x2,1);
  my_daxpy(ND,1.0,linevec,1,x2,1);

  PetscReal d01[ND],d02[ND],d12[ND];

  my_dcopy(ND,x1,1,d01,1);
  my_daxpy(ND,-1.0,x0,1,d01,1);

  my_dcopy(ND,x2,1,d02,1);
  my_daxpy(ND,-1.0,x0,1,d02,1);

  my_dcopy(ND,x2,1,d12,1);
  my_daxpy(ND,-1.0,x1,1,d12,1);

  PetscReal t[ND];
  douter(ND,d01,d02,t);

  PetscReal n1,n2;
  n1=my_dnrm2(ND,t,1);
  n2=my_dnrm2(ND,d12,1);
  if (n1<D_EPS) {
    *dist=0.0;
  }
  else {
    *dist=n1/n2;
  }

  return(0);
}

