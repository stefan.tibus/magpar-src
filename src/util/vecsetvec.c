/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: vecsetvec.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today$\n\n";

#include "util.h"

int VecSetVec(Vec vec, PetscReal *vec3, int dim)
{
  MagparFunctionInfoBegin;

  int vsize;
  ierr = VecGetLocalSize(vec,&vsize);CHKERRQ(ierr);
  if (vsize%dim!=0) {
    SETERRQ3(PETSC_ERR_ARG_CORRUPT,
      "vsize=%i, dim=%i, %i!=0!\n",
      vsize,dim,vsize%dim
    );
  }

  PetscReal *ta_vec;
  ierr = VecGetArray(vec,&ta_vec);CHKERRQ(ierr);
  for (int j=0; j<vsize/dim; j++) {
    for (int i=0; i<dim; i++) {
      ta_vec[dim*j+i]=vec3[i];
    }
  }
  ierr = VecRestoreArray(vec,&ta_vec);CHKERRQ(ierr);

  MagparFunctionInfoReturn(0);
}

