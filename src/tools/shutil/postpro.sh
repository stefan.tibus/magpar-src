#!/bin/bash

if [ $# != 1 ]; then
  echo "Syntax: $0 proj_name"
   exit 0
fi

proj=$1

mkdir -p img0
mkdir -p img1
mkdir -p img2
mkdir -p inp
mkdir -p d
mkdir -p fld

for myi in `seq 0 9` ; do
  echo "$myi..."
  mv -i $proj.$myi???.0.* img0/
  mv -i $proj.$myi???.1.* img1/
  mv -i $proj.$myi???.2.* img2/
  mv -i $proj.$myi???.inp inp/
  mv -i $proj.$myi???.vtu inp/
  mv -i $proj.$myi???.gz  inp/
  mv -i $proj.$myi???.d   d/
  mv -i Writefld.*.d      fld/
done

exit

