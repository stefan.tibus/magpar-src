/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: writepng.c 2987 2010-03-01 17:45:38Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";


#include "writepng.h"

/* for getpwuid
EXTERN_C_BEGIN
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>
EXTERN_C_END
*/


int WritePNGfile(char *file_name, int width, int height, png_bytepp image, char *desc)
{
  FILE *fp;
  png_structp  png_ptr;
  png_infop    info_ptr;
  png_text     text_ptr[3];
  int          bit_depth=8;
  png_color_16 vtrans_values;

/*
  struct passwd       *t_passwd;
*/

  MagparFunctionInfoBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  if (rank) {
    MagparFunctionInfoReturn(0);
  }

  /* code template taken from example.c supplied with libpng */

  /* open the file */
  fp = fopen(file_name, "wb");
  if (fp == NULL)
     return (1);

  /* Create and initialize the png_struct with the desired error handler
   * functions.  If you want to use the default stderr and longjump method,
   * you can supply NULL for the last three parameters.  We also check that
   * the library version is compatible with the one used at compile time,
   * in case we are using dynamically linked libraries.  REQUIRED.
   */
  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
     NULL, NULL, NULL);

  if (png_ptr == NULL)
  {
     fclose(fp);
     return (1);
  }

  /* Allocate/initialize the image information data.  REQUIRED */
  info_ptr = png_create_info_struct(png_ptr);
  if (info_ptr == NULL)
  {
     fclose(fp);
     png_destroy_write_struct(&png_ptr, NULL);
     return (1);
  }

  /* Set error handling.  REQUIRED if you aren't supplying your own
   * error handling functions in the png_create_write_struct() call.
   */
  if (setjmp(png_jmpbuf(png_ptr)))
  {
     /* If we get here, we had a problem reading the file */
     fclose(fp);
     png_destroy_write_struct(&png_ptr, &info_ptr);
     return (1);
  }

  /* One of the following I/O initialization functions is REQUIRED */
  /* set up the output control if you are using standard C streams */
  png_init_io(png_ptr, fp);

  /* This is the hard way */

  /* Set the image information here.  Width and height are up to 2^31,
   * bit_depth is one of 1, 2, 4, 8, or 16, but valid values also depend on
   * the color_type selected. color_type is one of PNG_COLOR_TYPE_GRAY,
   * PNG_COLOR_TYPE_GRAY_ALPHA, PNG_COLOR_TYPE_PALETTE, PNG_COLOR_TYPE_RGB,
   * or PNG_COLOR_TYPE_RGB_ALPHA.  interlace is either PNG_INTERLACE_NONE or
   * PNG_INTERLACE_ADAM7, and the compression_type and filter_type MUST
   * currently be PNG_COMPRESSION_TYPE_BASE and PNG_FILTER_TYPE_BASE. REQUIRED
   */
  png_set_IHDR(png_ptr, info_ptr, width, height, bit_depth, PNG_COLOR_TYPE_RGB,
     PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);


/*
  t_passwd=getpwuid(geteuid());
*/
  /* Optionally write comments into the image */
  text_ptr[0].key = (png_charp)"Title";
  text_ptr[0].text = file_name;
  text_ptr[0].compression = PNG_TEXT_COMPRESSION_NONE;
  text_ptr[1].key = (png_charp)"Author";
  text_ptr[1].text = (png_charp)"unknown";
/*
  text_ptr[1].text = t_passwd->pw_name;
*/
  text_ptr[1].compression = PNG_TEXT_COMPRESSION_NONE;
  text_ptr[2].key = (char*)"Description";
  text_ptr[2].text = desc;
  text_ptr[2].compression = PNG_TEXT_COMPRESSION_zTXt;
  png_set_text(png_ptr, info_ptr, text_ptr, 3);

  /* other optional chunks like cHRM, bKGD, tRNS, tIME, oFFs, pHYs, */
  /* note that if sRGB is present the gAMA and cHRM chunks must be ignored
   * on read and must be written in accordance with the sRGB profile */

  vtrans_values.red=  255;
  vtrans_values.green=255;
  vtrans_values.blue= 255;
  png_set_tRNS(png_ptr, info_ptr, NULL, 1, &vtrans_values);

  /* Write the file header information.  REQUIRED */
  png_write_info(png_ptr, info_ptr);

  /* If you want, you can write the info in two steps, in case you need to
   * write your private chunk ahead of PLTE:
   *
   *   png_write_info_before_PLTE(write_ptr, write_info_ptr);
   *   write_my_chunk();
   *   png_write_info(png_ptr, info_ptr);
   *
   * However, given the level of known- and unknown-chunk support in 1.1.0
   * and up, this should no longer be necessary.
   */

  /* Once we write out the header, the compression type on the text
   * chunks gets changed to PNG_TEXT_COMPRESSION_NONE_WR or
   * PNG_TEXT_COMPRESSION_zTXt_WR, so it doesn't get written out again
   * at the end.
   */

  /* set up the transformations you want.  Note that these are
   * all optional.  Only call them if you want them.
   */

  /* The easiest way to write the image (you may have a different memory
   * layout, however, so choose what fits your needs best).  You need to
   * use the first method if you aren't handling interlacing yourself.
   */

  /* One of the following output methods is REQUIRED */
  png_write_image(png_ptr, image);

  /* You can write optional chunks like tEXt, zTXt, and tIME at the end
   * as well.  Shouldn't be necessary in 1.1.0 and up as all the public
   * chunks are supported and you can use png_set_unknown_chunks() to
   * register unknown chunks into the info structure to be written out.
   */

  /* It is REQUIRED to call this to finish writing the rest of the file */
  png_write_end(png_ptr, info_ptr);

  /* clean up after the write, and free any memory allocated */
  png_destroy_write_struct(&png_ptr, &info_ptr);

  /* close the file */
  fclose(fp);


  MagparFunctionInfoReturn(0);
}

