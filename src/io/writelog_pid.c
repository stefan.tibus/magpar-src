/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: writelog_pid.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "magpario.h"
#include "util/util.h"
#include "field/field.h"

#ifdef ADDONS
#include "addons/addons.h"
#endif

#ifdef SUNDIALS_VERSION
#include "llg/llg.h"
#endif

static int     doinit=1;
static int     fieldon=0;

static FILE    **logfile; /**< pointer to the logfile */
static Vec     Mpid,Mbak;
static Vec     Hpid;
static Vec     Xcomp;
static Vec     *maskpidvol;
static Vec     *maskpidone;
static PetscReal *volpid;

int WriteLogPidInit(GridData *gdata)
{
  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  PetscTruth flg;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-logpid",(PetscInt*)&fieldon,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    fieldon=0;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -logpid not found, using default value: %i\n",
      fieldon
    );
  }

  if (gdata->n_prop==1) {
    PetscPrintf(PETSC_COMM_WORLD,"Only %i pid found\n",gdata->n_prop);
    fieldon=0;
  }

  if (!fieldon) {
    PetscPrintf(PETSC_COMM_WORLD,"Not storing *.log_pid files\n");
    MagparFunctionLogReturn(0);
  }

  PetscPrintf(PETSC_COMM_WORLD,"Creating *.log_pid files\n");

  ierr = VecDuplicate(gdata->M,&Mpid);CHKERRQ(ierr);
  ierr = VecDuplicate(gdata->M,&Mbak);CHKERRQ(ierr);

  ierr = VecDuplicate(gdata->M,&Hpid);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&Xcomp);CHKERRQ(ierr);
  ierr = VecSetSizes(Xcomp,gdata->ln_vert,gdata->n_vert);CHKERRQ(ierr);
  ierr = VecSetFromOptions(Xcomp);CHKERRQ(ierr);

  ierr = PetscMalloc(gdata->n_prop*sizeof(FILE*),&logfile);CHKERRQ(ierr);
  ierr = PetscMalloc(gdata->n_prop*sizeof(Vec),&maskpidvol);CHKERRQ(ierr);
  for (int i=0;i<gdata->n_prop;i++) {
    ierr = VecDuplicate(gdata->M,&maskpidvol[i]);CHKERRQ(ierr);
  }
  PetscReal *ta_evol,*ta_vvol;
  VecGetArray(gdata->elevol,&ta_evol);
  for (int i=0;i<gdata->ln_ele;i++) {
    for (int l=0;l<NV;l++) {
      for (int k=0;k<ND;k++) {
        ierr = VecSetValue(
          maskpidvol[gdata->eleprop[i]],
          ND*gdata->elevert[NV*i+l]+k,
          ta_evol[i]/NV,
          ADD_VALUES
        );CHKERRQ(ierr);
       }
    }
  }
  VecRestoreArray(gdata->elevol,&ta_evol);

  ierr = PetscMalloc(gdata->n_prop*sizeof(PetscReal),&volpid);CHKERRQ(ierr);
  for (int i=0;i<gdata->n_prop;i++) {
    ierr = VecAssemblyBegin(maskpidvol[i]);CHKERRQ(ierr);
    ierr = VecAssemblyEnd(maskpidvol[i]);CHKERRQ(ierr);
    /* can use NORM_1 because all volumes are positive */
    VecStrideNorm(maskpidvol[i],0,NORM_1,&volpid[i]);
    PetscPrintf(PETSC_COMM_WORLD,"pid: %i  volume: %g\n",i,volpid[i]);
  }

  ierr = PetscMalloc(gdata->n_prop*sizeof(Vec),&maskpidone);CHKERRQ(ierr);
  for (int i=0;i<gdata->n_prop;i++) {
    PetscReal *ta_mpv;

    ierr = VecDuplicate(gdata->M,&maskpidone[i]);CHKERRQ(ierr);
    ierr = VecCopy(maskpidvol[i],maskpidone[i]);CHKERRQ(ierr);
    VecGetArray(maskpidone[i],&ta_mpv);
    VecGetArray(gdata->vertvol,&ta_vvol);
    for (int j=0;j<gdata->ln_vert;j++) {
      for (int k=0;k<ND;k++) {
        ta_mpv[ND*j+k]/=ta_vvol[j];
      }
    }
    VecRestoreArray(maskpidone[i],&ta_mpv);
    VecRestoreArray(gdata->vertvol,&ta_vvol);

/*
    PetscReal v;
    VecStrideNorm(maskpidone[i],0,NORM_1,&v);
    PetscPrintf(PETSC_COMM_WORLD,"pid: %i  one: %g\n",i,v);
*/
  }

  if (rank) {
    MagparFunctionLogReturn(0);
  }

  for (int i=0;i<gdata->n_prop;i++) {
    /* skip if volume is zero */
    if (volpid[i]<D_EPS) {
      PetscPrintf(PETSC_COMM_WORLD,"volume of pid: %i is zero, skipping output\n",i);
      continue;
    }

    char fmesh[256];
    ierr = PetscSNPrintf(fmesh,255,"%s%s%03i",gdata->simname,".log_",i+1);CHKERRQ(ierr);
    ierr = PetscFOpen(PETSC_COMM_WORLD,fmesh,"a",&logfile[i]);CHKERRQ(ierr);
    if (!logfile[i]) {
      SETERRQ1(PETSC_ERR_FILE_OPEN,"Cannot open %s\n",fmesh);
    }
    PetscPrintf(PETSC_COMM_WORLD,"Opening file %s\n",fmesh);

    PetscGetDate(fmesh,255);
    ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile[i],"#.date: %s\n",fmesh);
    ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile[i],"#.volume_with_pid:%i\n",i+1);
    ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile[i],"#.volume:%g\n",volpid[i]);

    /*      1    2    3    4    5    6    7    8    9   10   11   12   13 */
    ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile[i],
      "#.%14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s\n",
      "............1:",
      " 2:",
      " 3:",
      " 4:",
      " 5:",
      " 6:",
      " 7:",
      " 8:",
      " 9:",
      "10:",
      "11:",
      "12:",
      "13:"
    );CHKERRQ(ierr);
    /*      1    2    3    4    5    6    7    8    9   10   11   12   13 */
    ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile[i],
      "#.%14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s\n",
      "..........time",
      "Etot",
      "Mavg_x",
      "Mavg_y",
      "Mavg_z",
      "|Mavg|",
      "Havg_x",
      "Havg_y",
      "Havg_z",
      "Edem",
#ifdef EXCH
      "Eanionly",
#else
      "Eexchani",
#endif
      "Eext",
#ifdef EXCH
      "Eexchonly"
#else
       "-"
#endif
    );CHKERRQ(ierr);
    /*      1    2    3    4    5    6    7    8    9   10 */
    ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile[i],
      "#.%14s %14s %14s %14s %14s %14s %14s %14s %14s %14s\n",
      /* "time",      */ "..........(ns)",
      /* "Etot",      */ "(J/m^3)",
      /* "Mavg_x",    */ "(1)",
      /* "Mavg_y",    */ "(1)",
      /* "Mavg_z",    */ "(1)",
      /* "|Mavg|",    */ "(1)",
      /* "Havg_x",    */ "(T)",
      /* "Havg_y",    */ "(T)",
      /* "Havg_z",    */ "(T)",
      /* "Edem",      */ "(J/m^3)",
      /* "Eexchani",  */ "(J/m^3)",
      /* "Eext",      */ "(J/m^3)",
#ifdef EXCH
      /* "Eexchonly", */ "(J/m^3)"
#else
      /* "void",      */ "-"
#endif
    );CHKERRQ(ierr);
  }

  MagparFunctionLogReturn(0);
}


int WriteLogPid(GridData *gdata)
{
  MagparFunctionInfoBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  if (doinit) {
    ierr = WriteLogPidInit(gdata);CHKERRQ(ierr);
    doinit=0;
  }
  if (!fieldon) {
    MagparFunctionInfoReturn(0);
  }

  VecCopy(gdata->M,Mbak);

  for (int i=0;i<gdata->n_prop;i++) {
    /* skip if volume is zero */
    if (volpid[i]<D_EPS) continue;

    ierr = VecPointwiseMult(Mpid,Mbak,maskpidvol[i]);CHKERRQ(ierr);

    /* calculate Mavg (ignoring Js) */
    PetscReal Mavg[ND];
    for (int j=0;j<ND;j++) {
      /* cannot use VecStrideNorm with NORM_1 because values are pos. and neg.! */
      ierr = VecStrideGather(Mpid,j,Xcomp,INSERT_VALUES);CHKERRQ(ierr);
      ierr = VecSum(Xcomp,&Mavg[j]);CHKERRQ(ierr);
    }
    my_dscal(ND,1.0/volpid[i],Mavg,1);

    ierr = VecPointwiseMult(Hpid,gdata->VHtot,maskpidvol[i]);CHKERRQ(ierr);

    /* calculate Havg */
    PetscReal Havg[ND];
    for (int j=0;j<ND;j++) {
      ierr = VecStrideGather(Hpid,j,Xcomp,INSERT_VALUES);CHKERRQ(ierr);
      ierr = VecSum(Xcomp,&Havg[j]);CHKERRQ(ierr);
    }
    my_dscal(ND,1.0/volpid[i],Havg,1);

    ierr = VecPointwiseMult(Mpid,Mbak,maskpidone[i]);CHKERRQ(ierr);
    VecCopy(Mpid,gdata->M);
    Htot_Energy(gdata);

/*          1    2    3    4    5    6    7    8    9   10   11   12   13 */
    ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile[i],
      "  %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e\n",
      /* "time",      "(ns)",    */ gdata->time*gdata->tscale*1e9,
      /* "Etot",      "(J/m^3)", */ gdata->Etot*gdata->escale*gdata->totvol/volpid[i],
      /* "Mavg_x",    "(1)",     */ Mavg[0],
      /* "Mavg_y",    "(1)",     */ Mavg[1],
      /* "Mavg_z",    "(1)",     */ Mavg[2],
      /* "|Mavg|",    "(1)",     */ sqrt(Mavg[0]*Mavg[0]+Mavg[1]*Mavg[1]+Mavg[2]*Mavg[2]),
      /* "Havg_x",    "(1)",     */ Havg[0],
      /* "Havg_y",    "(1)",     */ Havg[1],
      /* "Havg_z",    "(1)",     */ Havg[2],
      /* "Edem",      "(J/m^3)", */ gdata->Edem*gdata->escale*gdata->totvol/volpid[i],
#ifdef EXCH
      /* "Eanionly",  "(J/m^3)", */ (gdata->Eexchani-gdata->Eexchonly)*gdata->escale*gdata->totvol/volpid[i],
#else
      /* "Eexchani",  "(J/m^3)", */ gdata->Eexchani*gdata->escale*gdata->totvol/volpid[i],
#endif
      /* "Eext",      "(J/m^3)", */ gdata->Eext*gdata->escale*gdata->totvol/volpid[i],
#ifdef EXCH
      /* "Eexchonly", "(J/m^3)", */ gdata->Eexchonly*gdata->escale*gdata->totvol/volpid[i]
#else
      /* "void",      "",        */ -1.0
#endif
    );
  }

  /* recalculate energy with correct M (otherwise wrong results at end of simulation) */
  VecCopy(Mbak,gdata->M);
  Htot_Energy(gdata);

  MagparFunctionInfoReturn(0);
}
