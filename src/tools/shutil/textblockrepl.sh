#!/bin/bash

if [ -z $1 ]; then
  flist=`find . -name "*"`
else
  flist=$@
fi

# for security
#exit

for f in $flist; do

echo "Processing $f... ----------------------------"

# skip if $f is not a regular file
if [ ! -f $f ]; then
  echo "not a regular file - skipped"
  continue
fi

# copy file to preserve attributes
cp $f $f.tmp



# replace strings
cat $f | \
sed "s/magpar.O_c++/magpar.exe/g" | \
cat > $f.tmp



# if nothing has changed retain original file
if cmp -s $f $f.tmp ; then
  echo "file unchanged"
  rm $f.tmp
  continue
fi

# check if file with size>0 has been created
if [ -s $f.tmp ]; then
  echo "file modified"
  diff $f $f.tmp
  mv $f.tmp $f
else
  # bail out
  echo "Created $f.tmp with size zero! Exiting!"
  exit
fi

done

exit

###########################################################
# backups, examples

awk 'RS="thisisnotaseparator" {gsub(" *int *ierr; *\n+","");print}' | \
awk 'RS="thisisnotaseparator" {gsub(" *int *rank,size,ierr;","");print}' | \
awk 'RS="thisisnotaseparator" {gsub("  ierr = MPI_Comm_size","  int rank,size;\n&");print}' | \
awk 'RS="thisisnotaseparator" {gsub("\n*$","") ;print}' | \
awk 'RS="thisisnotaseparator" {gsub("{\n+","{\n") ;print}' | \
awk 'RS="thisisnotaseparator" {gsub("#undef __FUNCT__\n","");print}' | \
awk 'RS="\n" {gsub("#define __FUNCT__ .*","");print}' | \
awk 'RS="thisisnotaseparator" {gsub("PetscScalar","PetscReal");print}' | \
awk 'RS="thisisnotaseparator" {gsub("ierr=","ierr = ");print}' | \
awk 'RS="thisisnotaseparator" {gsub("M_PI","PETSC_PI");print}' | \
awk 'RS="thisisnotaseparator" {gsub("\n+$","\n");print}' |

awk 'RS="thisisnotaseparator" {gsub("fabs\(","PetscAbsReal(");print}' | \
awk 'RS="thisisnotaseparator" {gsub("\n+$","\n");print}' |

sed "s/str1/str2/g" | \

replace
()"#\
