/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* $Id: magpario.h 2962 2010-02-04 19:50:44Z scholz $ */

#ifndef MAGPARIO_H
#define MAGPARIO_H

#include "griddata.h"

int ReadINP(GridData *gdata, char *fmesh, Vec Vdest, int col);
int read_one_line(FILE *fd, char *line, int bufsize, char **comment_out);
int ReadKrn(GridData *gdata);
int ReadMesh(GridData *gdata);
int ReadPatran(GridData *gdata);
int Vec3VolAvg(Vec v,PetscReal* res);
int WriteAVS(GridData *gdata);
int WriteDat(GridData *gdata);
int WriteSet(GridData *gdata);
int WriteDatinit(GridData *gdata);
int WriteFEMAVS(GridData *gdata);
int WriteLog(GridData *gdata);
int WriteLogPid(GridData *gdata);
int WriteMavg(GridData *gdata);

#endif
