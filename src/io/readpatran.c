/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: readpatran.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "magpario.h"

/* Reads in the grid data from a Patran Neutral file */

int ReadPatran(GridData *gdata)
{
  int          i;

  char         msg[256],fmesh[256];
  FILE         *fd;

  int          a1,a2,a3,a4;
  int          it, id, iv, kc, n1, n2, n3, n4, n5;


  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  if (rank) {
    gdata->ln_vert=0;
    gdata->ln_ele=0;

    MagparFunctionInfoReturn(0);
  }

  /* TODO: read filename directly from option */
  ierr = PetscSNPrintf(fmesh,255,"%s%s",gdata->simname,".out");CHKERRQ(ierr);
  ierr = PetscFOpen(PETSC_COMM_WORLD,fmesh,"r",&fd);CHKERRQ(ierr);
  if (!rank && !fd) {
    SETERRQ1(PETSC_ERR_FILE_OPEN,"Cannot open %s\n",fmesh);
  }
  PetscPrintf(PETSC_COMM_WORLD,"Opening file %s\n",fmesh);

  /* read in number of vertices */
  fscanf(fd,"%i %i %i %i %i %i %i %i %i\n",
    &it,&id,&iv,&kc,&n1,&n2,&n3,&n4,&n5
  );
  assert(it==25); /* packet type 25: title card */
  assert(id==0);
  assert(iv==0);
  assert(kc==1);

  fgets(msg,256,fd);
  PetscPrintf(PETSC_COMM_WORLD,"Title: %s\n",msg);

  fscanf(fd,"%i %i %i %i %i %i %i %i %i\n",
    &it,&id,&iv,&kc,&n1,&n2,&n3,&n4,&n5
  );
  assert(it==26); /* packet type 26: summary data */
  assert(id==0);
  assert(iv==0);
  assert(kc==1);

  gdata->n_vert=n1;
  gdata->ln_vert=gdata->n_vert;
  PetscPrintf(PETSC_COMM_WORLD,"Number of grid vertices: %i\n",gdata->n_vert);
  gdata->n_ele=n2;
  gdata->ln_ele=gdata->n_ele;
  PetscPrintf(PETSC_COMM_WORLD,"Number of grid elements: %i\n",gdata->n_ele);
  PetscPrintf(PETSC_COMM_WORLD,"Number of materials:     %i\n",n3);
  gdata->n_prop=n4;
  assert(gdata->n_prop>0);
  PetscPrintf(PETSC_COMM_WORLD,"Number of element prop.: %i\n",gdata->n_prop);
  PetscPrintf(PETSC_COMM_WORLD,"Number of coord. frames: %i\n",n5);

  fgets(msg,256,fd);
  PetscPrintf(PETSC_COMM_WORLD,"Neutral file creation date, Patran Version:\n%s\n",msg);

  /* *************************************************************************
     Read in all vertices
  */

  ierr = PetscMalloc(ND*gdata->n_vert*sizeof(PetscReal),&gdata->vertxyz);CHKERRQ(ierr);

  /* read data of all vertices */
  for (i=0; i<gdata->n_vert; i++) {
    fscanf(fd,"%i %i %i %i %i %i %i %i %i\n",
      &it,&id,&iv,&kc,&n1,&n2,&n3,&n4,&n5
    );
    assert(it==1); /* packet type 01: node data */
    assert(iv==0);
    assert(kc==2);

    /* assert ascending ordering without holes */
    assert(id-1==i);

#ifdef PETSC_USE_SINGLE
    fscanf(fd,"%f %f %f\n",
#else
    fscanf(fd,"%lf %lf %lf\n",
#endif
      gdata->vertxyz+ND*i+0,
      gdata->vertxyz+ND*i+1,
      gdata->vertxyz+ND*i+2
    );

    fgets(msg,256,fd);
  }

  /* *************************************************************************
     Read in all elements
  */
  ierr = PetscMalloc(NV*gdata->n_ele*sizeof(int),&gdata->elevert);CHKERRQ(ierr);
  ierr = PetscMalloc(gdata->n_ele*sizeof(int),&gdata->eleprop);CHKERRQ(ierr);

  for (i=0; i<gdata->n_ele; i++) {
    fscanf(fd,"%i %i %i %i %i %i %i %i %i\n",
      &it,&id,&iv,&kc,&n1,&n2,&n3,&n4,&n5
    );

    assert(it==2); /* packet type 02: element data */
    assert(kc==2);
    assert(n1==0);

    /* assume tet elements */
    assert(iv==5);

    /* assert ascending ordering without holes */

    /* does not work due to unknown offset in Patran's element numbers
     * and does not really matter anyway (element numbering not interesting)

    assert(t_cnt==id-2);
    t_cnt=id-1;
    */

    fscanf(fd,"%i %i %i %i",&a1,&a2,&a3,&a4); /* a3: property id */
    assert(a1==NV); /* number of nodes per element */

    /* internal property ids start with 0 !!! */
    gdata->eleprop[i]=--a3;
    assert(gdata->eleprop[i]>=0);
    assert(gdata->eleprop[i]<gdata->n_prop);

    fgets(msg,256,fd);
    fscanf(fd,"%i %i %i %i\n",
      gdata->elevert+i*NV+0,
      gdata->elevert+i*NV+1,
      gdata->elevert+i*NV+2,
      gdata->elevert+i*NV+3
    );
    gdata->elevert[i*NV+0]--;
    gdata->elevert[i*NV+1]--;
    gdata->elevert[i*NV+2]--;
    gdata->elevert[i*NV+3]--;
  }

  ierr = PetscFClose(PETSC_COMM_WORLD,fd);CHKERRQ(ierr);


  MagparFunctionLogReturn(0);
}

