/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: initinfo.c 3013 2010-03-26 16:12:31Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "magpar_version.h"
#include "init.h"

#ifdef METIS
EXTERN_C_BEGIN
#include "metis.h"
EXTERN_C_END
#endif

#ifdef SUNDIALS_VERSION
#include "llg/llg.h"
#endif

#ifdef PNG
#include "png.h"
#endif

#ifdef ZLIB
#include "zlib.h"
#endif

#ifdef ADDONS
#include "addons/addons.h"
#endif

int InitInfo()
{
  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  PetscPrintf(PETSC_COMM_WORLD,"\nmagpar compiled with...\n");
#ifdef __GNUC__
  PetscPrintf(PETSC_COMM_WORLD," - GNU C(++) version "__VERSION__"\n");
#endif
#if defined(_WIN32) || defined (__CYGWIN32__)
  PetscPrintf(PETSC_COMM_WORLD," - Cygwin environment\n");
#endif

  PetscPrintf(PETSC_COMM_WORLD," - assert checks ");
#ifdef NDEBUG
  PetscPrintf(PETSC_COMM_WORLD,"disabled (NDEBUG defined)\n");
#else
  PetscPrintf(PETSC_COMM_WORLD,"enabled (NDEBUG undefined)\n");
#endif

#ifdef UNIPROC
  PetscPrintf(PETSC_COMM_WORLD," - uniprocessor support only (by PETSc configuration)\n");
#else
  PetscPrintf(PETSC_COMM_WORLD," - MPI for parallelization on multiple processors\n");
#endif

  PetscPrintf(PETSC_COMM_WORLD," - X11 support ");
#ifdef PETSC_HAVE_X11
  PetscPrintf(PETSC_COMM_WORLD,"enabled for visualizing matrix structures\n");
#else
  PetscPrintf(PETSC_COMM_WORLD,"disabled\n");
#endif

  PetscPrintf(PETSC_COMM_WORLD," - EXCH ");
#ifdef EXCH
  PetscPrintf(PETSC_COMM_WORLD,"defined\n");
#else
  PetscPrintf(PETSC_COMM_WORLD,"undefined\n");
#endif

  PetscPrintf(PETSC_COMM_WORLD," - floating point data/operations using ");
#ifdef PETSC_USE_SINGLE
  PetscPrintf(PETSC_COMM_WORLD,"single precision\n");
  if (2*sizeof(PetscReal)!=sizeof(double)) PetscPrintf(PETSC_COMM_WORLD,"Warning: 2*sizeof(PetscReal)!=sizeof(double)\n");
#else
  PetscPrintf(PETSC_COMM_WORLD,"double precision\n");
  if (sizeof(PetscReal)!=sizeof(double)) PetscPrintf(PETSC_COMM_WORLD,"Warning: sizeof(PetscReal)!=sizeof(double)\n");
#endif
  PetscPrintf(PETSC_COMM_WORLD,"   (see also data type sizes below)\n");

#ifdef ADDONS
  PetscPrintf(PETSC_COMM_WORLD," - add-ons included\n");
  PetscPrintf(PETSC_COMM_WORLD," - dense boundary matrix and MF+ approximation\n");
#else
  PetscPrintf(PETSC_COMM_WORLD," - dense boundary matrix for demagnetizing field\n");
#endif

  PetscPrintf(PETSC_COMM_WORLD,"\nLinked libraries:\n");

  PetscPrintf(PETSC_COMM_WORLD," - PETSc    (version %i.%i.%ipl%i): core library (parallel data structures, solvers)\n",
    PETSC_VERSION_MAJOR,PETSC_VERSION_MINOR,PETSC_VERSION_SUBMINOR,PETSC_VERSION_PATCH);

#ifndef UNIPROC
  PetscPrintf(PETSC_COMM_WORLD," - MPI      (version %i.%i): message passing standard\n",
    MPI_VERSION,MPI_SUBVERSION);
#ifdef MPICH_VERSION
  PetscPrintf(PETSC_COMM_WORLD," - MPICH    (version %s): message passing library\n",
    MPICH_VERSION);
#elif defined(MPICH2)
  PetscPrintf(PETSC_COMM_WORLD," - MPICH    (version %i): message passing library\n",
    MPICH2);
#endif
#ifdef LAM_MPI
  PetscPrintf(PETSC_COMM_WORLD," - LAM/MPI  (version %s): message passing library\n",
    LAM_VERSION);
#endif
#endif

#ifdef METIS
  PetscPrintf(PETSC_COMM_WORLD," - ParMetis (version ");
#ifdef PARMETIS_MAJOR_VERSION
  PetscPrintf(PETSC_COMM_WORLD,"%i",PARMETIS_MAJOR_VERSION);
#endif
#ifdef PARMETIS_MINOR_VERSION
  PetscPrintf(PETSC_COMM_WORLD,".%i",PARMETIS_MINOR_VERSION);
#endif
#ifdef PARMETIS_SUBMINOR_VERSION
  PetscPrintf(PETSC_COMM_WORLD,".%i",PARMETIS_SUBMINOR_VERSION);
#endif
  PetscPrintf(PETSC_COMM_WORLD,"): mesh partitioning\n");
  PetscPrintf(PETSC_COMM_WORLD,"   using within:");
  PetscPrintf(PETSC_COMM_WORLD,METISTITLE);
#else
  PetscPrintf(PETSC_COMM_WORLD," - ParMetis not linked - mesh partitioning unavailable.\n");
#endif

#ifdef SUNDIALS_VERSION
#if SUNDIALS_VERSION < 230
#error "SUNDIALS versions lower/older than 2.3.0 are not supported"
#endif
  PetscPrintf(PETSC_COMM_WORLD," - SUNDIALS (version %s): time integration of LLG\n",
    SUNDIALS_PACKAGE_VERSION);
#else
  PetscPrintf(PETSC_COMM_WORLD," - PVODE time integration not available\n");
#endif

#ifdef TAO
  PetscPrintf(PETSC_COMM_WORLD," - TAO      (version %s): energy minimization\n",TAO_VERSION_NUMBER);
  PetscPrintf(PETSC_COMM_WORLD,"   This product includes software produced by UChicago Argonne, LLC\n");
  PetscPrintf(PETSC_COMM_WORLD,"   under Contract No. DE-AC02-06CH11357 with the Department of Energy.\n");
#else
  PetscPrintf(PETSC_COMM_WORLD," - TAO energy minimization not available\n");
#endif

#ifdef PNG
  PetscPrintf(PETSC_COMM_WORLD," - libpng   (version %s): png graphics output\n",
    PNG_LIBPNG_VER_STRING);
#else
  PetscPrintf(PETSC_COMM_WORLD," - PNG output not available\n");
#endif

#ifdef ZLIB
  PetscPrintf(PETSC_COMM_WORLD," - zlib     (version %s): compression of output files\n",ZLIB_VERSION);
#else
  PetscPrintf(PETSC_COMM_WORLD," - zlib compression not available\n");
#endif

#ifdef PETSC_HAVE_SUPERLU_DIST
  PetscPrintf(PETSC_COMM_WORLD," - SuperLU available for parallel direct LU solver\n");
#endif

#ifdef PETSC_HAVE_UMFPACK
  PetscPrintf(PETSC_COMM_WORLD," - UMFPACK available for sequential direct LU solver\n");
#endif

#ifdef PETSC_HAVE_HYPRE
  PetscPrintf(PETSC_COMM_WORLD," - hypre available for preconditioning\n");
#endif

#ifdef PYTHON
  const char *pythonversion;
  pythonversion=Py_GetVersion();
  PetscPrintf(PETSC_COMM_WORLD," - Python    version %s\n",pythonversion);
#endif

  PetscPrintf(PETSC_COMM_WORLD,
    "\ndata type sizes (bytes):\n"
    "int         %i\n"
    "PetscInt    %i\n"
    "double      %i\n"
    "PetscReal   %i\n"
    "size_t      %i\n"
    "D_EPS       %g\n",
    sizeof(int),
    sizeof(PetscInt),
    sizeof(double),
    sizeof(PetscReal),
    sizeof(size_t),
    D_EPS
  );

  PetscPrintf(PETSC_COMM_WORLD,"\nmagpar running with...\n");
  PetscTruth profile;
  PetscOptionsHasName(PETSC_NULL,"-profile",&profile);
  PetscPrintf(PETSC_COMM_WORLD," - profiling ");
  if (profile) {
    PetscPrintf(PETSC_COMM_WORLD,"enabled\n");
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"disabled\n");
  }
  PetscTruth info;
  PetscOptionsHasName(PETSC_NULL,"-info",&info);
  PetscPrintf(PETSC_COMM_WORLD," - PetscInfo (debugging,profiling output) ");
  if (info) {
    PetscPrintf(PETSC_COMM_WORLD,"enabled\n");
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"disabled\n");
  }
  PetscPrintf(PETSC_COMM_WORLD,"\n");


  PetscPrintf(PETSC_COMM_WORLD,
    "--------------------------------------------------------------------------\n"
    "magpar running on %i processor%s\n\n", size, size>1 ? "s" : ""
  );
  char hostname[256];
  ierr = PetscGetHostName(hostname,255);
  PetscSynchronizedPrintf(PETSC_COMM_WORLD,
    "Hello world from processor %i on host %s!\n",rank,hostname
  );
  PetscSynchronizedFlush(PETSC_COMM_WORLD);
  PetscPrintf(PETSC_COMM_WORLD,"\n");

  MagparFunctionLogReturn(0);
}
