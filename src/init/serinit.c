/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: serinit.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "init.h"
#include "io/magpario.h"

#ifdef ADDONS
#include "addons/addons.h"
#endif

int SerInit(GridData *gdata)
{
  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  /* print useful information about magpar itself, library versions, etc. */
  ierr = InitInfo();CHKERRQ(ierr);

  PetscPrintf(PETSC_COMM_WORLD,
    "--------------------------------------------------------------------------\n"
    "Starting single processor part\n"
  );

  /* read FE mesh */
  ierr = ReadMesh(gdata);CHKERRQ(ierr);

  /* read material properties */
  ierr = ReadKrn(gdata);CHKERRQ(ierr);

  /* modify material properties */
  ierr = ModifyPropSer(gdata);CHKERRQ(ierr);
#ifdef ADDONS
  ierr = ModifyPropSerGrains(gdata);CHKERRQ(ierr);
#endif

  /* initialize magnetization etc. */

#ifdef ADDONS
  /* FIXME: MagInit calls VertProp, which checks vertcouple
   * need to initialize before call to MagInit to avoid segv
   * better initialize all global variables (gdata struct) elsewhere! */
  gdata->vertcoupl=PETSC_NULL;
#endif

  /* we need to initialize the magnetization before we modify the mesh
     (i.e. filter elements and possibly remove elements and nodes)
     in case we read the magnetization from an inp file
  */
  ierr = MagInit(gdata);CHKERRQ(ierr);

#ifdef ADDONS
  ierr = Hext_in1_Init(gdata);CHKERRQ(ierr);
  ierr = Hext_in2_Init(gdata);CHKERRQ(ierr);
  ierr = FilterElements(&gdata->n_ele,&gdata->elevert,&gdata->eleprop,gdata->propdat);CHKERRQ(ierr);
  ierr = MeshMirror(&gdata->n_vert,&gdata->n_ele,&gdata->vertxyz,&gdata->elevert,&gdata->eleprop);CHKERRQ(ierr);
  ierr = FilterNodes(&gdata->n_vert,&gdata->n_ele,&gdata->vertxyz,gdata->elevert,&gdata->M,&gdata->VH1,&gdata->VH2);CHKERRQ(ierr);
  if (!rank) {
    gdata->ln_vert=gdata->n_vert;
    gdata->ln_ele =gdata->n_ele;
  }
#else
  Vec dum=PETSC_NULL;
  ierr = FilterElements(&gdata->n_ele,&gdata->elevert,&gdata->eleprop,gdata->propdat);CHKERRQ(ierr);
  ierr = FilterNodes(&gdata->n_vert,&gdata->n_ele,&gdata->vertxyz,gdata->elevert,&gdata->M,&dum,&dum);CHKERRQ(ierr);
  if (!rank) {
    gdata->ln_vert=gdata->n_vert;
    gdata->ln_ele =gdata->n_ele;
  }
#endif

  /* mesh refinement */
  ierr = RegularRefinement(gdata);CHKERRQ(ierr);

#ifdef ADDONS
  /* need to do after refinement due to coupling matrix */
  ierr = DecoupleGrains(gdata);CHKERRQ(ierr);
  /* TODO: cannot move behind Reorder (and avoid renumbering coupling array) because
     Reorder generates the gdata->vertl2g, elel2g arrays
     should be done separately, so that Reorder really only renumbers vertices and elements
  */
#endif

  /* Reorder/renumber nodes and elements */
  ierr = Reorder(gdata);CHKERRQ(ierr);

#ifdef ADDONS
  /* save coupling information after reordering */
  if (gdata->vertcoupl!=PETSC_NULL) {
    char str[256];
    ierr = PetscSNPrintf(str,255,"%s.%04i.%s",gdata->simname,gdata->inp,"vertcoupl");CHKERRQ(ierr);
    WriteVertcoupl(str,gdata->n_vert,gdata->vertcoupl);
  }
#endif

  /* find faces, neighbours, surface */
  ierr = FacNB(gdata);CHKERRQ(ierr);

  /* assign property ids to vertices */
  ierr = VertProp(gdata);CHKERRQ(ierr);

#ifdef ADDONS
  ierr = DataPartitionSurfSer(gdata);CHKERRQ(ierr);
  ierr = DataPartitionSurfSerMoveData(gdata);CHKERRQ(ierr);
#endif

  /* distort surface and/or interior mesh (introduce roughness) */
  ierr = DistortMesh(gdata);CHKERRQ(ierr);

  /* distribute data to processors */
  ierr = DataMoveData(gdata);CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}

