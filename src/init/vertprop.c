/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: vertprop.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "init.h"

int VertProp(GridData *gdata)
{
  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  if (rank) {
    MagparFunctionLogReturn(0);
  }

  /* initialize vertprop array */
  ierr = PetscMalloc(gdata->n_vert*sizeof(int),&gdata->vertprop);CHKERRQ(ierr);
  for (int i=0; i<gdata->n_vert; i++) {
    gdata->vertprop[i]=-1;
  }

  /* assign property ids to vertices: larger Ms wins */
  for (int i=0; i<gdata->n_ele; i++) {
    for (int j=0; j<NV; j++) {
      if (gdata->vertprop[gdata->elevert[NV*i+j]]==-1 ||
          gdata->propdat[NP*gdata->eleprop[i]+4]>
          gdata->propdat[NP*gdata->vertprop[gdata->elevert[NV*i+j]]+4]) {
        gdata->vertprop[gdata->elevert[NV*i+j]]=gdata->eleprop[i];
      }
    }
  }

  int *npidvert;
  ierr = PetscMalloc(gdata->n_prop*sizeof(int),&npidvert);CHKERRQ(ierr);
  for (int i=0; i<gdata->n_prop; i++) {
    npidvert[i]=0;
  }

  /* verify that all vertices have valid pids */
  for (int i=0; i<gdata->n_vert; i++) {
    if (gdata->vertprop[i]<0) {
      PetscPrintf(PETSC_COMM_WORLD,"Warning: node %i unused\n",i);
      gdata->vertprop[i]=0;
    }
    npidvert[gdata->vertprop[i]]++;
    assert(gdata->vertprop[i]<gdata->n_prop);
  }

  PetscInfo(0,"   pid |   n_vert\n");
  PetscInfo(0,"-----------------\n");
  for (int i=0; i<gdata->n_prop; i++) {
    PetscInfo2(0,"%6i | %8i\n",i,npidvert[i]);
  }

#ifdef ADDONS
  PetscInfo(0,"  vert |   pid    | v_coupl \n");
  PetscInfo(0,"----------------------------\n");
  for (int i=0; i<gdata->n_vert; i++) {
    if (gdata->vertcoupl!=PETSC_NULL) {
      PetscInfo3(0,"%6i | %8i | %8i\n",i,gdata->vertprop[i],gdata->vertcoupl[i]);
    }
    else {
      PetscInfo2(0,"%6i | %8i\n",i,gdata->vertprop[i]);
    }
  }
#endif

  ierr = PetscFree(npidvert);CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}
