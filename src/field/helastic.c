/*
    This file is part of magpar.

    Copyright (C) 2007 Ahmet Kaya
    Copyright (C) 2005-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: helastic.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "field.h"

static int       doinit=1;
static int       fieldon=0;
static PetscReal *rval;

#define NPE 6
static PetscReal *propdat;
static Vec       VHelastic;

int ReadProp(int n_prop)
{
  PetscTruth   flg;

  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  char str[256];
  ierr = PetscOptionsGetString(PETSC_NULL,"-helastic_propfile",str,256,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    SETERRQ1(PETSC_ERR_ARG_INCOMP,"Problem with option -helastic_propfile %s\n",str);
  }

  ierr = PetscMalloc(n_prop*NPE*sizeof(PetscReal),&propdat);CHKERRQ(ierr);

  if (!rank) {
    FILE *fd;
    ierr = PetscFOpen(PETSC_COMM_WORLD,str,"r",&fd);CHKERRQ(ierr);
    if (!fd) {
      SETERRQ1(PETSC_ERR_FILE_OPEN,"Cannot open %s\n",str);
    }
    PetscPrintf(PETSC_COMM_WORLD,"Opening file %s\n",str);

    for (int i=0; i<n_prop; i++) {
      for (int j=0; j<NPE; j++) {
        int k;
#ifdef PETSC_USE_SINGLE
        k=fscanf(fd,"%f",propdat+NPE*i+j);
#else
        k=fscanf(fd,"%lf",propdat+NPE*i+j);
#endif
        if (k!=1) {
          SETERRQ2(PETSC_ERR_FILE_UNEXPECTED,"Data in %s corrupt: %i!\n",str,j);
        }
      }
      /* read remainder of this line */
      fgets(str,256,fd);

      PetscPrintf(MPI_COMM_WORLD,"----- property: %i -----\n",i);
      PetscPrintf(MPI_COMM_WORLD,
        "texture:    %g\n"
        "lambda100:  %g\n"
        "lambda111:  %g\n"
        "sigmaX:     %g\n"
        "sigmaY:     %g\n"
        "sigmaZ:     %g\n",
        propdat[NPE*i+0],
        propdat[NPE*i+1],
        propdat[NPE*i+2],
        propdat[NPE*i+3],
        propdat[NPE*i+4],
        propdat[NPE*i+5]
      );
      assert(NPE==6);
    }

    ierr = PetscFClose(PETSC_COMM_WORLD,fd);CHKERRQ(ierr);
  }
  ierr = MPI_Bcast(propdat,NPE*n_prop,MPIU_SCALAR,0,PETSC_COMM_WORLD);CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}


int Helastic_Init(GridData *gdata)
{
  PetscTruth flg;

  MagparFunctionLogBegin;

  ierr = PetscOptionsHasName(PETSC_NULL,"-helastic_propfile",&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -helastic_propfile not found, using default value: %i (field off)\n",
      fieldon
    );
  }
  else {
    fieldon=1;
  }

  if (!fieldon) {
    PetscPrintf(PETSC_COMM_WORLD,"Helastic off\n");
    MagparFunctionLogReturn(0);
  }
  PetscPrintf(PETSC_COMM_WORLD,"Helastic on\n");

  /* read material properties from file */
  ierr = ReadProp(gdata->n_prop);CHKERRQ(ierr);

  ierr = VecDuplicate(gdata->M,&VHelastic);CHKERRQ(ierr);
  ierr = VecZeroEntries(VHelastic);CHKERRQ(ierr);


  /* create arrays of random numbers for random texture */

  ierr = PetscMalloc(ND*gdata->ln_vert*sizeof(PetscReal),&rval);CHKERRQ(ierr);

  PetscRandom r;
  ierr = PetscRandomCreate(PETSC_COMM_WORLD,&r);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(r);CHKERRQ(ierr);

  for (int i=0;i<gdata->ln_vert;i++) {
    PetscReal  value1,value2,value3,value4;
    PetscReal  ran_num1,ran_num2,ran_num3;
    PetscReal  ww;
    PetscReal  xx1,xx2,xx3,xx4;

    do {
      PetscRandomGetValue(r,&value1);
      PetscRandomGetValue(r,&value2);

      xx1 = 2.0*value1-1.0;
      xx2 = 2.0*value2-1.0;
      ww = xx1*xx1+xx2*xx2;
    } while (ww >= 1.0);

    ww=sqrt((-2.0*log(ww))/ww);
    ran_num1=xx1*ww;
    ran_num2=xx2*ww;

    do {
      PetscRandomGetValue(r,&value3);
      PetscRandomGetValue(r,&value4);

      xx3 = 2.0*value3-1.0;
      xx4 = 2.0*value4-1.0;
      ww = xx3*xx3+xx4*xx4;
    } while (ww >= 1.0);

    ww=sqrt((-2.0*log(ww))/ww);
    ran_num3 = xx3*ww;

    rval[ND*i+0]=(ran_num1+4.0)/(8.0);
    rval[ND*i+1]=(ran_num2+4.0)/(8.0);
    rval[ND*i+2]=(ran_num3+4.0)/(8.0);
  }

  PetscRandomDestroy(r);
  /* end of create random number */

  MagparFunctionLogReturn(0);
}


int Helastic(GridData *gdata,Vec VHtotsum)
{
  MagparFunctionInfoBegin;

  if (doinit) {
    ierr = Helastic_Init(gdata);CHKERRQ(ierr);
    doinit=0;
  }
  if (!fieldon) {
    MagparFunctionInfoReturn(0);
  }

  PetscReal *ta_Helastic,*ta_M;
  ierr = VecGetArray(VHelastic,&ta_Helastic);CHKERRQ(ierr);
  ierr = VecGetArray(gdata->M,&ta_M);CHKERRQ(ierr);

  /* TODO: for nodes at interfaces, the assignment is "at will"
     not any more: higher Ms wins
  */

  for (int j=0; j<gdata->ln_vert; j++) {
    int celltexture;
    PetscReal xA,xB,xC;

    if (propdat[NPE*gdata->vertprop[j]+0]<=0.0) continue;

    /* FIXME: safer to use round or rint?? */
    celltexture=(int)propdat[NPE*gdata->vertprop[j]+0];
    switch (celltexture) {
      case 1:
        xA=0.0;
        xB=0.0;
        xC=0.0;
      break;
      case 2:
        xA=0.0;
        xB=rval[ND*j+0]*2.0*PETSC_PI;
        xC=rval[ND*j+1]*2.0*PETSC_PI;
      break;
      case 3:
        xA=PETSC_PI/2.0;
        xB=3.0*PETSC_PI/4.0;
        xC=0.0;
      break;
      case 4:
        xA=PETSC_PI/2.0;
        xB=3.0*PETSC_PI/4.0;
        xC=rval[ND*j+0]*2.0*PETSC_PI;
      break;
      case 5:
        xA=PETSC_PI/2.0;
        xB=asin(sqrt(2.0/3.0));
        xC=PETSC_PI;
      break;
      case 6:
        xA=asin(sqrt(2.0/3.0));
        xB=3*PETSC_PI/4.0;
        xC=rval[ND*j+0]*2.0*PETSC_PI;
      break;
      case 7:
        xA=rval[ND*j+0]*PETSC_PI;
        xB=rval[ND*j+1]*2.0*PETSC_PI;
        xC=rval[ND*j+2]*2.0*PETSC_PI;
      break;
      default:
        SETERRQ1(PETSC_ERR_ARG_INCOMP,
          "Unknown value for texture: %i\n",
          celltexture
        );
      break;
    }

    /* Find the matrix elements from angles */
    PetscReal c1,c2,c3;
    c1=cos(xA);
    c2=cos(xB);
    c3=cos(xC);

    PetscReal s1,s2,s3;
    s1=sin(xA);
    s2=sin(xB);
    s3=sin(xC);

    /* matrix A^-1 */
    PetscReal a11,a12,a13,a21,a22,a23,a31,a32,a33;

    a11= c2*c3-c1*s2*s3;
    a21=-c2*s3-c1*s2*c3;
    a31=s1*s2;
    a12= s2*c3+c1*c2*s3;
    a22=-s2*s3+c1*c2*c3;
    a32=-s1*c2;
    a13=s1*s3;
    a23=s1*c3;
    a33=c1;

    /* below operations are for finding inverse Matrix */

    PetscReal detA;
    detA=a11*a22*a33+a13*a21*a32+a12*a23*a31-a11*a23*a32-a13*a22*a31-a12*a21*a33;

    /* matrix A */
    PetscReal b11,b12,b13,b21,b22,b23,b31,b32,b33;
    b11 = (a22*a33 - a23*a32)/detA;
    b12 = (a23*a31 - a21*a33)/detA;
    b13 = (a21*a32 - a22*a31)/detA;
    b21 = (a13*a32 - a12*a33)/detA;
    b22 = (a11*a33 - a13*a31)/detA;
    b23 = (a12*a31 - a11*a32)/detA;
    b31 = (a12*a23 - a13*a22)/detA;
    b32 = (a13*a21 - a11*a23)/detA;
    b33 = (a11*a22 - a12*a21)/detA;

    /* get components of M */

    PetscReal mx,my,mz;
    mx=ta_M[ND*j+0];
    my=ta_M[ND*j+1];
    mz=ta_M[ND*j+2];

    PetscReal mxp,myp,mzp;
    mxp=b11*mx+b21*my+b31*mz;
    myp=b12*mx+b22*my+b32*mz;
    mzp=b13*mx+b23*my+b33*mz;

    PetscReal lam100,lam111;
    lam100=propdat[NPE*gdata->vertprop[j]+1];
    lam111=propdat[NPE*gdata->vertprop[j]+2];

    PetscReal sigx,sigy,sigz;
    sigx=propdat[NPE*gdata->vertprop[j]+3];
    sigy=propdat[NPE*gdata->vertprop[j]+4];
    sigz=propdat[NPE*gdata->vertprop[j]+5];

    /* direction cosines of Z stress in xtal coor. sys. */
/*
    PetscReal norm;
    norm=sqrt(sigx*sigx+sigy*sigy+sigz*sigz);

    PetscReal gamma1g,gamma2g,gamma3g;
    gamma1g=sigx/norm;
    gamma2g=sigy/norm;
    gamma3g=sigz/norm;

    gamma1=b11*gamma1g+b21*gamma2g+b31*gamma3g;
    gamma2=b12*gamma1g+b22*gamma2g+b32*gamma3g;
    gamma3=b13*gamma1g+b23*gamma2g+b33*gamma3g;
*/

    PetscReal msat;
    msat=gdata->propdat[NP*gdata->vertprop[j]+4];

    PetscReal gamma1,gamma2,gamma3;
    PetscReal elasHkXp,elasHkYp,elasHkZp;

    gamma1=b31;
    gamma2=b32;
    gamma3=b33;
    elasHkXp =  3.0/2.0*lam100*sigz/(msat)*(2.0*mxp*(gamma1*gamma1))+3.0*lam111*sigz/(msat)*(myp*gamma2+mzp*gamma3)*gamma1;
    elasHkYp =  3.0/2.0*lam100*sigz/(msat)*(2.0*myp*(gamma2*gamma2))+3.0*lam111*sigz/(msat)*(mzp*gamma3+mxp*gamma1)*gamma2;
    elasHkZp =  3.0/2.0*lam100*sigz/(msat)*(2.0*mzp*(gamma3*gamma3))+3.0*lam111*sigz/(msat)*(mxp*gamma1+myp*gamma2)*gamma3;

    gamma1=b21;
    gamma2=b22;
    gamma3=b23;
    elasHkXp += 3.0/2.0*lam100*sigy/(msat)*(2.0*mxp*(gamma1*gamma1))+3.0*lam111*sigy/(msat)*(myp*gamma2+mzp*gamma3)*gamma1;
    elasHkYp += 3.0/2.0*lam100*sigy/(msat)*(2.0*myp*(gamma2*gamma2))+3.0*lam111*sigy/(msat)*(mzp*gamma3+mxp*gamma1)*gamma2;
    elasHkZp += 3.0/2.0*lam100*sigy/(msat)*(2.0*mzp*(gamma3*gamma3))+3.0*lam111*sigy/(msat)*(mxp*gamma1+myp*gamma2)*gamma3;

    gamma1=b11;
    gamma2=b12;
    gamma3=b13;
    elasHkXp += 3.0/2.0*lam100*sigx/(msat)*(2.0*mxp*(gamma1*gamma1))+3.0*lam111*sigx/(msat)*(myp*gamma2+mzp*gamma3)*gamma1;
    elasHkYp += 3.0/2.0*lam100*sigx/(msat)*(2.0*myp*(gamma2*gamma2))+3.0*lam111*sigx/(msat)*(mzp*gamma3+mxp*gamma1)*gamma2;
    elasHkZp += 3.0/2.0*lam100*sigx/(msat)*(2.0*mzp*(gamma3*gamma3))+3.0*lam111*sigx/(msat)*(mxp*gamma1+myp*gamma2)*gamma3;

    /* convert into global coordinate system */
    PetscReal elasHkX,elasHkY,elasHkZ;
    elasHkX=a11*elasHkXp+a12*elasHkYp+a13*elasHkZp;
    elasHkY=a21*elasHkXp+a22*elasHkYp+a23*elasHkZp;
    elasHkZ=a31*elasHkXp+a32*elasHkYp+a33*elasHkZp;

    /* assemble cubic H components */
    ta_Helastic[ND*j+0] = elasHkX/gdata->hscale*MU0;
    ta_Helastic[ND*j+1] = elasHkY/gdata->hscale*MU0;
    ta_Helastic[ND*j+2] = elasHkZ/gdata->hscale*MU0;
  }

  ierr = VecRestoreArray(gdata->M,&ta_M);CHKERRQ(ierr);
  ierr = VecRestoreArray(VHelastic,&ta_Helastic);CHKERRQ(ierr);

  ierr = VecAXPY(VHtotsum,1.0,VHelastic);CHKERRQ(ierr);

/* DEBUG: verify results
  PetscPrintf(PETSC_COMM_WORLD,
    "deb01: time : %g and elasticfields are.: %lf,%lf ,%lf \n",
    gdata->time*gdata->tscale*1e9,elasHkX,elasHkY,elasHkZ
  );
  ierr = VecView(VHelastic,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
*/

  MagparFunctionProfReturn(0);
}


int Helastic_Energy(GridData *gdata,Vec VMom,PetscReal *energy)
{
  PetscReal e;

  MagparFunctionInfoBegin;

  if (!fieldon) {
    *energy=0.0;
    MagparFunctionInfoReturn(0);
  }

  ierr = VecDot(VMom,VHelastic,&e);CHKERRQ(ierr);
  e *= -1.0;

  *energy=e;

  MagparFunctionProfReturn(0);
}

