/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: modifyprop_ser.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "init.h"
#include "io/magpario.h"
#include "util/util.h"

int ModifyPropSer(GridData *gdata)
{
  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  if (rank) {
    MagparFunctionLogReturn(0);
  }

  PetscTruth flg;
  int nsliceprop;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-nslicepropser",(PetscInt*)&nsliceprop,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    /* set default values if option is missing */
    nsliceprop=0;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -nslicepropser not found, using default value %i!\n",
      nsliceprop
    );
  }

  /* exit if there is nothing to modify */
  if (nsliceprop<=0) {
    PetscPrintf(PETSC_COMM_WORLD,
      "nslicepropser==%i, nothing modified!\n",
      nsliceprop
    );
    MagparFunctionLogReturn(0);
  }


  int *ia,*ja;
  ierr = Mesh2Dual(gdata->n_ele,gdata->n_vert,gdata->elevert,&ia,&ja);CHKERRQ(ierr);

  int *elebnd;
  ierr = PetscMalloc(gdata->n_ele*sizeof(int),&elebnd);CHKERRQ(ierr);

  /* mark element if a whole face is on the boundary */
  for (int i=0;i<gdata->n_ele;i++) {
    elebnd[i]=C_INT;
    /* get number of neighbours for current element */
    int t_nnb;
    t_nnb=ia[i+1]-ia[i];
    if (t_nnb<NF) {
      elebnd[i]=C_BND;
    }
  }

  /* mark element if at least a node is on the boundary */
/*
  for (int i=0;i<gdata->n_ele;i++) {
    elebnd[i]=C_INT;
    for (int j=0;j<NV;j++) {
      if (gdata->vertbndg2bnd[gdata->elevert[i*NV+j]]>=0) {
        if (elebnd[i]!=C_BND) {
          elebnd[i]=C_BND;
        }
      }
    }
  }
*/

  /* allocate array of new property ids */
  int *newpid;
  ierr = PetscMalloc(gdata->n_ele*sizeof(int),&newpid);CHKERRQ(ierr);

  int cnt1;
  cnt1=0;

  /* elements on surface get new (highest) property id */
  for (int i=0;i<gdata->n_ele;i++) {
    if (elebnd[i]==C_BND) {
/*
      newpid[i]=gdata->eleprop[i]+gdata->n_prop;
      cnt1++;
*/

      /* special case: bottom surface */
      if (
        gdata->vertxyz[ND*gdata->elevert[NV*i+0]+2]+
        gdata->vertxyz[ND*gdata->elevert[NV*i+1]+2]+
        gdata->vertxyz[ND*gdata->elevert[NV*i+2]+2]+
        gdata->vertxyz[ND*gdata->elevert[NV*i+3]+2]<3
      ) {
        newpid[i]=2*gdata->n_prop;
        cnt1++;
      }
      /* special case: top surface */
      else if (
        gdata->vertxyz[ND*gdata->elevert[NV*i+0]+2]+
        gdata->vertxyz[ND*gdata->elevert[NV*i+1]+2]+
        gdata->vertxyz[ND*gdata->elevert[NV*i+2]+2]+
        gdata->vertxyz[ND*gdata->elevert[NV*i+3]+2]>77
      ) {
        newpid[i]=2*gdata->n_prop+1;
        cnt1++;
      }
      /* assign new property id to other surface elements */
      else {
        newpid[i]=gdata->eleprop[i]+gdata->n_prop;
        cnt1++;
      }
    }
    else {
      /* retain old property id */
      newpid[i]=gdata->eleprop[i];
    }
  }

  /* modify property id of selected elements */
  for (int i=0;i<gdata->n_ele;i++) {
    if (newpid[i]>=gdata->n_prop) {
      gdata->eleprop[i]=newpid[i];
      newpid[i]=C_UNK;
    }
  }
  PetscPrintf(PETSC_COMM_WORLD,
    "Modified property id of %i surface elements\n",
    cnt1
  );


  int cnt2;
  cnt2=0;

  /* propagate new property ids into the interior */
  for (int j=1;j<nsliceprop;j++) {
    for (int i=0;i<gdata->n_ele;i++) {
      if (gdata->eleprop[i]>=gdata->n_prop) {
        /* get number of neighbours for current element */
        int t_nnb;
        t_nnb=ia[i+1]-ia[i];

        /* loop all neighbours */
        for (int k=0;k<t_nnb;k++) {
          /* get the id of our neighbour */
          int t_nbid;
          t_nbid=ja[ia[i]+k];

          /* set new property id unless we do already have a new one! */
          if (gdata->eleprop[t_nbid]<gdata->n_prop) {
            newpid[t_nbid]=gdata->eleprop[i];
          }
        }
      }
    }

    /* modify property id of selected elements */
    for (int i=0;i<gdata->n_ele;i++) {
      if (newpid[i]>=gdata->n_prop) {
        gdata->eleprop[i]=newpid[i];
        cnt2++;
        newpid[i]=C_UNK;
      }
    }
  }

  PetscPrintf(PETSC_COMM_WORLD,
    "Modified property id of %i additional elements\n",
    cnt2
  );

  PetscPrintf(PETSC_COMM_WORLD,
    "Modified property id of %i elements in total\n",
    cnt1+cnt2
  );

  assert(cnt1+cnt2<=gdata->n_ele);


  /* update counter of property ids */
  gdata->n_prop = 2*gdata->n_prop+2;

  /* reread property ids */
  ierr = PetscFree(gdata->propdat);CHKERRQ(ierr);
  ierr = ReadKrn(gdata);CHKERRQ(ierr);

  ierr = PetscFree(newpid);CHKERRQ(ierr);
  ierr = PetscFree(elebnd);CHKERRQ(ierr);

  ierr = PetscFree(ia);CHKERRQ(ierr);
  ierr = PetscFree(ja);CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}

