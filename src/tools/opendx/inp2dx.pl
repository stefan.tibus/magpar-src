#!/usr/bin/perl -w

#    This file is part of magpar.
#
#    Copyright (C) 2004-2010 Werner Scholz
#
#    www:   http://www.magpar.net/
#    email: magpar(at)magpar.net
#
#    magpar is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    magpar is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with magpar; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# $Id: inp2dx.pl 2962 2010-02-04 19:50:44Z scholz $

use strict;
use POSIX qw(floor ceil);
use Getopt::Long;

=head1 NAME

B<inp2dx> -- Convert AVS inp files into OpenDX files

=head1 SYNOPSIS

From a the command line invoke as

=over 4

=item

=back

inp2dx model.inp

=back

=cut

=head1 DESCRIPTION

=cut


my $hmap;

my $finp;
my $hinp;

my $n_ele;
my $n_vert;

my $n_dat1;
my $n_dat2;
my $n_dat3;

my @data1;
my @data2;
my @data3;

$finp=$ARGV[0];

if ($finp eq "-") {$hinp="STDIN"}
else {
  unless (open($hinp, $finp)) {
    print STDERR "Can't open $finp: $!\n";
    exit;
  };
};

my $i;
my $j;
my $str;

# *************************************************************************
# Read and write INP header

<$hinp> =~ /(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/;

$n_vert=$1;
$n_ele=$2;
$n_dat1=$3;
$n_dat2=$4;
$n_dat3=$5;

if ($n_dat3!=0) { print STDERR "Model data will be ignored! $n_dat3!=0\n";};

# *************************************************************************
# Read all vertices and write them

print "object \"nodes\" class array type float rank 1 shape 3 items $1 data follows\n";

for ($i=1; $i<=$n_vert; $i++) {
  <$hinp> =~ /(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/;
  if ($1!=$i) { print STDERR "Wrong vertex numbering! $1!=$i\n"; exit };

  print "$2 $3 $4\n";
}
print "\#\n";

# *************************************************************************
# Read and write elements

print "object \"elements\" class array type int rank 1 shape 4 items $n_ele data follows\n";

for ($i=1; $i<=$n_ele; $i++) {
  <$hinp> =~ /(\d+)\s+(\d+)\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)/;

  if ($1!=$i) { print STDERR "Wrong element numbering! $1!=$i\n"; exit };

  printf("%d %d %d %d\n",$4-1,$5-1,$6-1,$7-1);

  $data3[$i]=$2;
}

print "attribute \"element type\" string \"tetrahedra\"\n";
print "attribute \"ref\" string \"positions\"\n";
print "\#\n";

# *************************************************************************
# Read and write all vertex data

# process node data only if there are any
if ($n_dat1!=0) {

  # read number of components for node data
  <$hinp> =~ /(\d+)/;

  if ($1<=0) { print STDERR "Inconsistent number of node data! $1!=$n_dat1\n"; exit };

  # read component labels and store them
  $data1[0][0]="e_void";
  for ($i=1; $i<=$n_dat1; $i++) {
    <$hinp> =~ /(\S+),\s+(\S+)/;
    $data1[0][$i]=$1;
  }

  for ($j=1; $j<=$n_vert; $j++) {
    $_=<$hinp>;
    push @data1 , [ split ];
  }

  # write data
  for ($i=1; $i<=$n_dat1; $i++) {
    print "object \"$data1[0][$i]_data\" class array type float rank 0 items $n_vert data follows\n";
    for ($j=1; $j<=$n_vert; $j++) {
      print "$data1[$j][$i]\n";
    }
    print "attribute \"dep\" string \"positions\"\n";
    print "\#\n";
  }
}
print "\#\n";

# *************************************************************************
# Read and write all element data

# process node data only if there are any
if ($n_dat2!=0) {

  # read number of components for node data
  <$hinp> =~ /(\d+)/;

  if ($1<=0) { print STDERR "Inconsistent number of element data! $1!=$n_dat2\n"; exit };

  # read component labels and store them
  $data2[0][0]="e_void";
  for ($i=1; $i<=$n_dat2; $i++) {
    <$hinp> =~ /(\S+),\s+(\S+)/;
    # insert "e_" to avoid conflict with vertex data with same name
    $data2[0][$i]="e_" . $1;
  }

  for ($j=1; $j<=$n_ele; $j++) {
    $_=<$hinp>;
    push @data2 , [ split ];
  }

  # write data
  for ($i=1; $i<=$n_dat2; $i++) {
    print "object \"$data2[0][$i]_data\" class array type float rank 0 items $n_ele data follows\n";
    for ($j=1; $j<=$n_ele; $j++) {
      print "$data2[$j][$i]\n";
    }
    print "attribute \"dep\" string \"connections\"\n";
    print "\#\n";
  }
}

## add property id obtained from element connectivity
#$n_dat2++;
#$data2[0][$n_dat2]="e_tetpid";
#for ($j=1; $j<=$n_ele; $j++) {
#  $data2[$j][$n_dat2]=$data3[$j];
#}
#
## write data
#$i=$n_dat2;
#print "object \"$data2[0][$i]_data\" class array type float rank 0 items $n_ele data follows\n";
#for ($j=1; $j<=$n_ele; $j++) {
#  print "$data2[$j][$i]\n";
#}
#print "attribute \"dep\" string \"connections\"\n";
#print "\#\n";

print "\#\n";

# *************************************************************************
# Write field objects

for ($i=1; $i<=$n_dat1; $i++) {
  print "object \"$data1[0][$i]\" class field\n";
  print "component \"positions\" value \"nodes\"\n";
  print "component \"connections\" value \"elements\"\n";
  print "component \"data\" value \"$data1[0][$i]_data\"\n";
  print "attribute \"label\" string \"$data1[0][$i]\"\n";
  print "\#\n";
}
for ($i=1; $i<=$n_dat2; $i++) {
  print "object \"$data2[0][$i]\" class field\n";
  print "component \"positions\" value \"nodes\"\n";
  print "component \"connections\" value \"elements\"\n";
  print "component \"data\" value \"$data2[0][$i]_data\"\n";
  print "attribute \"label\" string \"$data2[0][$i]\"\n";
  print "\#\n";
}

#
# the series
print "object \"all_data\" class group\n";
for ($i=0; $i<$n_dat1; $i++) {
  print "member $i value \"$data1[0][$i+1]\"\n";
}
for ($i=$n_dat1; $i<$n_dat1+$n_dat2; $i++) {
  print "member $i value \"$data2[0][$i-$n_dat1+1]\"\n";
}
print "end\n";



__END__

