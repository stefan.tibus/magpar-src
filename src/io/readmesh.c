/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: readmesh.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "magpario.h"

int ReadMesh(GridData *gdata)
{
  MagparFunctionLogBegin;

  PetscTruth flg;
  ierr = PetscOptionsGetString(PETSC_NULL,"-simName",gdata->simname,255,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE)
    SETERRQ(PETSC_ERR_ARG_CORRUPT,"Option -simName not found!\n");

  int meshtype;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-meshtype",(PetscInt*)&meshtype,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    meshtype=1;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -meshtype not found, using default value: %i\n",
      meshtype
    );
  }

  ierr = PetscOptionsGetInt(PETSC_NULL,"-inp",(PetscInt*)&gdata->inp,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    gdata->inp=1;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -inp not found, using default value: %i\n",
      gdata->inp
    );
  }
  ierr = MPI_Bcast(&gdata->inp,1,MPIU_SCALAR,0,PETSC_COMM_WORLD);CHKERRQ(ierr);

  /* indicate, that we have no FE mesh yet */
  gdata->vertxyz=NULL;
  gdata->elevert=NULL;

  if (meshtype==0){
    ierr = ReadPatran(gdata);CHKERRQ(ierr);
  }
  else if (meshtype==1){
    /* TODO: read filename directly from option */
    char fmesh[256];
    ierr = PetscSNPrintf(fmesh,255,"%s.inp",gdata->simname);CHKERRQ(ierr);
    ierr = ReadINP(gdata,fmesh,PETSC_NULL,0);CHKERRQ(ierr);
  }
  else {
    SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"-meshtype has illegal value!\n");
  }

  MagparFunctionLogReturn(0);
}

