/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* $Id: field.h 3001 2010-03-15 15:22:36Z scholz $ */

#ifndef FIELD_H
#define FIELD_H

#include "griddata.h"

int Hcubic_Energy(GridData *gdata,Vec VMom,PetscReal *energy);
int Hcubic(GridData *gdata,Vec VHtotsum);
int Hdemag_Energy(GridData *gdata,Vec VMom,PetscReal *energy);
int Hdemag(GridData *gdata,Vec VHtotsum);
int Helastic_Energy(GridData *gdata,Vec VMom,PetscReal *energy);
int Helastic(GridData *gdata,Vec VHtotsum);
int Hexchani_Energy(GridData *gdata,Vec VMom,PetscReal *energy);
#ifdef EXCH
int Hexchonly_Energy(GridData *gdata,Vec VMom,PetscReal *energy);
#endif
int Hexchani(GridData *gdata,Vec VHtotsum);
int Hexternal_Energy(GridData *gdata,Vec VMom,PetscReal *energy);
int Hexternal(GridData *gdata,Vec VHtotsum);
PetscReal Hexternal_hext();
int Hext_ho(GridData *gdata,Vec VHextsum,PetscReal *hext);
int Hext_ho_hstep(GridData *gdata);
int Hext_ho_hsweep(GridData *gdata);
int Hext_kq(GridData *gdata,Vec VHextsum,PetscReal *hext);
int Hext_cu(GridData *gdata,Vec VHextsum,PetscReal *hext);
int Hext_py(GridData *gdata,Vec VHextsum,PetscReal *hext);
int Htot_EminiEnergy(GridData *gdata, PetscReal *f);
int Htot_Energy(GridData *gdata);
int Htot_Gradient(GridData *gdata);
int Htot(GridData *gdata);

int BMatrix(GridData *gdata,Mat *Bmatrix);
int Bele(PetscReal* bvert,PetscReal* facv1,PetscReal* facv2,PetscReal* facv3,PetscReal* matele);
int MpHext(Vec M,PetscReal *vres);

int read_hstep_file(FILE *fd,PetscReal **indata,const int col);
PetscScalar get_scalf_hstep(PetscReal hstep_fdata[],PetscReal hstep);

#endif
