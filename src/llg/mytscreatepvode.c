/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: mytscreatepvode.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "llg.h"
#include "util/util.h"

#if SUNDIALS_VERSION >= 230
#include "cvode/cvode_diag.h"
#include "cvode/cvode_spgmr.h"
#else
#error "SUNDIALS versions lower/older than 2.3.0 are not supported"
#endif

void     *cvode_mem=PETSC_NULL; /* data structures for PVODE */
N_Vector nvu;        /* vector for independent variable (i.e. magnetization) */
realtype abstol;     /* absolute tolerance */
realtype reltol;     /* relative tolerance */

int PVodeInit(GridData *gdata)
{
  int          flag;
  PetscTruth   flg;

  MagparFunctionLogBegin;

  char str[256];
  ierr = PetscOptionsGetString(PETSC_NULL,"-ts_pvode_type",str,256,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    sprintf(str,"bdf");
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -ts_pvode_type not found, using default value: %s\n",
      str
    );
  }

  int lmmn=-1;
  ierr = PetscStrcmp(str,"adams",&flg);CHKERRQ(ierr);
  if (flg) lmmn=CV_ADAMS;
  ierr = PetscStrcmp(str,"bdf",&flg);CHKERRQ(ierr);
  if (flg) lmmn=CV_BDF;
  if (lmmn != CV_ADAMS && lmmn != CV_BDF) {
    SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"Unknown time integration method.\n");
  }
  cvode_mem = CVodeCreate(lmmn,lmmn==CV_ADAMS ? CV_FUNCTIONAL : CV_NEWTON);
  PetscPrintf(PETSC_COMM_WORLD,"PVODE time integration method: %s\n",str);

  int maxorder;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-maxorder",(PetscInt*)&maxorder,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    maxorder=2;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -maxorder not found, using default value: %i\n",
      maxorder
    );
  }
  flag=CVodeSetMaxOrd(cvode_mem,maxorder);

  PetscReal delt; /* ratio between tolerance of nonlinear and linear iterations */
  ierr = PetscOptionsGetReal(PETSC_NULL,"-ts_pvode_linear_tolerance",&delt,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    delt=0.05;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -ts_pvode_linear_tolerance not found, using default value: %g\n",
      delt
    );
  }
  PetscPrintf(PETSC_COMM_WORLD,"delt: %g\n",delt);

  PetscReal dtime;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-ts_dt",&dtime,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    dtime = gdata->tscale*1e9/100;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -ts_dt not found, using default value: %g\n",
      dtime
    );
  }

  PetscReal dtmin;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-mintimestep",&dtmin,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    dtmin=0.0;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -mintimestep not found, using default value: %g\n",
      dtmin
    );
  }

  PetscReal dtmax;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-maxtimestep",&dtmax,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    dtmax=1;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -maxtimestep not found, using default value: %g\n",
      dtmax
    );
  }

  dtime /= 1e9*gdata->tscale;
  dtmin /= 1e9*gdata->tscale;
  dtmax /= 1e9*gdata->tscale;

  /* dtime must not be zero! otherwise PVODE does not start properly */
  dtime = dtime < D_EPS ? dtmin : dtime;
  dtmin = dtmin < 0 ? dtime : dtmin;
  dtmax = dtmax < 1000 ? dtmax : 1000;

  PetscPrintf(PETSC_COMM_WORLD,"initial time step: %g ns\n",dtime*gdata->tscale*1e9);
  PetscPrintf(PETSC_COMM_WORLD,"minimum time step: %g ns\n",dtmin*gdata->tscale*1e9);
  PetscPrintf(PETSC_COMM_WORLD,"maximum time step: %g ns\n",dtmax*gdata->tscale*1e9);
  PetscPrintf(PETSC_COMM_WORLD,"reduced initial time step: %g (1)\n",dtime);
  PetscPrintf(PETSC_COMM_WORLD,"reduced minimum time step: %g (1)\n",dtmin);
  PetscPrintf(PETSC_COMM_WORLD,"reduced maximum time step: %g (1)\n",dtmax);

  flag=CVodeSetInitStep(cvode_mem,dtime);
  flag=CVodeSetMinStep(cvode_mem,dtmin);
  flag=CVodeSetMaxStep(cvode_mem,dtmax);

  CVodeSetFdata(cvode_mem,gdata);

  flag = CVodeMalloc(
    cvode_mem,
    RHSfunction,
    gdata->time,
    nvu,
    CV_SS,
    reltol,
    &abstol
  );
  if (flag != CV_SUCCESS) {
    SETERRQ1(PETSC_ERR_LIB,"CVodeMalloc failed: %i\n",flag);
  }

  int precon;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-precon",(PetscInt*)&precon,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    precon=1;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -precon not found, using default value: %i\n",
      precon
    );
  }
  PetscPrintf(PETSC_COMM_WORLD,"preconditioning method: %i\n",precon);

  switch (precon) {
    case 0:
      PetscPrintf(PETSC_COMM_WORLD,"preconditioning off\n");
      flag = CVDiag(cvode_mem);
    break;
    case 1:
      /* Jacobi preconditioning - use Jtimes only, if demag is zero */
      PetscPrintf(PETSC_COMM_WORLD,"Jacobi preconditioning on\n");

      int maxl;        /**< maximum Krylov dimension */
      ierr = PetscOptionsGetInt(PETSC_NULL,"-maxl",(PetscInt*)&maxl,&flg);CHKERRQ(ierr);
      if (flg!=PETSC_TRUE) {
        maxl=300;
        PetscPrintf(PETSC_COMM_WORLD,
          "Option -maxl not found, using default value: %i\n",
          maxl
        );
      }
      PetscPrintf(PETSC_COMM_WORLD,"maxl: %i\n",maxl);
      flag = CVSpgmr(cvode_mem,PREC_LEFT,(int)maxl);

      /* initialize Jacobi preconditioning */
      ierr = Precond_Init(gdata);CHKERRQ(ierr);

#if SUNDIALS_VERSION >= 230
      flag = CVSpilsSetPrecType(cvode_mem,PREC_LEFT);
      flag = CVSpilsSetGSType(cvode_mem,MODIFIED_GS);
      flag = CVSpilsSetPreconditioner(cvode_mem,Precond,PSolve,gdata);
#else
#error "SUNDIALS versions lower/older than 2.3.0 are not supported"
#endif
      if (gdata->VHdem==PETSC_NULL) {
#if SUNDIALS_VERSION >= 230
        flag = CVSpilsSetJacTimesVecFn(cvode_mem,Jtimes,gdata);
#else
#error "SUNDIALS versions lower/older than 2.3.0 are not supported"
#endif
        PetscPrintf(PETSC_COMM_WORLD,"Hdemag off: PVODE uses Jtimes function\n");
      }
      else {
        PetscPrintf(PETSC_COMM_WORLD,
          "Hdemag on: PVODE uses internal difference quotient approximation for Jacobian*vector\n"
        );
      }
    break;
    case 2:
      /* BBD preconditioning */
      SETERRQ(PETSC_ERR_SUP,"Not yet implemented!\n");
    break;
    default:
      /* no preconditioning */
      PetscPrintf(PETSC_COMM_WORLD,
        "PVODE uses internal difference quotient approximation for Jacobian*vector\n"
      );
      flag = CVDiag(cvode_mem);
    break;
  }

  if (flag == CV_SUCCESS) {
    PetscPrintf(PETSC_COMM_WORLD,"PVODE successfully initialized\n");
  }
  else {
    SETERRQ(PETSC_ERR_MEM,"CVSpgmr failed.\n");
  }

  MagparFunctionLogReturn(0);
}


int PVodeReInit(GridData *gdata)
{
  MagparFunctionInfoBegin;

#if 1

  /* TODO: remove this section between VecGet/RestoreArray after sufficient testing */

  PetscReal *tmp;
  ierr = VecGetArray(gdata->M,&tmp);CHKERRQ(ierr);

#ifdef UNIPROC
  assert(tmp==NV_DATA_S(nvu));
#else
  assert(tmp==NV_DATA_P(nvu));
#endif

  ierr = VecRestoreArray(gdata->M,&tmp);CHKERRQ(ierr);

  /* renormalize M since we just reinitialized PVODE */
  RenormVec(gdata->M,1.0,D_EPS,ND);

#else

/* TODO: remove this section after sufficient testing */

#ifdef UNIPROC
  VecPlaceArray(gdata->M,NV_DATA_S(nvu));
#else
  VecPlaceArray(gdata->M,NV_DATA_P(nvu));
#endif

  /* renormalize M since we just reinitialized PVODE */
  RenormVec(gdata->M,1.0,D_EPS,ND);

  ierr=VecResetArray(gdata->M);
#endif

  PetscInfo1(0,"t=%g ns: renormalized M\n",gdata->time*gdata->tscale*1e9);

  int flag;

#undef RESETDTIME
#ifdef RESETDTIME
  realtype dtime;
  flag=CVodeGetCurrentStep(cvode_mem,&dtime);
#endif

  flag =
    CVodeReInit(
      cvode_mem,
      RHSfunction,
      gdata->time,
      nvu,
      CV_SS,
      reltol,
      &abstol
    );
  if (flag != CV_SUCCESS) {
    SETERRQ1(PETSC_ERR_LIB,"CVodeReInit failed: %i\n", flag);
  }
  PetscInfo(0,"PVODE successfully reinitialized\n");

#ifdef RESETDTIME
  flag=CVodeSetInitStep(cvode_mem,dtime);
#endif

  MagparFunctionProfReturn(0);
}


int myTSCreatePVode(GridData *gdata)
{
  PetscTruth flg;

  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  ierr = PetscOptionsGetReal(PETSC_NULL,"-ts_pvode_rtol",&reltol,  &flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    reltol=1e-6;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -ts_pvode_rtol not found, using default value: %g\n",
      reltol
    );
  }
  PetscPrintf(PETSC_COMM_WORLD,"reltol:      %g\n",reltol);

  ierr = PetscOptionsGetReal(PETSC_NULL,"-ts_pvode_atol",&abstol,  &flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    abstol=1e-6;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -ts_pvode_atol not found, using default value: %g\n",
      abstol
    );
  }
  PetscPrintf(PETSC_COMM_WORLD,"abstol:      %g\n",abstol);

  int neq, local_N;
  ierr = VecGetSize(gdata->M,(PetscInt*)&neq);CHKERRQ(ierr);
  ierr = VecGetLocalSize(gdata->M,(PetscInt*)&local_N);CHKERRQ(ierr);

  assert(neq==ND*gdata->n_vert);
  assert(local_N==ND*gdata->ln_vert);

#ifdef UNIPROC
  nvu=N_VNew_Serial(neq);
#else
  nvu=N_VNew_Parallel(PETSC_COMM_WORLD,local_N,neq);
#endif

  /* create new vector with PVODE's array */
  /* real data must be of same type as PETSc data (usually double) !!! */
  Vec  Mnew;
  ierr = VecCreateMPIWithArray(
    PETSC_COMM_WORLD,
    local_N,
    neq,
#ifdef UNIPROC
    NV_DATA_S(nvu),
#else
    NV_DATA_P(nvu),
#endif
    &Mnew
  );CHKERRQ(ierr);

  /* copy current magnetization distribution into the new vector */
  VecCopy(gdata->M,Mnew);
  VecDestroy(gdata->M);
  gdata->M=Mnew;
  ierr = VecSetBlockSize(gdata->M,ND);CHKERRQ(ierr);

  cvode_mem=NULL;

  /* initialize PVode */
  ierr = PVodeInit(gdata);CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}

