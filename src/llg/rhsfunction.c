/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: rhsfunction.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "llg.h"

static Vec dMdt=PETSC_NULL;

int RHSfunction(realtype t,N_Vector u,N_Vector udot,void *f_data)
{
  GridData     *gdata = (GridData*)f_data;

  MagparFunctionInfoBegin;
  t_t2=0; /* just to avoid warning about unused variable */

  /* SUNDIALS<230: must not use CHKERRQ here because we have to return void */
  if (dMdt==PETSC_NULL) {
    /* FIXME: use VecCreateMPIWithArray instead!? */
    ierr = VecDuplicate(gdata->M,&dMdt);
  }

#ifdef UNIPROC
  ierr = VecPlaceArray(gdata->M,NV_DATA_S(u));
  ierr = VecPlaceArray(dMdt,NV_DATA_S(udot));
#else
  ierr = VecPlaceArray(gdata->M,NV_DATA_P(u));
  ierr = VecPlaceArray(dMdt,NV_DATA_P(udot));
#endif

  /* set time to get time dependent parts (e.g. Hext) right */
  gdata->time=t;

  calc_dMdt(PETSC_NULL,0.0,gdata->M,dMdt,gdata);

  /* We could use this differential dMdt to set vequil.
     However, there is more noise in dMdt than if we take the finite difference over one full time step
     using the EquilCheck functions.
  #include "util/util.h"
  gdata->vequil=RenormVec(dMdt,0.0,PETSC_MAX,ND);
  */

  ierr = VecResetArray(gdata->M);
  ierr = VecResetArray(dMdt);

#if SUNDIALS_VERSION >= 230
  MagparFunctionProfReturn(0);
#else
  PetscFunctionReturnVoid();
#endif
}

