#!/bin/bash

nnodes=`head -1 $1 | awk '{print $1}'`
nele=`head -1 $2 | awk '{print $1}'`
echo "$nnodes $nele 3 0 0"
grep -v "^#" $1 | tail -n +2
#grep -v "^#" $2 | tail -n +2 | awk '{if ($6=="0") { print "Error: tet with pid 0 found!" > "/dev/stderr" ; exit } print $1 " " $6 " tet " $2 " " $3 " " $4 " " $5 }'
grep -v "^#" $2 | tail -n +2 | awk '{if ($6=="0") { print "Warning: tet with pid 0 found!" > "/dev/stderr"        } print $1 " " $6 " tet " $2 " " $3 " " $4 " " $5 }'
echo "3 1 1 1"
echo "x, none"
echo "y, none"
echo "z, none"
grep -v "^#" $1 | tail -n +2

