/*
    This file is part of magpar.

    Copyright (C) 2006-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: matcreateseqadj.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#define PREALLOC_DG 20
#include "util.h"

int matcreateseqadj(int nvert,int nele,int *elevert,Mat *matout)
{
  MagparFunctionInfoBegin;

  int *ia,*ja;
  ierr = Mesh2Nodal(nele,nvert,elevert,&ia,&ja);CHKERRQ(ierr);

  /* calculate number of nonzeroes for each line */
  for (int i=0;i<nvert;i++) {
    ia[i]=ia[i+1]-ia[i]+1;
  }

  /* create matrix mat */
  Mat mat;
  ierr = MatCreateSeqAIJ(
    PETSC_COMM_SELF,
    nvert,nvert,
    0,ia,
/*
    PREALLOC_DG,PETSC_NULL,
*/
    &mat
  );CHKERRQ(ierr);
  ierr = MatSetFromOptions(mat);CHKERRQ(ierr);

  ierr = PetscFree(ia);CHKERRQ(ierr);
  ierr = PetscFree(ja);CHKERRQ(ierr);

  for (int i=0; i<nele; i++) {
    for (int l=0;l<NV;l++) {
      for (int j=0;j<NV;j++) {
        /* set stiffness matrix element */
        ierr = MatSetValue(
          mat,
          elevert[i*NV+j],
          elevert[i*NV+l],
          1.0,
          ADD_VALUES
        );CHKERRQ(ierr);
      } /* end for j loop */
    }
  }

  PetscInfo(0,"mat matrix assembly complete\n");
  ierr = MatAssemblyBegin(mat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(mat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  *matout=mat;

  MagparFunctionInfoReturn(0);
}

