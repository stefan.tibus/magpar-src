/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: readinp.c 2988 2010-03-01 17:46:55Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "magpario.h"

/* Reads grid and/or data from AVS INP file */

int ReadINP(GridData *gdata, char *fmesh, Vec Vdest, int col)
{
  int          i,j;

  char         msg[256];
  FILE         *fd;

  int          a1;
  int          id, n1, n2, n3, n4, n5;
  int          n_ele;
  int          t_dnv;
  PetscReal    d1,d2,d3;

  PetscTruth   flg;


  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  /* check if we are supposed to read vertex data */
  if (Vdest!=PETSC_NULL && col>0) {
    ierr = VecValid(Vdest,&flg);CHKERRQ(ierr);
    if (flg!=PETSC_TRUE) col=0;
  }
  else {
    col=0;
  }

  if (rank) {
    gdata->ln_vert=0;
    gdata->ln_ele=0;

    if (col>0) {
      VecAssemblyBegin(Vdest);
      VecAssemblyEnd(Vdest);
    }

    MagparFunctionLogReturn(0);
  }

  ierr = PetscFOpen(PETSC_COMM_WORLD,fmesh,"r",&fd);CHKERRQ(ierr);
  if (!rank && !fd) {
    SETERRQ1(PETSC_ERR_FILE_OPEN,"Cannot open %s\n",fmesh);
  }
  PetscPrintf(PETSC_COMM_WORLD,"Opening file %s\n",fmesh);


  /* read
     number of vertices,
     number of elements,
     number of vertex data,
     number of element data,
     number of model data
  */
  fscanf(fd,"%i %i %i %i %i\n",
    &n1,&n2,&n3,&n4,&n5
  );
  n_ele=n2;

  PetscPrintf(PETSC_COMM_WORLD,"Number of grid vertices: %i\n",n1);
  PetscPrintf(PETSC_COMM_WORLD,"Number of grid elements: %i\n",n_ele);

  t_dnv=n3;

  PetscPrintf(PETSC_COMM_WORLD,"Number of vertex data:   %i\n",n3);
  PetscPrintf(PETSC_COMM_WORLD,"Number of element data:  %i\n",n4);
  PetscPrintf(PETSC_COMM_WORLD,"Number of element data2: %i\n",n5);

  /* *************************************************************************
     Read in all vertices
  */

  if (gdata->vertxyz==PETSC_NULL) {
    flg=PETSC_FALSE;
    PetscPrintf(PETSC_COMM_WORLD,
      "Allocating array for vertices and reading their coordinates\n");

    gdata->n_vert=n1;
    gdata->ln_vert=gdata->n_vert;

    ierr = PetscMalloc(ND*gdata->n_vert*sizeof(PetscReal),&gdata->vertxyz);CHKERRQ(ierr);
  }
  else {
    flg=PETSC_TRUE;
    if (gdata->n_vert!=n1) {
      SETERRQ3(PETSC_ERR_FILE_UNEXPECTED,
        "Data in %s corrupt! Number of vertices does not agree with previously read mesh (%i != %i).\n",
        fmesh,gdata->n_vert,n1
      );
    }
    if (gdata->ln_vert!=gdata->n_vert) {
      SETERRQ3(PETSC_ERR_FILE_UNEXPECTED,
        "Data in %s corrupt! Number of elements does not agree with previously read mesh (%i != %i).\n",
        fmesh,gdata->ln_vert,gdata->n_vert
      );
    }
  }

  /* read data of all vertices */
  for (i=0; i<gdata->n_vert; i++) {
#ifdef PETSC_USE_SINGLE
    fscanf(fd,"%i %f %f %f\n",
#else
    fscanf(fd,"%i %lf %lf %lf\n",
#endif
      &id,&d1,&d2,&d3
    );

    id--;

    if (flg==PETSC_FALSE) {
      gdata->vertxyz[ND*id+0]=d1;
      gdata->vertxyz[ND*id+1]=d2;
      gdata->vertxyz[ND*id+2]=d3;
    }
    else {
      if (PetscAbsReal(d1)>D_EPS) {
        if (PetscAbsReal((gdata->vertxyz[ND*id+0]-d1)/d1) > 1e-4)
        {
          PetscPrintf(PETSC_COMM_WORLD,"Data in %s corrupt!\n",fmesh);
          SETERRQ3(PETSC_ERR_FILE_UNEXPECTED,
            "x-coordinates of vertex %i does not agree with previously read mesh (%g != %g).\n",
            i,
            gdata->vertxyz[ND*id+0],
            d1
          );
        }
      }
      if (PetscAbsReal(d2)>D_EPS) {
        if (PetscAbsReal((gdata->vertxyz[ND*id+1]-d2)/d2) > 1e-4)
        {
          PetscPrintf(PETSC_COMM_WORLD,"Data in %s corrupt!\n",fmesh);
          SETERRQ3(PETSC_ERR_FILE_UNEXPECTED,
            "y-coordinates of vertex %i does not agree with previously read mesh (%g != %g).\n",
            i,
            gdata->vertxyz[ND*id+1],
            d2
          );
        }
      }
      if (PetscAbsReal(d3)>D_EPS) {
        if (PetscAbsReal((gdata->vertxyz[ND*id+2]-d3)/d3) > 1e-4)
        {
          PetscPrintf(PETSC_COMM_WORLD,"Data in %s corrupt!\n",fmesh);
          SETERRQ3(PETSC_ERR_FILE_UNEXPECTED,
            "z-coordinates of vertex %i does not agree with previously read mesh (%g != %g).\n",
            i,
            gdata->vertxyz[ND*id+2],
            d2
          );
        }
      }
    }
  }

  /* *************************************************************************
     Read in all elements
  */

  if (gdata->elevert==PETSC_NULL) {
    flg=PETSC_FALSE;
    PetscPrintf(PETSC_COMM_WORLD,"Allocating array for element connectivity and reading it\n");

    gdata->n_ele=n_ele;
    gdata->ln_ele=gdata->n_ele;

    gdata->n_prop=0;

    ierr = PetscMalloc(NV*gdata->n_ele*sizeof(int),&gdata->elevert);CHKERRQ(ierr);
    ierr = PetscMalloc(gdata->n_ele*sizeof(int),&gdata->eleprop);CHKERRQ(ierr);
  }
  else {
    flg=PETSC_TRUE;
    assert(gdata->n_ele==n_ele || n_ele==0);
    assert(gdata->ln_ele==gdata->n_ele);
  }


  for (i=0; i<n_ele; i++) {
    fscanf(fd,"%i %i tet %i %i %i %i\n",
      &id,&a1,&n1,&n2,&n3,&n4
    );

    /* our numbering starts with 0, not 1 */
    id--;

    if (a1<1) {
      SETERRQ3(PETSC_ERR_FILE_UNEXPECTED,
        "Data in %s corrupt! Property of element %i is %i, but has to be >0! Exiting!\n",
        fmesh,i,a1
      );
    }

    if (flg==PETSC_FALSE) {
      gdata->n_prop=PetscMax(gdata->n_prop,a1);
      /* internal property ids start with 0 !!! */
      gdata->eleprop[id]=--a1;

      gdata->elevert[id*NV+0]=--n1;
      gdata->elevert[id*NV+1]=--n2;
      gdata->elevert[id*NV+2]=--n3;
      gdata->elevert[id*NV+3]=--n4;
    }
    else {
      /* internal property ids start with 0 !!! */
      assert(gdata->eleprop[id]==--a1);

      assert(gdata->elevert[id*NV+0]==--n1);
      assert(gdata->elevert[id*NV+1]==--n2);
      assert(gdata->elevert[id*NV+2]==--n3);
      assert(gdata->elevert[id*NV+3]==--n4);
    }
  }

  PetscPrintf(PETSC_COMM_WORLD,"Number of element prop.: %i\n",gdata->n_prop);

  if (col<=0) {
    PetscFClose(PETSC_COMM_WORLD,fd);
    MagparFunctionLogReturn(0);
  }

  /* *************************************************************************
     Read vertex data
  */

  /* read number of components of vertex data */
  fscanf(fd,"%i\n",&a1);
  assert(a1==t_dnv);

  if (a1<col+2) {
    SETERRQ3(PETSC_ERR_FILE_UNEXPECTED,
      "Data in %s corrupt! Cannot read data from column %i. Only %i data columns found! Exiting!\n",
      fmesh,col,a1
    );
  }

  /* read remainder of this line */
  fgets(msg,256,fd);

  /* we assume that all components have size 1 ! */
  for (i=0; i<t_dnv; i++) {
    fgets(msg,256,fd);
    if ((col-1)<=i && i<=(col+2-1)) {
      PetscPrintf(PETSC_COMM_WORLD,"reading: %s",msg);
    }
  }

  for (i=0; i<gdata->n_vert; i++) {
    /* read id */
    fscanf(fd,"%i",&id);

    /* skip useless columns */
    for (j=0; j<col-1; j++) {
#ifdef PETSC_USE_SINGLE
      if (fscanf(fd,"%f",&d1)!=1) {
#else
      if (fscanf(fd,"%lf",&d1)!=1) {
#endif
        SETERRQ3(PETSC_ERR_FILE_UNEXPECTED,
          "Data in %s corrupt! Cannot read data from column %i. Only %i data columns found! Exiting!\n",
          fmesh,col,j
        );
      }
    }

    /* read data */
#ifdef PETSC_USE_SINGLE
    fscanf(fd,"%f %f %f",&d1,&d2,&d3);
#else
    fscanf(fd,"%lf %lf %lf",&d1,&d2,&d3);
#endif
    /* read remainder of this line */
    fgets(msg,256,fd);

    /* our numbering starts with 0, not 1 */
    id--;

    ierr = VecSetValue(Vdest,ND*id+0,d1,INSERT_VALUES);CHKERRQ(ierr);
    ierr = VecSetValue(Vdest,ND*id+1,d2,INSERT_VALUES);CHKERRQ(ierr);
    ierr = VecSetValue(Vdest,ND*id+2,d3,INSERT_VALUES);CHKERRQ(ierr);
  }

  ierr = VecAssemblyBegin(Vdest);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(Vdest);CHKERRQ(ierr);

  ierr = PetscFClose(PETSC_COMM_WORLD,fd);CHKERRQ(ierr);


  MagparFunctionLogReturn(0);
}

