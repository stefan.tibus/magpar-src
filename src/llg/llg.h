/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* $Id: llg.h 2962 2010-02-04 19:50:44Z scholz $ */

#ifndef LLG_H
#define LLG_H

#include "griddata.h"

#ifdef SUNDIALS_VERSION

#if SUNDIALS_VERSION < 230
#error "SUNDIALS versions lower/older than 2.3.0 are not supported"
#endif

#ifdef UNIPROC
#include "nvector/nvector_serial.h"       /* definitions of type N_Vector, macro N_VDATA for serial version */
#else
#include "nvector/nvector_parallel.h"     /* definitions of type N_Vector, macro N_VDATA for parallel version */
#endif

#include "sundials/sundials_types.h"      /* definitions of real, integer, boole, TRUE,FALSE */
#include "cvode/cvode.h"                  /* main CVODE header file */
#include "sundials/sundials_spgmr.h"      /* use CVSPGMR linear solver each internal step */
#include "sundials/sundials_dense.h"      /* use generic DENSE solver in preconditioning */
#include "sundials/sundials_math.h"       /* contains SQR macro */

int RHSfunction(realtype t,N_Vector u,N_Vector udot,void *f_data);
int PSolve(realtype t,N_Vector y,N_Vector fy,N_Vector r,N_Vector z,realtype gamma,realtype delta,int lr,void *P_data,N_Vector vtemp);
int Precond(realtype t,N_Vector y,N_Vector fy,booleantype jok,booleantype *jcurPtr,realtype gamma,void *P_data,N_Vector vtemp1,N_Vector vtemp2,N_Vector vtemp3);
int Jtimes(N_Vector v,N_Vector Jv,realtype t,N_Vector y,N_Vector fy,void *jac_data,N_Vector tmp);

int WriteLogPVodeInit(GridData *gdata);
int WriteLogPVode(GridData *gdata);

int CheckIterationLLG(GridData *gdata);
int EquilCheck(GridData *gdata);
int myTSCreatePVode(GridData *gdata);
int myTSStepPVode(GridData *gdata);
int Precond_Init(GridData *gdata);
int PVodeInit(GridData *gdata);
int PVodeReInit(GridData *gdata);

#endif

int myLLGJacobian(TS ts,PetscReal t,Vec global_in,Mat *AA,Mat *BB,MatStructure *str,void *ctx);
int calc_dMdt(TS ts,PetscReal dt,Vec invec,Vec outvec,void *ptr);

#endif
