/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: mytssteppvode.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "llg.h"

int myTSStepPVode(GridData *gdata)
{
  extern void *cvode_mem;
  extern      N_Vector nvu;

  static int  nsteps=-1;

  MagparFunctionInfoBegin;

  /* take 20 time steps at once
     problem: tols not increased, because devNorm gets too large
     TODO: check again if it buys any speedup
  */
  if (nsteps<0) {
    PetscTruth flg;
    ierr = PetscOptionsGetInt(PETSC_NULL,"-ts_nsteps",(PetscInt*)&nsteps,&flg);CHKERRQ(ierr);
    if (flg!=PETSC_TRUE) {
      nsteps=1;
      PetscPrintf(PETSC_COMM_WORLD,
        "Option -ts_nsteps not found, using default value: %i\n",
        nsteps
      );
    }
    if (nsteps<1) {
      SETERRQ1(PETSC_ERR_ARG_OUTOFRANGE,"Option set: -ts_nsteps %i, but has to be >0\n",nsteps);
    }
  }

  ierr = CV_SUCCESS; /* just to avoid compiler warning about uninitialized var */
  for (int i=0;i<nsteps;i++) {
    ierr = CVode(cvode_mem,PETSC_MAX,nvu,&gdata->time,CV_ONE_STEP);

    if (ierr != CV_SUCCESS) {
      SETERRQ1(PETSC_ERR_LIB,"CVode failed, ierr=%i.\n",ierr);
    }
  }

  /* integrate for 5 ps
     same problem as above
  ierr = CVode(
    cvode_mem,
    gdata->time+5.0e-12/gdata->tscale,
    nvu,
    &gdata->time,
    NORMAL
  );
  */

#define DEBUG

#ifdef DEBUG

  /* FIXME: why is VecPlaceArray not necessary with PETSc version 2.3.0? */
  /* check that gdata->M points to the result */
  PetscReal *tmp;
  ierr = VecGetArray(gdata->M,&tmp);CHKERRQ(ierr);

#ifdef UNIPROC
  assert(tmp==NV_DATA_S(nvu));
#else
  assert(tmp==NV_DATA_P(nvu));
#endif

  ierr = VecRestoreArray(gdata->M,&tmp);CHKERRQ(ierr);

#endif /* DEBUG */

  MagparFunctionProfReturn(0);
}
