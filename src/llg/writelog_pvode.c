/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: writelog_pvode.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "llg.h"

#include "cvode/cvode.h"

#if SUNDIALS_VERSION >= 230
#include "cvode/cvode_spgmr.h"
#else
#error "SUNDIALS versions lower/older than 2.3.0 are not supported"
#endif

static int  doinit=1;
static int  fieldon=0;
static FILE *logfile=NULL; /**< pointer to the logfile */

int WriteLogPVodeInit(GridData *gdata)
{
  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  if (rank) {
    MagparFunctionLogReturn(0);
  }

  if (gdata->mode!=0 && gdata->mode!=2 && gdata->mode!=3) {
    MagparFunctionLogReturn(0);
  }
  fieldon=1;

  char fmesh[256];
  ierr = PetscSNPrintf(fmesh,255,"%s%s",gdata->simname,".log_pvode");CHKERRQ(ierr);
  ierr = PetscFOpen(PETSC_COMM_WORLD,fmesh,"a",&logfile);CHKERRQ(ierr);
  if (!logfile) {
    SETERRQ1(PETSC_ERR_FILE_OPEN,"Cannot open %s\n",fmesh);
  }
  PetscPrintf(PETSC_COMM_WORLD,"Opening file %s\n",fmesh);

  PetscGetDate(fmesh,255);
  ierr = PetscFPrintf(PETSC_COMM_WORLD,
    logfile,
    "#.date: %s\n",
    fmesh
  );

  /*      1   2   3   4   5   6   7   8   9  10 */
  ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile,
    "#.%14s %7s %4s %4s %4s %4s %4s %4s %4s %4s\n",
    "............1:",
    " 2:",
    " 3:",
    " 4:",
    " 5:",
    " 6:",
    " 7:",
    " 8:",
    " 9:",
    "10:"
  );CHKERRQ(ierr);
  /*      1   2   3   4   5   6   7   8   9  10 */
  ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile,
    "#.%14s %7s %4s %4s %4s %4s %4s %4s %4s %4s\n",
    "..........time",
    "nstep",
    "qu",
    "nfe",
    "nni",
    "ncfn",
    "nli",
    "npe",
    "nps",
    "ncfl"
  );CHKERRQ(ierr);
  /*      1   2   3   4   5   6   7   8   9  10 */
  ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile,
    "#.%14s %7s %4s %4s %4s %4s %4s %4s %4s %4s\n",
    /* "time",      */ "..........(ns)",
    /* "nstep",     */ "-",
    /* "qu",        */ "-",
    /* "nfe",       */ "-",
    /* "nni",       */ "-",
    /* "ncfn",      */ "-",
    /* "nli",       */ "-",
    /* "npe",       */ "-",
    /* "nps",       */ "-",
    /* "ncfl",      */ "-"
  );CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}


int WriteLogPVode(GridData *gdata)
{
  extern void *cvode_mem;

  MagparFunctionInfoBegin;

  if (doinit) {
    ierr = WriteLogPVodeInit(gdata);CHKERRQ(ierr);
    doinit=0;
  }
  if (!fieldon || cvode_mem==PETSC_NULL) {
    MagparFunctionInfoReturn(0);
  }

  int flag;
  static long int last_nfe,last_nni,last_ncfn,last_nli,last_npe,last_nps,last_ncfl,last_nstep;
  long int nstep,nfe,nni,ncfn,nli,npe,nps,ncfl;
  int qu;

  flag=CVodeGetNumSteps(cvode_mem,&nstep);

  /* if PVODE hase been (re)started */
  if (nstep==0 || nstep<last_nstep) {
    /* reset counters */
    last_nfe=last_nni=last_ncfn=last_nli=last_npe=last_nps=last_ncfl=0;
  }
  if (nstep==0) {
    nfe=nni=ncfn=nli=npe=nps=ncfl=0;
    qu=0;
  }
  else {
    flag=CVodeGetLastOrder(cvode_mem,&qu);
    flag=CVodeGetNumRhsEvals(cvode_mem,&nfe);
    flag=CVodeGetNumNonlinSolvIters(cvode_mem,&nni);
    flag=CVodeGetNumNonlinSolvConvFails(cvode_mem,&ncfn);
#if SUNDIALS_VERSION >= 230
    flag=CVSpilsGetNumLinIters(  cvode_mem,&nli);
    flag=CVSpilsGetNumPrecEvals( cvode_mem,&npe);
    flag=CVSpilsGetNumPrecSolves(cvode_mem,&nps);
    flag=CVSpilsGetNumConvFails( cvode_mem,&ncfl);
#else
#error "SUNDIALS versions lower/older than 2.3.0 are not supported"
#endif
  }

  /*     1   2    3    4    5    6    7    8    9   10 */
  ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile,
    "  %14e %7d %4d %4ld %4ld %4ld %4ld %4ld %4ld %4ld\n",
    /* " 1:time",      */ gdata->time*gdata->tscale*1e9,
    /* " 2:nstep",     */ nstep,
    /* " 3:qu",        */ qu,
    /* " 4:nfe",       */ nfe- last_nfe,
    /* " 5:nni",       */ nni- last_nni,
    /* " 6:ncfn",      */ ncfn-last_ncfn,
    /* " 7:nli",       */ nli- last_nli,
    /* " 8:npe",       */ npe- last_npe,
    /* " 9:nps",       */ nps- last_nps,
    /* "10:ncfl",      */ ncfl-last_ncfl
  );

  last_nfe=  nfe;
  last_nni=  nni;
  last_ncfn= ncfn;
  last_nli=  nli;
  last_npe=  npe;
  last_nps=  nps;
  last_ncfl= ncfl;
  last_nstep= nstep;

  MagparFunctionInfoReturn(0);
}

