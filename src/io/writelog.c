/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: writelog.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "magpario.h"
#include "util/util.h"
#include "field/field.h"

#ifdef ADDONS
#include "addons/addons.h"
#endif

#ifdef SPINTORQ
#include "spintorq/spintorq.h"
#endif

#ifdef SUNDIALS_VERSION
#include "llg/llg.h"
#endif

static int     doinit=1;
static FILE    *logfile; /**< pointer to the logfile */

static Vec     V1Mtot[ND]; /**< "box method": vertex volumes (to calcuate Mtot) */

int Vec3VolAvg(Vec v,PetscReal* res)
{
  MagparFunctionInfoBegin;

  /* calculate Mtot (ignoring Js) */
  for (int i=0;i<ND;i++) {
    ierr = VecDot(v,V1Mtot[i],&res[i]);CHKERRQ(ierr);
  }

  MagparFunctionInfoReturn(0);
}


int WriteLogInit(GridData *gdata)
{
  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  PetscReal *vvol;
  ierr = VecGetArray(gdata->vertvol,&vvol);CHKERRQ(ierr);

  /* calculate magnetic volume */
  PetscReal Vtot;
  Vtot=0.0;
  for (int j=0;j<gdata->ln_vert;j++) {
    if (gdata->propdat[NP*gdata->vertprop[j]+4]>0.0) {
      Vtot += vvol[j];
    }
  }
  PetscReal temp;
  ierr = PetscGlobalSum(&Vtot,&temp,PETSC_COMM_WORLD);CHKERRQ(ierr);
  Vtot=temp;

  PetscInfo2(0,"Using total volume %g (%g)\n",Vtot,gdata->totvol);
  /* Vtot, gdata->totvol might be different because the latter is sum over gdata->elevol */

  /* calculate average M only over magnetic regions */
  for (int i=0;i<ND;i++) {
    ierr = VecDuplicate(gdata->M,&V1Mtot[i]);CHKERRQ(ierr);

    PetscReal *v1m;
    ierr = VecGetArray(V1Mtot[i],&v1m);CHKERRQ(ierr);
    for (int j=0;j<gdata->ln_vert;j++) {
      if (gdata->propdat[NP*gdata->vertprop[j]+4]>0.0) {
        v1m[ND*j+i]=vvol[j];
      }
    }
    ierr = VecRestoreArray(V1Mtot[i],&v1m);CHKERRQ(ierr);

    ierr = VecScale(V1Mtot[i],1.0/Vtot);CHKERRQ(ierr);
  }

  ierr = VecRestoreArray(gdata->vertvol,&vvol);CHKERRQ(ierr);

  if (rank) {
    MagparFunctionLogReturn(0);
  }

  char fmesh[256];
  ierr = PetscSNPrintf(fmesh,255,"%s%s",gdata->simname,".log");CHKERRQ(ierr);
  ierr = PetscFOpen(PETSC_COMM_WORLD,fmesh,"a",&logfile);CHKERRQ(ierr);
  if (!logfile) {
    SETERRQ1(PETSC_ERR_FILE_OPEN,"Cannot open %s\n",fmesh);
  }
  PetscPrintf(PETSC_COMM_WORLD,"Opening file %s\n",fmesh);

  PetscGetDate(fmesh,255);
  ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile,"#.date: %s\n",fmesh);

  /*     1   2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   20 */
  ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile,
    "#.%3s %4s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s\n",
    ".1:",
    " 2:",
    " 3:",
    " 4:",
    " 5:",
    " 6:",
    " 7:",
    " 8:",
    " 9:",
    "10:",
    "11:",
    "12:",
    "13:",
    "14:",
    "15:",
    "16:",
    "17:",
    "18:",
    "20:"
  );CHKERRQ(ierr);
  /*     1   2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   20*/
  ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile,
    "#.%3s %4s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s\n",
    ".eq",
    "inp",
    "time",
    "Hext",
    "Etot",
    "J//Hext",
    "Mx",
    "My",
    "Mz",
    "|M|",
    "Edem",
#ifdef EXCH
    "Eanionly",
#else
    "Eexchani",
#endif
    "Eext",
#ifdef EXCH
    "Eexchonly",
#else
    "-",
#endif
    "devNorm",
    "mx|dM/dt|",
    "timestep",
    "tCPU",
    "CPUtstep"
  );CHKERRQ(ierr);
  /*     1   2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   20*/
  ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile,
    "#.%3s %4s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s %14s\n",
    /* "eq",           */ "..-",
    /* "inp",          */ "-",
    /* " 3:time",      */ "(ns)",
    /* " 4:Hext",      */ "(kA/m)",
    /* " 5:Etot",      */ "(J/m^3)",
    /* " 6:J//Hext",   */ "[J/Javg]",
    /* " 7:Mx",        */ "(1)",
    /* " 8:My",        */ "(1)",
    /* " 9:Mz",        */ "(1)",
    /* "10:|M|",       */ "(1)",
    /* "11:Edem",      */ "(J/m^3)",
    /* "12:Eexchani",  */ "(J/m^3)",
    /* "13:Eext",      */ "(J/m^3)",
#ifdef EXCH
    /* "14:Eexchonly", */ "(J/m^3)",
#else
    /* "14:void",      */ "-",
#endif
    /* "15:devNorm",   */ "(1)",
    /* "16:mx|dM/dt|", */ "(1)",
    /* "17:timestep",  */ "(ns)",
    /* "18:tCPU",      */ "(s)",
    /* "20:CPUtstep",  */ "(s)"
  );CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}


int WriteLogData(GridData *gdata)
{
  static PetscLogDouble tCPUstart=-1; /**< (absolute) time at program start */
  static PetscLogDouble tCPUlast;
  static PetscReal      timelast;

  MagparFunctionInfoBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  if (doinit) {
    tCPUstart=t_t1;
    tCPUlast=t_t1;
    timelast=gdata->time;
    ierr = WriteLogInit(gdata);CHKERRQ(ierr);
    doinit=0;
  }

  PetscReal devNorm;
  devNorm=RenormVec(gdata->M,1.0,PETSC_MAX,ND);

  /* calculate Mtot (ignoring Js) */
  PetscReal Mtot[ND];
  ierr = Vec3VolAvg(gdata->M,Mtot);CHKERRQ(ierr);

  /* calculate average magnetization parallel to the external field */
  PetscReal valMpHext;
  ierr = MpHext(gdata->M,&valMpHext);CHKERRQ(ierr);

  PetscReal hext;
  hext = Hexternal_hext();

/* Edem<0 occurs often in airboxes (should be zero)
   and due to magnetostatic field of other parts (not self-demag),
   which acts like an external field.
   As a result, lots of output when Hdemag_Energy function is called for each
   volume individually (with -logpid 1).
   Thus, better done in WriteLogData, where we check Edem for the whole model.
*/

  if (gdata->Edem<0) {
    PetscPrintf(MPI_COMM_WORLD,
      "Warning: Edem<0 at t=%g ns: Edem=%g J/m^3\n",
      gdata->time*gdata->tscale*1e9,
      gdata->Edem*gdata->escale
    );
  }

/*       1   2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   20*/
  ierr = PetscFPrintf(PETSC_COMM_WORLD,logfile,
    "  %3d %4d %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e %14e\n",
    /* "eq",           "",        */ gdata->equil,
    /* "inp",          "",        */ gdata->inp >= 0 ? gdata->inp : 0,
    /* " 3:time",      "(ns)",    */ gdata->time*gdata->tscale*1e9,
    /* " 4:Hext",      "(kA/m)",  */ hext*gdata->hscale/(MU0*1000.0),
    /* " 5:Etot",      "(J/m^3)", */ gdata->Etot*gdata->escale,
    /* " 6:J//Hext",   "[J/Javg]",*/ valMpHext,
    /* " 7:Mx",        "(1)",     */ Mtot[0],
    /* " 8:My",        "(1)",     */ Mtot[1],
    /* " 9:Mz",        "(1)",     */ Mtot[2],
    /* "10:|M|",       "(1)",     */ sqrt(Mtot[0]*Mtot[0]+Mtot[1]*Mtot[1]+Mtot[2]*Mtot[2]),
    /* "11:Edem",      "(J/m^3)", */ gdata->Edem*gdata->escale,
#ifdef EXCH
    /* "12:Eanionly",  "(J/m^3)", */ (gdata->Eexchani-gdata->Eexchonly)*gdata->escale,
#else
    /* "12:Eexchani",  "(J/m^3)", */ gdata->Eexchani*gdata->escale,
#endif
    /* "13:Eext",      "(J/m^3)", */ gdata->Eext*gdata->escale,
#ifdef EXCH
    /* "14:Eexchonly", "(J/m^3)", */ gdata->Eexchonly*gdata->escale,
#else
    /* "14:void",      "",        */ -1.0,
#endif
    /* "15:devNorm",   "(1)",     */ devNorm,
    /* "16:vequil",    "(1)",     */ gdata->vequil,
    /* "17:timestep",  "(ns)",    */ (gdata->time-timelast)*gdata->tscale*1e9,
    /* "18:tCPU",      "(s)",     */ t_t1-tCPUstart,
    /* "20:CPUtstep",  "(s)",     */ t_t1-tCPUlast /* CPU time for last time step */
  );

  tCPUlast=t_t1;
  timelast=gdata->time;

  MagparFunctionInfoReturn(0);
}


int WriteLog(GridData *gdata)
{
  MagparFunctionInfoBegin;

  ierr = WriteLogData(gdata);CHKERRQ(ierr);
  ierr = WriteLogPid(gdata);CHKERRQ(ierr);

#ifdef SUNDIALS_VERSION
  /* write sundials logfile */
  ierr = WriteLogPVode(gdata);CHKERRQ(ierr);
#endif

#ifdef ADDONS
  /* write addons logfile */
  ierr = WriteLogExt(gdata);CHKERRQ(ierr);
#endif

#ifdef SPINTORQ
  /* write addons logfile */
  ierr = WriteLogGMR(gdata);CHKERRQ(ierr);
#endif

  MagparFunctionInfoReturn(0);
}

