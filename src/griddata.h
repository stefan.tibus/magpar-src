/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* $Id: griddata.h 3014 2010-03-26 17:27:32Z scholz $ */

#ifdef PYTHON
#include "Python.h"
#endif

#ifndef GRIDDATA_H
#define GRIDDATA_H

/* define variable __FUNCT__ (used by MagparFunction below)
   need to do before including petsc headers
   otherwise cpp complains about it being redefined here
*/
#ifdef __GNUC__
#define __FUNCT__ __FUNCTION__
#elif __STDC_VERSION__ == 199901L
#define __FUNCT__ __FUNC__
#else
#define __FUNCT__ "function"
#endif

#include "petscsys.h"
#include "petscts.h"
#include "petscao.h"

/** energy minimization */
#ifdef TAO
#include "tao.h"
#endif

/* check PETSc and TAO versions:
magpar requires
(PETSc version 3.0.0 and TAO ???) or
(PETSc version 2.3.3 and TAO 1.9)
*/

#if (PETSC_VERSION_MAJOR == 3 && PETSC_VERSION_MINOR == 1 && PETSC_VERSION_SUBMINOR == 0)
#define PETSC_VERSION 310
#elif (PETSC_VERSION_MAJOR == 3 && PETSC_VERSION_MINOR == 0 && PETSC_VERSION_SUBMINOR == 0)
#define PETSC_VERSION 300
#elif (PETSC_VERSION_MAJOR == 2 && PETSC_VERSION_MINOR == 3 && PETSC_VERSION_SUBMINOR == 3)
#define PETSC_VERSION 233
#ifdef TAO
#if (TAO_VERSION_MAJOR != 1 && TAO_VERSION_MINOR != 8 && TAO_VERSION_SUBMINOR != 3)
#error PETSc version 2.3.3 requires TAO version 1.9
#endif
#endif
#else
#error "PETSc version unsupported or unknown! Please use one of the supported versions!"
#endif

#define MagparFunctionLogBegin        int ierr;PetscLogDouble t_t1,t_t2;{PetscFunctionBegin;PetscPrintf(PETSC_COMM_WORLD,"\n>> %s::%s\n",__FILE__,__FUNCT__);PetscInfo2(0,"%s%s",Id,Td);PetscGetTime(&t_t1);ierr=0;}
#define MagparFunctionLogReturn(A)   {PetscGetTime(&t_t2);PetscPrintf(PETSC_COMM_WORLD,"<< %s::%s took %g s\n\n",__FILE__,__FUNCT__,t_t2-t_t1);CHKMEMQ;PetscFunctionReturn(A);}
#if PETSC_VERSION >= 232
#define MagparFunctionInfoBegin       int ierr;PetscLogDouble t_t1,t_t2;{PetscFunctionBegin;PetscInfo1(0,"%s\n",__FILE__);PetscInfo2(0,"%s%s",Id,Td);PetscGetTime(&t_t1);ierr=0;}
#define MagparFunctionInfoReturn(A)  {PetscGetTime(&t_t2);PetscInfo1(0,"took %g s\n\n",t_t2-t_t1);PetscFunctionReturn(A);}
#define MagparFunctionProfReturn(A)  {PetscGetTime(&t_t2);PetscTruth profile;PetscOptionsHasName(PETSC_NULL,"-profile",&profile);if (profile) {PetscPrintf(PETSC_COMM_WORLD,"%s::%s took %g s\n",__FILE__,__FUNCT__,t_t2-t_t1);} else {PetscInfo1(0,"took %g s\n",t_t2-t_t1);} PetscFunctionReturn(A);}
#else
#define MagparFunctionInfoBegin       int ierr;PetscLogDouble t_t1,t_t2;{PetscFunctionBegin;PetscGetTime(&t_t1);ierr=0;}
#define MagparFunctionInfoReturn(A)  {PetscGetTime(&t_t2);PetscFunctionReturn(A);}
#define MagparFunctionProfReturn(A)  {PetscGetTime(&t_t2);PetscFunctionReturn(A);}
#endif

/** for assert statements */
#include <assert.h>

/** \name Defines of numbers
*/
/*@{*/
#define ND 3       /**< space dimensions (no of cartesian coordinates) */
#define NV 4       /**< number of vertices(=degrees of freedom) per element */
#define NF 4       /**< number of faces per element */
#define NN 3       /**< number of vertices per face */
#define NP 19      /**< number of property data */
/*@}*/


/** \name Defines of indicators
*/
/*@{*/
#define C_BND -1   /**< indicator for boundary node/face */
#define C_INT -2   /**< indicator for interior node/face */
#define C_UNK -4   /**< indicator for unknown state */
/*@}*/

#define D_EPS PETSC_MACHINE_EPSILON*100 /**< threshold for equality of two real numbers */

/** \name Defines for physical constants
*/
/*@{*/
#define MU0 (12.566371e-7) /**< mu0 = 4*M_PI*1e-7 = 12.566371e-7 Tm/A (=Vs/Am) */
#define GAMMA (2.210173e5) /**< gamma = mu0*g*|e|/(2*me) [m/As] = 1.758799e+11 [1/(Ts)] (cf. Diplomarbeit Scholz S. 14) */
/*@}*/

/* value of damping constant for which M is locked/rigid */
#define RIGID_M_ALPHA 999

/* replace all calls to cblas with simple macros here
   because we are just coping with ND=3 vectors
   thus, big overhead for calling real cblas functions!
*/

#define my_daxpy(a,b,c,d,e,f) {(e)[0]+=b*(c)[0];(e)[1]+=b*(c)[1];(e)[2]+=b*(c)[2];}
#define my_dcopy(a,b,c,d,e)   {(d)[0]=(b)[0];(d)[1]=(b)[1];(d)[2]=(b)[2];}
#define my_dnrm2(a,b,c)       sqrt((b)[0]*(b)[0]+(b)[1]*(b)[1]+(b)[2]*(b)[2])
#define my_dscal(a,b,c,d)     {(c)[0]*=b;(c)[1]*=b;(c)[2]*=b;}
#define my_ddot(a,b,c,d,e)    ((b)[0]*(d)[0]+(b)[1]*(d)[1]+(b)[2]*(d)[2])
#define douter(a,b,c,d)       {(d)[0]=(b)[1]*(c)[2]-(b)[2]*(c)[1];(d)[1]=(b)[2]*(c)[0]-(b)[0]*(c)[2];(d)[2]=(b)[0]*(c)[1]-(b)[1]*(c)[0];}

/** \brief Contains all data to control the simulations.
*/

#if PETSC_VERSION <= 220
typedef int PetscInt;
typedef int PetscBLASInt;
#endif

typedef struct {
  char      simname[256]; /**< project name, basename for output files (*.inp, *.off, etc.) */
  int       inp;          /**< number of AVS inp file (initially read or next number to be written) */

  PetscReal time;         /**< absolute time */

  int       mode;         /**< integration method (defined in allopt.txt) */

  PetscReal hscale;       /**< scaling factor of all fields and magnetization to dimensionless units */
  PetscReal tscale;       /**< scaling factor for dimensionless time */
  PetscReal escale;       /**< scaling factor for energy */
  PetscReal lenscale;     /**< scaling factor for length */


  /* ************************************************************************
     other variables
  */

  /* mesh data */

/** \name global counters of finite element mesh
*/
/*@{*/
  int n_vert;     /**< number of vertices */
  int n_ele;      /**< number of elements */
/*@}*/

/** \name local counters (on each processor)
*/
/*@{*/
  int ln_vert;    /**< local number of vertices */
  int ln_ele;     /**< local number of elements */
/*@}*/

/** \name vertex data
*/
/*@{*/
  PetscReal *vertxyz;      /**< (x,y,z) coordinates of all vertices */
  int       *vertl2g;      /**< mapping local vertex id -> global vertex id */
  int       *vertnewproc;  /**< assignment of vertices to new processors */
  Vec       vertvol;       /**< "volume" assigned to each vertex */
  int       *vertprop;     /**< property id assigned to each vertex */
/*@}*/

/** \name element data
*/
/*@{*/
  int       *elevert;    /**< each element's corner vertices (global ids) */
  int       *eleprop;    /**< property id assigned to each element */
  int       *elel2g;     /**< local to global element id mapping */
  int       *elenewproc; /**< assignment of elements to new processors */
  Vec       elevol;      /**< element volumes */
  PetscReal elenmax;     /**< maximum edge length (vertex to vertex) in the mesh */
#define HAVE_ELEVERTALL
#ifdef HAVE_ELEVERTALL
  int       *elevertall; /**< each element's corner vertices (bcast to all procs during init.) */
#define HAVE_M2IJ
#ifdef HAVE_M2IJ
  int       *mesh2nodal_ia;
  int       *mesh2nodal_ja;
  int       *mesh2dual_ia;
  int       *mesh2dual_ja;
#endif
#endif
/*@}*/

/** \name surface data
*/
/*@{*/
  int       n_vert_bnd;    /**< number of vertices on the boundary */
  int       n_bnd_fac;     /**< number of boundary faces */
  int       ln_bnd_fac;    /**< local number of faces on the boundary */
  int       *vertbndg2bnd; /**< mapping global vertex id -> "boundary vertex counter" */
  int       *bndfacvert;   /**< table of vertices which belong to each triangular face */
/*@}*/

/** \name material properties
*/
/*@{*/
  int       n_prop; /**< number of material properties (grains) */
  PetscReal *propdat; /**< ::NP material properties:
                            0: theta;
                            1: phi;
                            2: K1;
                            3: K2;
                            4: Js;
                            5: A;
                            6: e_cubic_1_x;
                            7: e_cubic_1_y;
                            8: e_cubic_1_z;
                            9: alpha;
                           10: e_cubic_2_x;
                           11: e_cubic_2_y;
                           12: e_cubic_2_z;
                           13: e_cubic_3_x;
                           14: e_cubic_3_y;
                           15: e_cubic_3_z;
                           16: anisotropy cubic ? 0-false-uniaxial : 1-cubic
                      */
/*@}*/

/* ************************************************************************** */

/** \name Matrices and vectors
*/
/*@{*/
  Vec       M;          /**< magnetization */
  Vec       VHdem;      /**< demagnetizing=magnetostatic field*/
  Vec       VMs3;       /**< vertex volume times Ms */
  PetscReal Edem;       /**< magnetostatic energy */
  Vec       VHexchani;  /**< exchange + uniaxial anisotropy field */
  PetscReal Eexchani;   /**< exchange + uniaxial anisotropy energy */
#ifdef EXCH
  Vec       VHexchonly; /**< exchange field */
  PetscReal Eexchonly;  /**< exchange energy */
#endif
  Vec       VHext;      /**< external field */
  PetscReal Eext;       /**< Zeeman energy */
  Vec       VHtot;      /**< total field */
  PetscReal Etot;       /**< total energy */
/*@}*/

  PetscReal totvol;     /**< total volume of magnetic material (Ms>0) */
  int       equil;      /**< equilibrium reached ? equil++ : equil=0 */
  PetscReal vequil;     /**< measure for equilibrium: max(dM/dt)   TODO: RM*/

#ifdef ADDONS
#include "addons/griddatadd.h"
#endif

#ifdef SPINTORQ
#include "spintorq/griddatadd.h"
#endif

#ifdef EBM
#include "ebm/griddata_ebm.h"
#endif

#ifdef PYTHON
#define MAGPARSTDPYTHONMOD "magpar_std_mod"
  PyObject *pModule;
#endif

} GridData;

#endif
