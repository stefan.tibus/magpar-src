/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: tettri.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "util.h"

PetscReal triarea(PetscReal *v0,PetscReal *v1,PetscReal *v2,PetscReal *n)
{
  PetscReal a[ND],b[ND],outer[ND];
  PetscReal result;

  /* calculate a=v1-v0 */
  my_dcopy(ND,v1,1,a,1);
  my_daxpy(ND,-1.0,v0,1,a,1);

  /* calculate b=v2-v0 */
  my_dcopy(ND,v2,1,b,1);
  my_daxpy(ND,-1.0,v0,1,b,1);

  /* result=0.5*|a x b| */
  douter(ND,a,b,outer);
  result=0.5*my_dnrm2(ND,outer,1);

  if (n!=PETSC_NULL) {
    my_dscal(ND,1.0/(2.0*result),outer,1);
    my_dcopy(ND,outer,1,n,1);
  }

  return(result);
}


PetscReal tetvol(PetscReal *x1,PetscReal *x2,PetscReal *x3,PetscReal *x4)
{
  PetscReal result;

  /* Calculate volume of the element */
  result=(1.0/6.0)*
    (x3[0]*x2[1]*x1[2] - x4[0]*x2[1]*x1[2] - x2[0]*x3[1]*x1[2] + x4[0]*x3[1]*x1[2] +
     x2[0]*x4[1]*x1[2] - x3[0]*x4[1]*x1[2] - x3[0]*x1[1]*x2[2] + x4[0]*x1[1]*x2[2] +
     x1[0]*x3[1]*x2[2] - x4[0]*x3[1]*x2[2] - x1[0]*x4[1]*x2[2] + x3[0]*x4[1]*x2[2] +
     x2[0]*x1[1]*x3[2] - x4[0]*x1[1]*x3[2] - x1[0]*x2[1]*x3[2] + x4[0]*x2[1]*x3[2] +
     x1[0]*x4[1]*x3[2] - x2[0]*x4[1]*x3[2] - x2[0]*x1[1]*x4[2] + x3[0]*x1[1]*x4[2] +
     x1[0]*x2[1]*x4[2] - x3[0]*x2[1]*x4[2] - x1[0]*x3[1]*x4[2] + x2[0]*x3[1]*x4[2]
    );

  return(result);
}

int tetnb(int *tet1, int *tet2, int *vshared)
{
  int t1[NV],t2[NV];

  for (int i=0;i<NV;i++) {
    t1[i]=tet1[i];
    t2[i]=tet2[i];
  }

  /* sort nodes */
  PetscSortInt(NV,(PetscInt*)t1);
  PetscSortInt(NV,(PetscInt*)t2);

  int cntshared=0;

  for (int i=0;i<NV;i++) {
    for (int j=0;j<NV;j++) {
      if (t1[i]==t2[j]) {
        if (vshared!=PETSC_NULL) vshared[cntshared]=t1[i];
        cntshared++;
      }
    }
  }

  assert(cntshared<=NV);

  return(cntshared);
}

int TetSharedVertices(int *v1,int *v2,int *s)
{
  int k;
  k=0;
  for (int i=0;i<NV;i++) {
    for (int j=0;j<NV;j++) {
      if (v1[i]==v2[j]) {
        s[k]=v1[i];
        k++;
        continue;
      }
    }
  }
  return(k);
}
