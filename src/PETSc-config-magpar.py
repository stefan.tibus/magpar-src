#!/usr/bin/env python

import os

configure_options = [
  # set floating point precision
  '--with-precision='+os.environ['PRECISION'],
  #
  # BLAS, LAPACK
  '--with-blas-lapack-dir='+os.environ['ATLAS_DIR'],
  # alternatively set libraries directly
  #'--with-blas-lib=['+os.environ['PD']+'/atlas/lib/libf77blas.a,'+os.environ['PD']+'/atlas/lib/libcblas.a,'+os.environ['PD']+'/atlas/lib/libatlas.a,/usr/lib/libg2c.a]',
  #'--with-lapack-lib='+os.environ['PD']+'/atlas/lib/liblapack.a',
  #
  # define additional required libraries (defined in Makefile.in.$HOSTNAME)
  '--LIBS='+os.environ['PETSC_XTRALIBS'],
  # e.g.
  # /usr/lib/gcc-lib/i386-redhat-linux/2.96/libg2c.a
  # required by lapack/atlas (xerbla.o) on RedHat 7.2
  #
  # /usr/local/lib/libifcore.a
  # required by Intel compilers:
  # otherwise linking of fortran object files with mpicc/icc fails

  # disable C++ and TAO support
  #'--with-cxx=0',
  #'--with-c++-support',

  # MPI
  # ---
  # for MPICH
  #
  '--with-mpi-dir='+os.environ['MPI_DIR'],
  #
  # for LAM/MPI
  #
  #'--with-mpi-include='+os.environ['PD']+'/mpi/include',
  #'--with-mpi-lib=['+os.environ['PD']+'/mpi/lib/libmpi.a,'+os.environ['PD']+'/mpi/lib/liblamf77mpi.a,'+os.environ['PD']+'/mpi/lib/liblam.a]',
  #
  # without MPI (single processor only)
  #
  #'--with-mpi=0',
  #
  # to disable PETSc XWindows functions
  #
  '--with-x=0',
  #
  # Cygwin settings
  # ---------------
  # (disable everything above)
  #
  #'--with-mpi=0',
  #'--with-x=0',
  #
  # optional packages
  # -----------------
  #
  # SuperLU_DIST
  #'--with-superlu-dir='+os.environ['PD']+'/SuperLU_DIST_2.0',
  #'--with-superlu_dist-lib='+os.environ['PD']+'/SuperLU_DIST_2.0/superlu_linux.a',
  #
  # hypre
  #'--download-hypre=1','-with-hypre=1',
  #
  # compiler settings
  # -----------------
  #
  # compile optimized libraries with C++ bindings (required for TAO)
  '--with-clanguage=C++',
  '--with-debugging=0',
  # compiler options
  '-COPTFLAGS='+os.environ['OPTFLAGS'],
  '-CXXOPTFLAGS='+os.environ['OPTFLAGS'],
  '-FOPTFLAGS='+os.environ['OPTFLAGS'],
  # compile only static libraries (TODO: any effect? - see installation instructions!)
  '--with-shared=0',
  '--with-dynamic=0'
  ]

if __name__ == '__main__':
  import configure
  configure.petsc_configure(configure_options)

# Extra options used for testing locally
test_options = []
