// Gmsh geo file for two prisms
box_a=1;
box_b=2;
box_c=5;
dist=0.3;
meshsize=0.3;

// define Cartesian coordinates of points and mesh size on each point
Point (1) = {-box_a, 0, 0, meshsize};
Point (2) = {0, 0, 0, meshsize};
Point (3) = {0, box_b, 0, meshsize};
Point (4) = {-box_a, box_b, 0, meshsize};

Point (5) = {dist+0, box_b, 0, meshsize};
Point (6) = {dist+box_a, box_b, 0, meshsize};
Point (7) = {dist+box_a, 0, 0, meshsize};
Point (8) = {dist+0, 0, 0, meshsize};

// define lines connecting points
Line (1) = {1,2};
Line (2) = {2,3};
Line (3) = {3,4};
Line (4) = {4,1};
Line (5) = {5,6};
Line (6) = {6,7};
Line (7) = {7,8};
Line (8) = {8,5};

// define polygons (line loops)
Line Loop (1) = {1,2,3,4};
Line Loop (2) = {5,6,7,8};

// define surface based on polygon
Plane Surface (1) = {1};
Plane Surface (2) = {2};

// extrude surfaces
Extrude {0,0,box_c} {
  Surface{1,2};
}
Physical Volume(1) = {1};
Physical Volume(2) = {2};
