/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
  $Id: barycent.c 3014 2010-03-26 17:27:32Z scholz $
*/

#include "util.h"
#include "petscblaslapack.h"

/*
  derived from petsc-2.2.1/include/petscblaslapack.h
*/

#if !defined(PETSC_USE_COMPLEX)

#if defined(PETSC_USES_FORTRAN_SINGLE)
#define DGESV   SGESV
#endif

#if defined(PETSC_USE_SINGLE)

#if defined(PETSC_HAVE_FORTRAN_UNDERSCORE) || defined(PETSC_BLASLAPACK_F2C)
#define LAgesv_ sgesv_
#elif defined(PETSC_HAVE_FORTRAN_CAPS)
#define LAgesv_ SGESV
#else
#define LAgesv_ sgesv
#endif

#else

#if defined(PETSC_HAVE_FORTRAN_UNDERSCORE) || defined(PETSC_BLASLAPACK_F2C)
#define LAgesv_ dgesv_
#elif defined(PETSC_HAVE_FORTRAN_CAPS)
#define LAgesv_ DGESV
#else
#define LAgesv_ dgesv
#endif

#endif

#else

#if defined(PETSC_USES_FORTRAN_SINGLE)
#define ZGESV  CGESV
#endif

#if defined(PETSC_HAVE_FORTRAN_UNDERSCORE) || defined(PETSC_BLASLAPACK_F2C)
#define LAgesv_ zgesv_
#elif defined(PETSC_HAVE_FORTRAN_CAPS)
#define LAgesv_ ZGESV
#else
#define LAgesv_ zgesv
#endif

#endif

EXTERN_C_BEGIN
EXTERN void      LAgesv_(const PetscBLASInt*, const PetscBLASInt*, PetscScalar*, const PetscBLASInt*, PetscBLASInt*, PetscScalar*, const PetscBLASInt*, PetscBLASInt*);
EXTERN_C_END


/* calculate barycentric coordinates
   info from: 3Ddata.pdf
   Course in4008 - Data Visualization
   http://visualization.tudelft.nl/frits.html
*/

int barycent(PetscReal *p,PetscReal *a,PetscReal *b,PetscReal *c,PetscReal *d,PetscReal *bary)
{
  my_dcopy(ND,p,1,bary,1);
  my_daxpy(ND,-1.0,d,1,bary,1);

  /* assemble tetmat */
  PetscReal tetmat[ND*ND];
  my_dcopy(ND,a,1,tetmat,1);   my_daxpy(ND,-1.0,d,1,tetmat,1);
  my_dcopy(ND,b,1,tetmat+ND,1);  my_daxpy(ND,-1.0,d,1,tetmat+ND,1);
  my_dcopy(ND,c,1,tetmat+2*ND,1);my_daxpy(ND,-1.0,d,1,tetmat+2*ND,1);

  /* calculate abc=(alpha,beta,gamma) */
  int one=1;
  int three=ND;
  int ipiv[ND];
  int info;
  LAgesv_(&three,&one,tetmat,&three,ipiv,bary,&three,&info);CHKERRQ(info);

  if (bary[0]>=0.0 && bary[1]>=0.0 && bary[2]>=0.0 &&
      (bary[0]+bary[1]+bary[2])<1.0) {
    return(1);
  }
  else {
    return(0);
  }
}

