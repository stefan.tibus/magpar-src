/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: myllgjacobian.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "llg.h"
#include "field/field.h"
#include "util/util.h"

extern Mat Ad2E_dMidMk;

int myLLGJacobian(TS ts,PetscReal t,Vec global_in,Mat *AA,Mat *BB,MatStructure *str,void *ctx)
{
  GridData      *gdata = (GridData*)ctx;
  Mat           A_Jij = *AA;

  int           i;

  MagparFunctionInfoBegin;

/* just init with unity matrix for testing */
/*
  for (i=0; i<ND*gdata->ln_vert; i++) {
    ierr = MatSetValue(A_Jij,i,i,1.0,INSERT_VALUES);CHKERRQ(ierr);
  }

  PetscInfo(0,"A_Jij matrix assembly complete\n");
  ierr = MatAssemblyBegin(A_Jij,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A_Jij,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  *str = SAME_NONZERO_PATTERN;

  MagparFunctionProfReturn(0);
*/

  PetscReal *ta_M,*ta_Htot;
  ierr = VecGetArray(global_in,&ta_M);CHKERRQ(ierr);
  ierr = VecGetArray(gdata->VHtot,&ta_Htot);CHKERRQ(ierr);

#if PETSC_VERSION < 300
  ierr = MatSetOption(A_Jij,MAT_ROWS_SORTED);CHKERRQ(ierr);
#endif
/*
  ierr = MatSetOption(A_Jij,MAT_NEW_NONZERO_LOCATION_ERR);CHKERRQ(ierr);
*/

  PetscInt       nonzeroes;
  const PetscInt *nonzcols;

  PetscTruth info;
  PetscOptionsHasName(PETSC_NULL,"-info",&info);
  if (info) {
    PetscPrintf(PETSC_COMM_WORLD,"Recalculating matrix elements of Jacobian...\n");
  }

  for (i=0; i<gdata->ln_vert; i++) {
    if (info) {
      ierr = ProgressBar(i,gdata->ln_vert,10);
    }

    ierr = MatGetRow(
      Ad2E_dMidMk,
      ND*gdata->vertl2g[i],
      &nonzeroes,
      (const PetscInt**)&nonzcols,
      PETSC_NULL
    );CHKERRQ(ierr);
    ierr = MatRestoreRow(
      Ad2E_dMidMk,
      ND*gdata->vertl2g[i],
      &nonzeroes,
      (const PetscInt**)&nonzcols,
      PETSC_NULL
    );CHKERRQ(ierr);

    /* row=nodes1=i col=nodes2=j */

    PetscReal *xi,*hi;
    xi=ta_M+ND*i;
    hi=ta_Htot+ND*i;

    int rows[ND];
    rows[0]=gdata->vertl2g[i]*ND;
    rows[1]=rows[0]+1;
    rows[2]=rows[0]+2;

    /* TODO: incorrect for mode 2: large damping, no precession for t<0 */
    PetscReal  a1,alpha;
    alpha=gdata->propdat[NP*gdata->vertprop[i]+9];

    /* skip calculation of Jacobian if Ms=0 or M is locked */
    if (gdata->propdat[NP*gdata->vertprop[i]+4]<=0.0 || alpha>=RIGID_M_ALPHA) continue;

    a1=1.0/(1.0+alpha*alpha);

    int j;
    j=-1;
    /* k increased by 1: matrix elements might be 0 by chance and therefore not stored! */
    for (int k=0; k<nonzeroes; k++) {
      /* skip this triplet, if we have already completed it */
      if (nonzcols[k]/ND==j) continue;

      j=nonzcols[k]/ND;

/*    does not work: matrix elements might be 0 by chance and
      therefore not stored

      assert(nonzcols[k]%ND==0);
*/

      int cols[ND];
      cols[0]=ND*j;
      cols[1]=ND*j+1;
      cols[2]=ND*j+2;

      PetscReal dHdM[ND*ND];
      MatGetValues(Ad2E_dMidMk,ND,(PetscInt*)rows,ND,(PetscInt*)cols,dHdM);

      /* row/column index correct ?
         dHdM should be symmetric anyway !? no: due to MatDiagonalScale !
      */

      PetscReal  *dh0dxi;
      PetscReal  *dh1dxi;
      PetscReal  *dh2dxi;

      dh0dxi = dHdM+ND*0;
      dh1dxi = dHdM+ND*1;
      dh2dxi = dHdM+ND*2;

      /* TODO: explain this: why is this correct?
         without the else path: faster, but sphere_larmor does not preserve E, Mz!

         Local field hi[]!=0 only on local node (rows[0]==cols[0]).
         However, dHidMj!=0 also on neighboring node
      */
      /* TODO: incorrect for mode 2: large damping, no precession for t<0 */
      PetscReal Jij[ND*ND];
      if (rows[0]==cols[0]) {
        Jij[ND*0+0] = -(a1*(      -xi[2]*dh1dxi[0]+xi[1]*dh2dxi[0]))-a1*alpha*(   xi[1]*hi[1]+  xi[2]*hi[2]-xi[1]*xi[1]*dh0dxi[0]-xi[2]*xi[2]*dh0dxi[0]+xi[0]*xi[1]*dh1dxi[0]+xi[0]*xi[2]*dh2dxi[0]);
        Jij[ND*1+0] = -(a1*(-hi[2]+xi[2]*dh0dxi[0]-xi[0]*dh2dxi[0]))-a1*alpha*(   xi[1]*hi[0]-2*xi[0]*hi[1]+xi[0]*xi[1]*dh0dxi[0]-xi[0]*xi[0]*dh1dxi[0]-xi[2]*xi[2]*dh1dxi[0]+xi[1]*xi[2]*dh2dxi[0]);
        Jij[ND*2+0] = -(a1*( hi[1]-xi[1]*dh0dxi[0]+xi[0]*dh1dxi[0]))-a1*alpha*(   xi[2]*hi[0]-2*xi[0]*hi[2]+xi[0]*xi[2]*dh0dxi[0]+xi[1]*xi[2]*dh1dxi[0]-xi[0]*xi[0]*dh2dxi[0]-xi[1]*xi[1]*dh2dxi[0]);

        Jij[ND*0+1] = -(a1*( hi[2]-xi[2]*dh1dxi[1]+xi[1]*dh2dxi[1]))-a1*alpha*(-2*xi[1]*hi[0]+  xi[0]*hi[1]-xi[1]*xi[1]*dh0dxi[1]-xi[2]*xi[2]*dh0dxi[1]+xi[0]*xi[1]*dh1dxi[1]+xi[0]*xi[2]*dh2dxi[1]);
        Jij[ND*1+1] = -(a1*(       xi[2]*dh0dxi[1]-xi[0]*dh2dxi[1]))-a1*alpha*(   xi[0]*hi[0]+  xi[2]*hi[2]+xi[0]*xi[1]*dh0dxi[1]-xi[0]*xi[0]*dh1dxi[1]-xi[2]*xi[2]*dh1dxi[1]+xi[1]*xi[2]*dh2dxi[1]);
        Jij[ND*2+1] = -(a1*(-hi[0]-xi[1]*dh0dxi[1]+xi[0]*dh1dxi[1]))-a1*alpha*(   xi[2]*hi[1]-2*xi[1]*hi[2]+xi[0]*xi[2]*dh0dxi[1]+xi[1]*xi[2]*dh1dxi[1]-xi[0]*xi[0]*dh2dxi[1]-xi[1]*xi[1]*dh2dxi[1]);

        Jij[ND*0+2] = -(a1*(-hi[1]-xi[2]*dh1dxi[2]+xi[1]*dh2dxi[2]))-a1*alpha*(-2*xi[2]*hi[0]+  xi[0]*hi[2]-xi[1]*xi[1]*dh0dxi[2]-xi[2]*xi[2]*dh0dxi[2]+xi[0]*xi[1]*dh1dxi[2]+xi[0]*xi[2]*dh2dxi[2]);
        Jij[ND*1+2] = -(a1*( hi[0]+xi[2]*dh0dxi[2]-xi[0]*dh2dxi[2]))-a1*alpha*(-2*xi[2]*hi[1]+  xi[1]*hi[2]+xi[0]*xi[1]*dh0dxi[2]-xi[0]*xi[0]*dh1dxi[2]-xi[2]*xi[2]*dh1dxi[2]+xi[1]*xi[2]*dh2dxi[2]);
        Jij[ND*2+2] = -(a1*(      -xi[1]*dh0dxi[2]+xi[0]*dh1dxi[2]))-a1*alpha*(   xi[0]*hi[0]+  xi[1]*hi[1]+xi[0]*xi[2]*dh0dxi[2]+xi[1]*xi[2]*dh1dxi[2]-xi[0]*xi[0]*dh2dxi[2]-xi[1]*xi[1]*dh2dxi[2]);
      }
      else {
        Jij[ND*0+0] = -(a1*(      -xi[2]*dh1dxi[0]+xi[1]*dh2dxi[0]))-a1*alpha*(                            -xi[1]*xi[1]*dh0dxi[0]-xi[2]*xi[2]*dh0dxi[0]+xi[0]*xi[1]*dh1dxi[0]+xi[0]*xi[2]*dh2dxi[0]);
        Jij[ND*1+0] = -(a1*(       xi[2]*dh0dxi[0]-xi[0]*dh2dxi[0]))-a1*alpha*(                            +xi[0]*xi[1]*dh0dxi[0]-xi[0]*xi[0]*dh1dxi[0]-xi[2]*xi[2]*dh1dxi[0]+xi[1]*xi[2]*dh2dxi[0]);
        Jij[ND*2+0] = -(a1*(      -xi[1]*dh0dxi[0]+xi[0]*dh1dxi[0]))-a1*alpha*(                            +xi[0]*xi[2]*dh0dxi[0]+xi[1]*xi[2]*dh1dxi[0]-xi[0]*xi[0]*dh2dxi[0]-xi[1]*xi[1]*dh2dxi[0]);

        Jij[ND*0+1] = -(a1*(      -xi[2]*dh1dxi[1]+xi[1]*dh2dxi[1]))-a1*alpha*(                            -xi[1]*xi[1]*dh0dxi[1]-xi[2]*xi[2]*dh0dxi[1]+xi[0]*xi[1]*dh1dxi[1]+xi[0]*xi[2]*dh2dxi[1]);
        Jij[ND*1+1] = -(a1*(       xi[2]*dh0dxi[1]-xi[0]*dh2dxi[1]))-a1*alpha*(                            +xi[0]*xi[1]*dh0dxi[1]-xi[0]*xi[0]*dh1dxi[1]-xi[2]*xi[2]*dh1dxi[1]+xi[1]*xi[2]*dh2dxi[1]);
        Jij[ND*2+1] = -(a1*(      -xi[1]*dh0dxi[1]+xi[0]*dh1dxi[1]))-a1*alpha*(                            +xi[0]*xi[2]*dh0dxi[1]+xi[1]*xi[2]*dh1dxi[1]-xi[0]*xi[0]*dh2dxi[1]-xi[1]*xi[1]*dh2dxi[1]);

        Jij[ND*0+2] = -(a1*(      -xi[2]*dh1dxi[2]+xi[1]*dh2dxi[2]))-a1*alpha*(                            -xi[1]*xi[1]*dh0dxi[2]-xi[2]*xi[2]*dh0dxi[2]+xi[0]*xi[1]*dh1dxi[2]+xi[0]*xi[2]*dh2dxi[2]);
        Jij[ND*1+2] = -(a1*(       xi[2]*dh0dxi[2]-xi[0]*dh2dxi[2]))-a1*alpha*(                             xi[0]*xi[1]*dh0dxi[2]-xi[0]*xi[0]*dh1dxi[2]-xi[2]*xi[2]*dh1dxi[2]+xi[1]*xi[2]*dh2dxi[2]);
        Jij[ND*2+2] = -(a1*(      -xi[1]*dh0dxi[2]+xi[0]*dh1dxi[2]))-a1*alpha*(                             xi[0]*xi[2]*dh0dxi[2]+xi[1]*xi[2]*dh1dxi[2]-xi[0]*xi[0]*dh2dxi[2]-xi[1]*xi[1]*dh2dxi[2]);
      }
      ierr = MatSetValues(A_Jij,ND,(PetscInt*)rows,ND,(PetscInt*)cols,Jij,INSERT_VALUES);CHKERRQ(ierr);
    }

#undef MATFLUSH
#ifdef MATFLUSH
    /* flush caches to save memory (and avoid swapping) */
    if (i % 100 == 99) {
      ierr = MatAssemblyBegin(A_Jij,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
      ierr = MatAssemblyEnd(A_Jij,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
    }
#endif
  }

#ifdef MATFLUSH
  /* continue assembling to stay in sync with other processors
     (MatAssembly is collective!)
  */
  for (;i<gdata->n_vert;i++) {
    if (i % 100 == 99) {
      ierr = MatAssemblyBegin(A_Jij,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
      ierr = MatAssemblyEnd(A_Jij,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
    }
  }
#endif

  if (info) {
    ierr = ProgressBar(1,1,10);
  }

  PetscInfo(0,"A_Jij matrix assembly complete\n");
  ierr = MatAssemblyBegin(A_Jij,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A_Jij,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

/*
  ierr = MatView(A_Jij,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
*/

  VecRestoreArray(global_in,&ta_M );
  VecRestoreArray(gdata->VHtot,&ta_Htot);

  /* TODO: optimize: */
/*
  *str = SAME_NONZERO_PATTERN;
*/
  *str = DIFFERENT_NONZERO_PATTERN;

  MagparFunctionProfReturn(0);
}

