/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: calc_dMdt.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "llg.h"
#include "util/util.h"
#include "field/field.h"
#ifdef SPINTORQ
#include "spintorq/spintorq.h"
#endif

int calc_dMdt(TS ts, PetscReal dt, Vec invec, Vec outvec, void *ptr)
{
  GridData *gdata = (GridData*)ptr;

  MagparFunctionInfoBegin;

  /* DEBUG
  assert(gdata->M==invec);
  */

  ierr = Htot(gdata);CHKERRQ(ierr);

  PetscReal *ta_M, *ta_Htot, *ta_outvec;
  VecGetArray(gdata->M,&ta_M);
  VecGetArray(gdata->VHtot,&ta_Htot);
  VecGetArray(outvec,&ta_outvec);

#ifdef SPINTORQ
  PetscReal  *ta_Hspintorque;
  PetscReal  *ta_Hspinforce;
  if (gdata->VHspintorque!=PETSC_NULL)  VecGetArray(gdata->VHspintorque,&ta_Hspintorque);
  else if (gdata->VHspinforce!=PETSC_NULL) VecGetArray(gdata->VHspinforce,&ta_Hspinforce);
#endif

  for (int i=0;i<gdata->ln_vert;i++) {
    PetscReal *torq, damp[ND];
    PetscReal a1,alpha;

    torq=ta_outvec+ND*i;

    /* skip non-magnetic vertices and vertices with alpha >= RIGID_M_ALPHA */
    if (gdata->propdat[NP*gdata->vertprop[i]+4]<=0.0 ||
        gdata->propdat[NP*gdata->vertprop[i]+9]>=RIGID_M_ALPHA) {
      torq[0]=0.0;
      torq[1]=0.0;
      torq[2]=0.0;
      continue;
    }

    douter(ND,ta_M+ND*i,ta_Htot+ND*i,torq);
    douter(ND,ta_M+ND*i,torq,damp);

    alpha=gdata->propdat[NP*gdata->vertprop[i]+9];
    a1=1.0/(1.0+alpha*alpha);

    if (gdata->mode==2 && gdata->time<0) {
      /* relax with large damping and no precession */
      a1=2.0;
      my_dcopy(ND,damp,1,torq,1);
      my_dscal(ND,-a1,torq,1);
    }
    else {
      my_dscal(ND,-a1,torq,1);
      my_daxpy(ND,-a1*alpha,damp,1,torq,1);
    }

#ifdef SPINTORQ
    /* FIXME: move to hspin*.c */
    PetscReal spintorq[ND], spindamp[ND];
    if (gdata->VHspintorque!=PETSC_NULL) {
      douter(ND,ta_M+ND*i,ta_Hspintorque+ND*i,spintorq);
      douter(ND,ta_M+ND*i,spintorq,spindamp);
      my_daxpy(ND,+a1*alpha,spintorq,1,torq,1);
      my_daxpy(ND,-a1,spindamp,1,torq,1);
    }
    else if (gdata->VHspinforce!=PETSC_NULL) {
      PetscReal  nonadiabatic;

      nonadiabatic=gdata->propdat_e[NPe*gdata->vertprop[i]+2];

      douter(ND,ta_M+ND*i,ta_Hspinforce+ND*i,spintorq);
      douter(ND,ta_M+ND*i,spintorq,spindamp);
      my_daxpy(ND,-a1*nonadiabatic+a1*alpha,spintorq,1,torq,1);
      my_daxpy(ND,-a1*alpha*nonadiabatic-a1,spindamp,1,torq,1);
    }
#endif
  }

  VecRestoreArray(gdata->M,&ta_M);
  VecRestoreArray(gdata->VHtot,&ta_Htot);
  VecRestoreArray(outvec,&ta_outvec);

#ifdef SPINTORQ
  if (gdata->VHspintorque!=PETSC_NULL)  VecRestoreArray(gdata->VHspintorque,&ta_Hspintorque);
  else if (gdata->VHspinforce!=PETSC_NULL) VecRestoreArray(gdata->VHspinforce,&ta_Hspinforce);
#endif

  MagparFunctionProfReturn(0);
}

