/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: bbox2.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "util.h"

int bbox2(GridData *gdata,PetscReal *vertxyz2,int n_prop,int *propid,PetscReal *bbox,int *pix)
{
  MagparFunctionLogBegin;

  ierr = calcbbox(gdata->ln_ele,gdata->elevert,gdata->eleprop,vertxyz2,n_prop,propid,bbox);

  /* find longest edge (neglecting z-direction!) */
  PetscReal maxedge=0;
  for (int i=0;i<ND-1;i++) {
    maxedge=PetscMax(maxedge,bbox[ND+i]-bbox[i]);
  }

  int res;
  res=pix[0];

  /* add approx. 1 pixel around the sample (i.e. 2 pixel to each edge length) */
  for (int i=0;i<ND;i++) {
    bbox[i]    -= 1.0*maxedge/(res-2);
    bbox[ND+i] += 1.0*maxedge/(res-2);
  }

  /* shift slightly */
  for (int i=0;i<ND;i++) {
    bbox[i]    += 0.1*maxedge/(res-2);
    bbox[ND+i] += 0.1*maxedge/(res-2);
  }
  /* adjust maxedge length */
  maxedge = maxedge*res/(res-2);

  PetscPrintf(PETSC_COMM_WORLD,
    "enlarged bbox: (%g,%g,%g) (%g,%g,%g) (%g,%g,%g)\nmaxedge: %g\n",
    bbox[0],bbox[1],bbox[2],bbox[ND+0],bbox[ND+1],bbox[ND+2],
    bbox[ND+0]-bbox[0],bbox[ND+1]-bbox[1],bbox[ND+2]-bbox[2],
    maxedge
  );

  /* calculate number of pixels */
  if (pix!=PETSC_NULL) {
    for (int i=0;i<ND;i++) {
      pix[i]=(int) ceil(res*(bbox[ND+i]-bbox[i])/maxedge);
    }

    PetscPrintf(PETSC_COMM_WORLD,
      "pixels: %i x %i x %i\n",
      pix[0],pix[1],pix[2]
    );
    PetscPrintf(PETSC_COMM_WORLD,
      "pixellength: %g x %g x %g = %g * (%g x %g x %g)\n",
      (bbox[ND+0]-bbox[0])/pix[0],
      (bbox[ND+1]-bbox[1])/pix[1],
      (bbox[ND+2]-bbox[2])/pix[2],
      (bbox[ND+0]-bbox[0])/pix[0],
      1.0,
      (bbox[ND+1]-bbox[1])/pix[1]/((bbox[ND+0]-bbox[0])/pix[0]),
      (bbox[ND+2]-bbox[2])/pix[2]/((bbox[ND+0]-bbox[0])/pix[0])
    );
  }

  MagparFunctionLogReturn(0);
}

