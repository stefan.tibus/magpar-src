/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: parinit.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "init.h"
#include "io/magpario.h"
#include "field/field.h"

#ifdef SUNDIALS_VERSION
#include "llg/llg.h"
#endif

#ifdef TAO
#include "emini/emini.h"
#endif

#ifdef PNG
#include "png/writepng.h"
#endif

#ifdef ADDONS
#include "addons/addons.h"
#include "llgts/llgts.h"
#include "llg2/llg2.h"
#include "brown/brown.h"
#endif

#ifdef EBM
#include "ebm/ebm.h"
#endif

int ParInit(GridData *gdata)
{
  PetscTruth   flg;

  MagparFunctionLogBegin;

  PetscPrintf(MPI_COMM_WORLD,
    "--------------------------------------------------------------------------\n"
    "Starting parallel part\n\n"
  );

  /* initialize mode here: Hdemag needs it */
  ierr = PetscOptionsGetInt(PETSC_NULL,"-mode",(PetscInt*)&gdata->mode,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    gdata->mode=99;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -mode not found, using default value: %i\n",
      gdata->mode
    );
  }
  PetscPrintf(PETSC_COMM_WORLD,"run mode: %i\n",gdata->mode);

  /* modify material properties */
  ierr = ModifyPropPar(gdata);CHKERRQ(ierr);

  ierr = EleVertVol(gdata);CHKERRQ(ierr);

  gdata->equil=0;
  /* Initialize with large value to avoid artificial equilibrium at start of simulation.
     Also done in first call to EquilCheck().
  */
  gdata->vequil=1.0;

  PetscReal  tstart;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-ts_init_time",&tstart,&flg);CHKERRQ(ierr);
  if (flg!=PETSC_TRUE) {
    tstart=0;
    PetscPrintf(PETSC_COMM_WORLD,
      "Option -ts_init_time not found, using default value: %g\n",
      tstart
    );
  }
  PetscPrintf(PETSC_COMM_WORLD,"initial time: %g\n",tstart);
  tstart /= 1e9*gdata->tscale;
  PetscPrintf(PETSC_COMM_WORLD,"reduced initial time:      %g (1)\n",tstart);
  gdata->time=tstart;

  ierr = Htot(gdata);CHKERRQ(ierr);
  ierr = Htot_Energy(gdata);CHKERRQ(ierr);

  switch (gdata->mode) {
    case 0:
      /* LLG time integration */
#ifdef SUNDIALS_VERSION
      ierr = myTSCreatePVode(gdata);CHKERRQ(ierr);
      ierr = WriteFEMAVS(gdata);CHKERRQ(ierr);
      ierr = CheckIterationLLG(gdata);CHKERRQ(ierr);
#else
      SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"PVODE not compiled in! Exiting!\n");
#endif
    break;
    case 1:
      /* energy minimization */
#ifdef TAO
      ierr = myTSCreateEmini(gdata);CHKERRQ(ierr);
      ierr = WriteFEMAVS(gdata);CHKERRQ(ierr);
      ierr = CheckIterationEmini(gdata);CHKERRQ(ierr);
#else
      SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"TAO not compiled in! Exiting!\n");
#endif
    break;
    case 2:
      /* relax with high damping and no precession during t<0,
         then switch to normal damping
      */
#ifdef SUNDIALS_VERSION
      ierr = myTSCreatePVode(gdata);CHKERRQ(ierr);
      ierr = WriteFEMAVS(gdata);CHKERRQ(ierr);
      ierr = CheckIterationLLG(gdata);CHKERRQ(ierr);
#else
      SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"PVODE not compiled in! Exiting!\n");
#endif
    break;
      /* relax with energy minimization, then switch to LLG time integration */
    case 3:
#ifdef TAO
      ierr = myTSCreateEmini(gdata);CHKERRQ(ierr);
      ierr = WriteFEMAVS(gdata);CHKERRQ(ierr);
      ierr = CheckIterationEmini(gdata);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,">> EminiSolve: Relaxing magnetization using energy minimization\n");
      ierr = EminiSolve(gdata);CHKERRQ(ierr);
      gdata->inp=PetscAbsInt(gdata->inp);

#ifdef SUNDIALS_VERSION
      ierr = myTSCreatePVode(gdata);CHKERRQ(ierr);
      ierr = CheckIterationLLG(gdata);CHKERRQ(ierr);
#else
      SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"PVODE not compiled in! Exiting!\n");
#endif

#else
      SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"TAO not compiled in! Exiting!\n");
#endif
    break;
#ifdef ADDONS
    case 4:
      ierr = WriteFEMAVS(gdata);CHKERRQ(ierr);
      ierr = CheckIterationLLG2(gdata);CHKERRQ(ierr);
    break;
#endif
    case 50:
#ifdef EBM
      /* nudged elastic band method */
      ierr = myTSCreateEBM(gdata);CHKERRQ(ierr);
      ierr = WriteFEMAVS(gdata);CHKERRQ(ierr);
#else
      SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"EBM not compiled in! Exiting!\n");
#endif
    break;
#ifdef ADDONS
    case 51:
      /* LLG time integration with PETSc internal ODE solvers */
      ierr = myTSCreate(gdata);CHKERRQ(ierr);
      ierr = WriteFEMAVS(gdata);CHKERRQ(ierr);
      ierr = CheckIterationLLGTS(gdata);CHKERRQ(ierr);
    break;
    case 52:
      ierr = BrownInit(gdata);CHKERRQ(ierr);
    break;
    case 53:
      /* energy minimization with non-linear magnet */
#ifdef TAO
      /* shorten magnetization vectors to avoid starting in saturation */
      ierr = VecScale(gdata->M,0.1);CHKERRQ(ierr);
      /* need to recalculate field, energy after scaling M */
      ierr = Htot(gdata);CHKERRQ(ierr);
      ierr = Htot_Energy(gdata);CHKERRQ(ierr);

      ierr = myTSCreateEmini(gdata);CHKERRQ(ierr);
      ierr = WriteFEMAVS(gdata);CHKERRQ(ierr);
      ierr = CheckIterationEmini(gdata);CHKERRQ(ierr);
#else
      SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"TAO not compiled in! Exiting!\n");
#endif
    break;
#endif
    case 99:
      /* just calculate fields, energies, etc. once and exit */
      ierr = WriteLog(gdata);CHKERRQ(ierr);
      ierr = WriteFEMAVS(gdata);CHKERRQ(ierr);
      ierr = WriteSet(gdata);CHKERRQ(ierr);
    break;
    default:
      SETERRQ(PETSC_ERR_ARG_OUTOFRANGE,"-mode out of valid range! Exiting!\n");
  }

  /* destroy various objects, which are not required any more */
  ierr = DataDestroyInit(gdata);CHKERRQ(ierr);

  MagparFunctionLogReturn(0);
}
