/*
    This file is part of magpar.

    Copyright (C) 2006-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
  $Id: distint.c 2962 2010-02-04 19:50:44Z scholz $
*/

#include "util.h"

int distint(int from, int to, int in)
{
  int out;
  if (from<=to) {
    out=in;
  }
  else {
    int i,j;

    i=from/to;
    j=from%to;
    if (in<j*(i+1)) {
      out=in/(i+1);
    }
    else {
      out=(in-j)/i;
    }
  }

  return(out);
}
