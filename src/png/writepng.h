/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* $Id: writepng.h 2962 2010-02-04 19:50:44Z scholz $ */

#ifndef WRITEPNG_H
#define WRITEPNG_H

#include "griddata.h"

#include "png.h"

int WritePNG(GridData *gdata);
int WritePNGinit(GridData *gdata);

int WritePNG2(GridData *gdata);
int WritePNG2init(GridData *gdata);

int WritePNGfile(char *file_name, int width, int height, png_bytepp image, char *desc);

#endif
