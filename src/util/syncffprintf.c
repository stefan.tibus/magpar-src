/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: syncffprintf.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "util.h"

#ifdef ZLIB
EXTERN_C_BEGIN
#include "zlib.h"
EXTERN_C_END
#endif

int SynchronizedFastFPrintf(FILE *fd, int nchars, int nmax, char *wbufs, PetscTruth zip)
{
  int          i;

  MPI_Status   status;

  MagparFunctionInfoBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  /* set null character and send it even if nchars==0 */
  assert(nmax>0);   /* otherwise the assignment might cause a SEGV */
  if (nchars==0) {
    wbufs[0]=0;
  }
  
  /* to include the null character at the end */
  nchars++;

  if (!rank) {
#ifdef ZLIB
    if (zip==PETSC_TRUE) {
      ierr = gzwrite(fd,wbufs,nchars-1); /* skip the trailing \000 */
/*
      ierr = gzprintf(fd,"%s",wbufs);
*/
    }
    else {
#endif
    ierr = PetscFPrintf(PETSC_COMM_WORLD,fd,"%s",wbufs);CHKERRQ(ierr);
#ifdef ZLIB
    }
#endif
    for (i=1;i<size;i++) {
      ierr = MPI_Recv(&nchars,1,MPI_INT,i,0,PETSC_COMM_WORLD,&status);CHKERRQ(ierr);
      /* check that we do not exceed the size of the buffer */
      assert(nchars<nmax);
      ierr = MPI_Recv(wbufs,nchars,MPI_CHAR,i,0,PETSC_COMM_WORLD,&status);CHKERRQ(ierr);

#ifdef ZLIB
      if (zip==PETSC_TRUE) {
        ierr = gzwrite(fd,wbufs,nchars-1); /* skip the trailing \000 */
/*
        ierr = gzprintf(fd,"%s",wbufs);
*/
      }
      else {
#endif
      ierr = PetscFPrintf(PETSC_COMM_WORLD,fd,"%s",wbufs);CHKERRQ(ierr);
#ifdef ZLIB
      }
#endif
    }
  }
  else {
    ierr = MPI_Send(&nchars,1,MPI_INT,0,0,PETSC_COMM_WORLD);CHKERRQ(ierr);
    ierr = MPI_Send(wbufs,nchars,MPI_CHAR,0,0,PETSC_COMM_WORLD);CHKERRQ(ierr);
  }

  MagparFunctionInfoReturn(0);
}

