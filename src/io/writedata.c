/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: writedata.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "magpario.h"
#include "util/util.h"
#ifdef ADDONS
#include "addons/addons.h"
#endif
#ifdef PNG
#include "png/writepng.h"
#endif

int WriteSet(GridData *gdata)
{
  MagparFunctionInfoBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  /* check if we are supposed to write output data */
  if (gdata->inp<0) MagparFunctionInfoReturn(0);
  int maxint=10000;
  if (gdata->inp>maxint) {
    PetscPrintf(PETSC_COMM_WORLD,
      "inp number reached maximum: %i\n"
      "no output files stored!\n",
      maxint
    );
    MagparFunctionInfoReturn(0);
  }

  /* do not evaluate return value of WriteAVS
     with "CHKERRQ(ierr);" but keep going
     gzopen fails sometimes for unknown reasons
  */
  WriteAVS(gdata);
  ierr = WriteDat(gdata);CHKERRQ(ierr);
#ifdef PNG
  ierr = WritePNG(gdata);CHKERRQ(ierr);
  ierr = WritePNG2(gdata);CHKERRQ(ierr);
#endif
#ifdef ADDONS
  ierr = WriteFld(gdata);CHKERRQ(ierr);
#endif

/* do this in writelog.c !
  ierr = WriteMavg(gdata);CHKERRQ(ierr);
*/

/* do these just during initialization
  ierr = WriteFEMAVS(gdata);CHKERRQ(ierr);
*/


  /* increment inp counter and indicate that we just wrote everything */
  gdata->inp=-(gdata->inp+1);

  MagparFunctionInfoReturn(0);
}

