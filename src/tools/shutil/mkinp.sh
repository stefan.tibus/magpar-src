#!/bin/bash

# bash required for $[] calculations!
# alternatively use sh and bc

if [ $# -lt 2 ]; then
  echo "Syntax: $0 <femsh-file> <inp-data-file> ..."
  echo "  <femsh-file>:    geometry data (vertex xyz data, element data)"
  echo "  <inp-data-file>: vertex and element data"
  exit
fi


femsh=$1
echo "Using mesh geometry from $femsh:"
nv=`head -1 $femsh | awk '{print $1}'`
ne=`head -1 $femsh | awk '{print $2}'`
echo "$nv vortices"
echo "$ne elements"
echo ""

shift
while [ $1 = $femsh ]; do
  echo "Skipping $1"
  shift
done

while [ $# -gt 0 ]; do
  echo "Processing $1 ..."
  
  fdat=`basename $1 .gz`
  if [ $1 != $fdat ]; then
    gunzip -c $1 > $fdat
  fi
  nvd=`head -1 $fdat | awk '{print $1}'`
  nedstart=$[ 1 + $nvd + $nv + 1 ]
  if [ $nedstart -gt `wc -l $fdat | awk '{print $1}'` ]; then
    ned=0
  else
    ned=`head -$nedstart $fdat | tail -1 | awk '{print $1}'`
  fi

  ned2=0
  
  (
    echo "$nv $ne $nvd $ned $ned2"
    tail --lines=+2 $femsh
    cat $fdat
  ) > $fdat.inp

  if [ $1 != $fdat ]; then
    rm $fdat
  fi

  shift
  while [ $# -gt 0 ] && [ $fdat = $femsh ]; do
    echo "Skipping $fdat"
    shift
  done

done
