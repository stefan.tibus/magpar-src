#!/bin/bash

# bash required for $[] calculations!
# alternatively use sh and bc

if [ $# -lt 2 ]; then
  echo "Syntax: $0 <datmsh-file> <d-file> ..."
  echo "  <datmsh-file>: sampling line geometry"
  echo "  <d-file>:      data"
  exit
fi


femsh=$1
echo "Using sampling line geometry from $femsh:"

shift
while [ $1 = $femsh ]; do
  echo "Skipping $1"
  shift
done

while [ $# -gt 0 ]; do
  echo "Processing $1 ..."
  
  fdat=`basename $1 .gz`
  if [ $1 != $fdat ]; then
    gunzip -c $1 > $fdat
  fi

  paste $femsh $fdat > `basename $fdat .d`.dat

  if [ $1 != $fdat ]; then
    rm $fdat
  fi

  shift
  while [ $# -gt 0 ] && [ $fdat = $femsh ]; do
    echo "Skipping $fdat"
    shift
  done

done
