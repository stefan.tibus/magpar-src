/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: destroyinit.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "init.h"

int DataDestroyInit(GridData *gdata)
{
  MagparFunctionLogBegin;

  int rank,size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);

  ierr = PetscFree(gdata->bndfacvert);CHKERRQ(ierr);
  gdata->bndfacvert=PETSC_NULL;
  ierr = PetscFree(gdata->vertbndg2bnd);CHKERRQ(ierr);
  gdata->vertbndg2bnd=PETSC_NULL;

#ifdef HAVE_ELEVERTALL
  if (size>1) {
    ierr = PetscFree(gdata->elevertall);CHKERRQ(ierr);
  }
#ifdef HAVE_M2IJ
  ierr = PetscFree(gdata->mesh2nodal_ia);CHKERRQ(ierr);
  ierr = PetscFree(gdata->mesh2nodal_ja);CHKERRQ(ierr);
  ierr = PetscFree(gdata->mesh2dual_ia);CHKERRQ(ierr);
  ierr = PetscFree(gdata->mesh2dual_ja);CHKERRQ(ierr);
#endif
#endif

  /* Don't destroy vertvol array because FieldSmoothInit needs it and
   * FieldSmoothInit can be called after DataDestroyInit when
   * Hdemag==off and Emini is used (eminisolve.c calls FieldSmoothApply)
   * e.g. in example iface

  ierr = VecDestroy(gdata->vertvol);CHKERRQ(ierr);
  gdata->vertvol=PETSC_NULL;
  */

  MagparFunctionLogReturn(0);
}
