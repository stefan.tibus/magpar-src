#!/bin/bash

# bash required for $[] calculations!
# alternatively use sh and bc

if [ $# -lt 2 ]; then
  echo "Syntax: $0 <femsh-file> <inp-data-file> ..."
  echo "  <femsh-file>:    geometry data (vertex xyz data, element data)"
  echo "  <inp-data-file>: vertex and element data"
  exit
fi


femsh=$1
shift

while [ $# -gt 0 ]; do
  mkinp $femsh $1
  inp2dx `basename $1 .gz`.inp > `basename $1 .gz`.inp.dx
  rm `basename $1 .gz`.inp

  shift
done
