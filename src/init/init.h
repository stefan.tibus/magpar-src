/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* $Id: init.h 2962 2010-02-04 19:50:44Z scholz $ */

#ifndef INIT_H
#define INIT_H

#include "griddata.h"

int CheckPartition(GridData *gdata,int parts);
int CleanUp(GridData *gdata);
int DataCreate(GridData *gdata);
int DataDestroy(GridData *gdata);
int DataDestroyInit(GridData *gdata);
int DataMoveData(GridData *gdata);
int DataPartitionElements(GridData *gdata);
int DataPartitionElementsSer(GridData *gdata);
int DistortMesh(GridData *gdata);
int EleVertVol(GridData *gdata);
int FacNB(GridData *gdata);
int FilterElements(int *pn_ele,int **pelevert,int **peleprop,PetscReal *propdat);
int FilterNodes(int *pn_vert, int *pn_ele, PetscScalar **pvertxyz, int *elevert, Vec *pM, Vec *pVH1, Vec *pVH2);
int InitInfo();
int MagInit(GridData *gdata);
int MagSet(int pid,int init_mag,PetscReal init_magparm,GridData *gdata);
int ModifyPropPar(GridData *gdata);
int ModifyPropSer(GridData *gdata);
int ParInit(GridData *gdata);
int PermuteData(GridData *gdata,IS isvert,IS isele);
int RegularRefinement(GridData *gdata);
int Reorder(GridData *gdata);
int SerInit(GridData *gdata);
int StiffMat(GridData *gdata);
int VertProp(GridData *gdata);
int cmp_int(const int *a, const int *b);
PetscReal tetvol(PetscReal *x1,PetscReal *x2,PetscReal *x3,PetscReal *x4);
PetscReal tetgrad(PetscReal *x1,PetscReal *x2,PetscReal *x3,PetscReal *x4,PetscReal D_etaj[][ND]);

#endif
