/*
    This file is part of magpar.

    Copyright (C) 2002-2010 Werner Scholz

    www:   http://www.magpar.net/
    email: magpar(at)magpar.net

    magpar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    magpar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with magpar; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

static char const Id[] = "$Id: cart2sphere.c 2962 2010-02-04 19:50:44Z scholz $\n\n";
static char const Td[] = "$Today: " __FILE__ "  " __DATE__ "  "  __TIME__  " $\n\n";

#include "util.h"

int Sphere2Cart(Vec S, Vec C)
{
  MagparFunctionInfoBegin;

  int t_Clen,t_Slen;
  ierr = VecGetLocalSize(S,(PetscInt*)&t_Slen);CHKERRQ(ierr);
  ierr = VecGetLocalSize(C,(PetscInt*)&t_Clen);CHKERRQ(ierr);
  assert(t_Clen%ND==0);
  assert(t_Clen*(ND-1)/ND==t_Slen);

  PetscReal *ta_C,*ta_S;
  ierr = VecGetArray(S,&ta_S);CHKERRQ(ierr);
  ierr = VecGetArray(C,&ta_C);CHKERRQ(ierr);

  for (int i=0;i<t_Clen/ND;i++) {
    /* x=sin(theta)*cos(phi) */
    ta_C[i*ND+0]=sin(ta_S[(ND-1)*i+0])*cos(ta_S[(ND-1)*i+1]);
    /* y=sin(theta)*sin(phi) */
    ta_C[i*ND+1]=sin(ta_S[(ND-1)*i+0])*sin(ta_S[(ND-1)*i+1]);
    /* y=cos(theta) */
    ta_C[i*ND+2]=cos(ta_S[(ND-1)*i+0]);
  }

  ierr = VecRestoreArray(C,&ta_C);CHKERRQ(ierr);
  ierr = VecRestoreArray(S,&ta_S);CHKERRQ(ierr);

  MagparFunctionInfoReturn(0);
}


int Cart2Sphere(Vec C, Vec S)
{
  MagparFunctionInfoBegin;

  int t_Clen,t_Slen;
  ierr = VecGetLocalSize(S,(PetscInt*)&t_Slen);CHKERRQ(ierr);
  ierr = VecGetLocalSize(C,(PetscInt*)&t_Clen);CHKERRQ(ierr);
  assert(t_Clen%ND==0);
  assert(t_Clen*(ND-1)/ND==t_Slen);

  PetscReal *ta_C,*ta_S;
  ierr = VecGetArray(S,&ta_S);CHKERRQ(ierr);
  ierr = VecGetArray(C,&ta_C);CHKERRQ(ierr);

  for (int i=0;i<t_Clen/ND;i++) {
    /* theta=acos(z) */
    ta_S[2*i+0]=acos(ta_C[ND*i+2]);
    /* phi=atan(y/x) */
    ta_S[2*i+1]=atan2(ta_C[ND*i+1],ta_C[ND*i+0]);

    /* map negative values [-Pi,0] to [Pi,2*Pi] */
/*
    if (ta_C[ND*i+0]<0) ta_S[2*i+1] = 2.0*PETSC_PI+ta_S[2*i+1];
*/
  }

  ierr = VecRestoreArray(C,&ta_C);CHKERRQ(ierr);
  ierr = VecRestoreArray(S,&ta_S);CHKERRQ(ierr);

  MagparFunctionInfoReturn(0);
}


int Cart2SphereDiff(Vec C, Vec X, Vec S)
{
  MagparFunctionInfoBegin;

  int t_Clen,t_Xlen,t_Slen;
  ierr = VecGetLocalSize(C,(PetscInt*)&t_Clen);CHKERRQ(ierr);
  ierr = VecGetLocalSize(X,(PetscInt*)&t_Xlen);CHKERRQ(ierr);
  ierr = VecGetLocalSize(S,(PetscInt*)&t_Slen);CHKERRQ(ierr);
  assert(t_Clen%ND==0);
  assert(t_Clen*(ND-1)/ND==t_Xlen);
  assert(t_Clen*(ND-1)/ND==t_Slen);

  PetscReal *ta_C,*ta_X,*ta_S;
  ierr = VecGetArray(C,&ta_C);CHKERRQ(ierr);
  ierr = VecGetArray(X,&ta_X);CHKERRQ(ierr);
  ierr = VecGetArray(S,&ta_S);CHKERRQ(ierr);

  /* cf. http://mathworld.wolfram.com/SphericalCoordinates.html */
  /* http://chsfpc5.chem.ncsu.edu/~franzen/CH795Z/math/ang_mom_op/ang_mom_op.html */
  for (int i=0;i<t_Clen/ND;i++) {
    ta_S[(ND-1)*i+0]=
       ta_C[ND*i+0]*cos(ta_X[(ND-1)*i+0])*cos(ta_X[(ND-1)*i+1])
      +ta_C[ND*i+1]*cos(ta_X[(ND-1)*i+0])*sin(ta_X[(ND-1)*i+1])
      -ta_C[ND*i+2]*sin(ta_X[(ND-1)*i+0]);

    ta_S[(ND-1)*i+1]=
      -ta_C[ND*i+0]*sin(ta_X[(ND-1)*i+0])*sin(ta_X[(ND-1)*i+1])
      +ta_C[ND*i+1]*sin(ta_X[(ND-1)*i+0])*cos(ta_X[(ND-1)*i+1]);
  }

  ierr = VecRestoreArray(C,&ta_C);CHKERRQ(ierr);
  ierr = VecRestoreArray(X,&ta_X);CHKERRQ(ierr);
  ierr = VecRestoreArray(S,&ta_S);CHKERRQ(ierr);

  MagparFunctionInfoReturn(0);
}

